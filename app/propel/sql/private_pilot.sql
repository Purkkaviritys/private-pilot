
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- flight_route
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `flight_route`;

CREATE TABLE `flight_route`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `active` TINYINT(1),
    `number_of_seats` TINYINT,
    `name` VARCHAR(100),
    `id_pilot` INTEGER,
    `id_plane` INTEGER,
    `expires_at` DATETIME,
    `price` DECIMAL,
    `description` VARCHAR(255),
    `departure_location` VARCHAR(200),
    `destination_location` VARCHAR(200),
    `id_departure_location` INTEGER,
    `id_destination_location` INTEGER,
    `estimated_flight_time` TIME,
    `rating` FLOAT,
    `departure_date` DATE,
    `destination_date` DATE,
    `departure_time` TIME,
    `destination_time` TIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `flight_route_FI_1` (`id_pilot`),
    INDEX `flight_route_FI_2` (`id_plane`),
    INDEX `flight_route_FI_3` (`id_departure_location`),
    INDEX `flight_route_FI_4` (`id_destination_location`),
    CONSTRAINT `flight_route_FK_1`
        FOREIGN KEY (`id_pilot`)
        REFERENCES `user` (`id`),
    CONSTRAINT `flight_route_FK_2`
        FOREIGN KEY (`id_plane`)
        REFERENCES `airplane` (`id`),
    CONSTRAINT `flight_route_FK_3`
        FOREIGN KEY (`id_departure_location`)
        REFERENCES `airport` (`id`),
    CONSTRAINT `flight_route_FK_4`
        FOREIGN KEY (`id_destination_location`)
        REFERENCES `airport` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- airplane
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `airplane`;

CREATE TABLE `airplane`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `id_image` INTEGER,
    `airplane_model` VARCHAR(200),
    `airplane_manufacturing_date` DATE,
    `airplane_country` VARCHAR(200),
    `airplane_serial_number` VARCHAR(200),
    `airplane_ttaf` VARCHAR(200),
    `number_of_seats_available` INTEGER,
    `number_of_seats` INTEGER,
    `description` VARCHAR(255),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `airplane_U_1` (`airplane_serial_number`),
    INDEX `airplane_FI_1` (`id_image`),
    CONSTRAINT `airplane_FK_1`
        FOREIGN KEY (`id_image`)
        REFERENCES `file` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- file
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(500),
    `mime_type` VARCHAR(100) NOT NULL,
    `url` VARCHAR(100) NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `file_U_1` (`url`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `active` TINYINT(1) NOT NULL,
    `id_image` INTEGER,
    `username` VARCHAR(200) NOT NULL,
    `email_address` VARCHAR(200) NOT NULL,
    `id_role` INTEGER NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `salt` VARCHAR(255),
    `id_plane` INTEGER,
    `id_personal_info` INTEGER,
    `description` VARCHAR(255),
    `timezone` VARCHAR(255),
    `last_login` DATE,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `user_U_1` (`email_address`),
    UNIQUE INDEX `user_U_2` (`username`),
    INDEX `user_FI_1` (`id_image`),
    INDEX `user_FI_2` (`id_role`),
    INDEX `user_FI_3` (`id_plane`),
    INDEX `user_FI_4` (`id_personal_info`),
    CONSTRAINT `user_FK_1`
        FOREIGN KEY (`id_image`)
        REFERENCES `file` (`id`),
    CONSTRAINT `user_FK_2`
        FOREIGN KEY (`id_role`)
        REFERENCES `user_role` (`id`),
    CONSTRAINT `user_FK_3`
        FOREIGN KEY (`id_plane`)
        REFERENCES `airplane` (`id`),
    CONSTRAINT `user_FK_4`
        FOREIGN KEY (`id_personal_info`)
        REFERENCES `user_personal_info` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- user_personal_info
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_personal_info`;

CREATE TABLE `user_personal_info`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(200),
    `last_name` VARCHAR(200),
    `line1` CHAR(200),
    `line2` CHAR(200),
    `city` CHAR(200),
    `state` CHAR(200),
    `postal_code` INTEGER,
    `country_code` CHAR(2),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- user_role
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `role` VARCHAR(200),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- payment_method
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `payment_method`;

CREATE TABLE `payment_method`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- user_comment
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_comment`;

CREATE TABLE `user_comment`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `visible` TINYINT(1),
    `id_user` INTEGER,
    `id_thread` INTEGER,
    `id_parent` INTEGER,
    `comment` VARCHAR(255),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `user_comment_FI_1` (`id_user`),
    CONSTRAINT `user_comment_FK_1`
        FOREIGN KEY (`id_user`)
        REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- received_order
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `received_order`;

CREATE TABLE `received_order`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `id_user_personal_info` INTEGER,
    `id_flight_route` INTEGER,
    `id_order_payment_method` INTEGER,
    `number_of_reserved_seats` INTEGER,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `received_order_FI_1` (`id_user_personal_info`),
    INDEX `received_order_FI_2` (`id_flight_route`),
    INDEX `received_order_FI_3` (`id_order_payment_method`),
    CONSTRAINT `received_order_FK_1`
        FOREIGN KEY (`id_user_personal_info`)
        REFERENCES `user_personal_info` (`id`),
    CONSTRAINT `received_order_FK_2`
        FOREIGN KEY (`id_flight_route`)
        REFERENCES `flight_route` (`id`),
    CONSTRAINT `received_order_FK_3`
        FOREIGN KEY (`id_order_payment_method`)
        REFERENCES `payment_method` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

-- ---------------------------------------------------------------------
-- airport
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `airport`;

CREATE TABLE `airport`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    `description` VARCHAR(255),
    `type` VARCHAR(255),
    `latitude` DOUBLE,
    `longitude` DOUBLE,
    `elevation` INTEGER,
    `state` VARCHAR(255),
    `city` VARCHAR(255),
    `municipality` VARCHAR(255),
    `country` CHAR(2),
    `continent` CHAR(3),
    `scheduled_service` CHAR(3),
    `gps_code` VARCHAR(255),
    `region` VARCHAR(255),
    `faa` CHAR(3),
    `iata` CHAR(3),
    `icao` CHAR(4),
    `role` CHAR(3),
    `enplanements` INTEGER,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_swedish_ci';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
