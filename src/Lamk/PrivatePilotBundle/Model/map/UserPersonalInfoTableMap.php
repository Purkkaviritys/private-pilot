<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'user_personal_info' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class UserPersonalInfoTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.UserPersonalInfoTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_personal_info');
        $this->setPhpName('UserPersonalInfo');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\UserPersonalInfo');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', false, 200, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', false, 200, null);
        $this->addColumn('line1', 'Line1', 'CHAR', false, 200, null);
        $this->getColumn('line1', false)->setPrimaryString(true);
        $this->addColumn('line2', 'Line2', 'CHAR', false, 200, null);
        $this->addColumn('city', 'City', 'CHAR', false, 200, null);
        $this->addColumn('state', 'State', 'CHAR', false, 200, null);
        $this->addColumn('postal_code', 'PostalCode', 'INTEGER', false, null, null);
        $this->addColumn('country_code', 'CountryCode', 'CHAR', false, 2, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'Lamk\\PrivatePilotBundle\\Model\\User', RelationMap::ONE_TO_MANY, array('id' => 'id_personal_info',), null, null, 'Users');
        $this->addRelation('ReceivedOrder', 'Lamk\\PrivatePilotBundle\\Model\\ReceivedOrder', RelationMap::ONE_TO_MANY, array('id' => 'id_user_personal_info',), null, null, 'ReceivedOrders');
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// UserPersonalInfoTableMap
