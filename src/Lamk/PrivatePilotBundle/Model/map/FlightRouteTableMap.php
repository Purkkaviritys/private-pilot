<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'flight_route' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class FlightRouteTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.FlightRouteTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('flight_route');
        $this->setPhpName('FlightRoute');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\FlightRoute');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('active', 'Active', 'BOOLEAN', false, 1, null);
        $this->addColumn('number_of_seats', 'NumberOfSeats', 'TINYINT', false, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 100, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        $this->addForeignKey('id_pilot', 'IdPilot', 'INTEGER', 'user', 'id', false, null, null);
        $this->addForeignKey('id_plane', 'IdPlane', 'INTEGER', 'airplane', 'id', false, null, null);
        $this->addColumn('expires_at', 'ExpiresAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('price', 'Price', 'DECIMAL', false, null, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('departure_location', 'DepartureLocation', 'VARCHAR', false, 200, null);
        $this->addColumn('destination_location', 'DestinationLocation', 'VARCHAR', false, 200, null);
        $this->addForeignKey('id_departure_location', 'IdDepartureLocation', 'INTEGER', 'airport', 'id', false, null, null);
        $this->addForeignKey('id_destination_location', 'IdDestinationLocation', 'INTEGER', 'airport', 'id', false, null, null);
        $this->addColumn('estimated_flight_time', 'EstimatedFlightTime', 'TIME', false, null, null);
        $this->addColumn('rating', 'Rating', 'FLOAT', false, null, null);
        $this->addColumn('departure_date', 'DepartureDate', 'DATE', false, null, null);
        $this->addColumn('destination_date', 'DestinationDate', 'DATE', false, null, null);
        $this->addColumn('departure_time', 'DepartureTime', 'TIME', false, null, null);
        $this->addColumn('destination_time', 'DestinationTime', 'TIME', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'Lamk\\PrivatePilotBundle\\Model\\User', RelationMap::MANY_TO_ONE, array('id_pilot' => 'id',), null, null);
        $this->addRelation('Airplane', 'Lamk\\PrivatePilotBundle\\Model\\Airplane', RelationMap::MANY_TO_ONE, array('id_plane' => 'id',), null, null);
        $this->addRelation('AirportRelatedByIdDepartureLocation', 'Lamk\\PrivatePilotBundle\\Model\\Airport', RelationMap::MANY_TO_ONE, array('id_departure_location' => 'id',), null, null);
        $this->addRelation('AirportRelatedByIdDestinationLocation', 'Lamk\\PrivatePilotBundle\\Model\\Airport', RelationMap::MANY_TO_ONE, array('id_destination_location' => 'id',), null, null);
        $this->addRelation('ReceivedOrder', 'Lamk\\PrivatePilotBundle\\Model\\ReceivedOrder', RelationMap::ONE_TO_MANY, array('id' => 'id_flight_route',), null, null, 'ReceivedOrders');
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// FlightRouteTableMap
