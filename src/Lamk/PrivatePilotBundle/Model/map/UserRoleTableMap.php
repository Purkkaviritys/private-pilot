<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'user_role' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class UserRoleTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.UserRoleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_role');
        $this->setPhpName('UserRole');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\UserRole');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('role', 'Role', 'VARCHAR', false, 200, null);
        $this->getColumn('role', false)->setPrimaryString(true);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'Lamk\\PrivatePilotBundle\\Model\\User', RelationMap::ONE_TO_MANY, array('id' => 'id_role',), null, null, 'Users');
    }
// buildRelations()
}

// UserRoleTableMap
