<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'user_comment' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class UserCommentTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.UserCommentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_comment');
        $this->setPhpName('UserComment');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\UserComment');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('visible', 'Visible', 'BOOLEAN', false, 1, null);
        $this->addForeignKey('id_user', 'IdUser', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('id_thread', 'IdThread', 'INTEGER', false, null, null);
        $this->addColumn('id_parent', 'IdParent', 'INTEGER', false, null, null);
        $this->addColumn('comment', 'Comment', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'Lamk\\PrivatePilotBundle\\Model\\User', RelationMap::MANY_TO_ONE, array('id_user' => 'id',), null, null);
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// UserCommentTableMap
