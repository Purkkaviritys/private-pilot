<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'received_order' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class ReceivedOrderTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.ReceivedOrderTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('received_order');
        $this->setPhpName('ReceivedOrder');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\ReceivedOrder');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_user_personal_info', 'IdUserPersonalInfo', 'INTEGER', 'user_personal_info', 'id', false, null, null);
        $this->addForeignKey('id_flight_route', 'IdFlightRoute', 'INTEGER', 'flight_route', 'id', false, null, null);
        $this->addForeignKey('id_order_payment_method', 'IdOrderPaymentMethod', 'INTEGER', 'payment_method', 'id', false, null, null);
        $this->addColumn('number_of_reserved_seats', 'NumberOfReservedSeats', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UserPersonalInfo', 'Lamk\\PrivatePilotBundle\\Model\\UserPersonalInfo', RelationMap::MANY_TO_ONE, array('id_user_personal_info' => 'id',), null, null);
        $this->addRelation('FlightRoute', 'Lamk\\PrivatePilotBundle\\Model\\FlightRoute', RelationMap::MANY_TO_ONE, array('id_flight_route' => 'id',), null, null);
        $this->addRelation('PaymentMethod', 'Lamk\\PrivatePilotBundle\\Model\\PaymentMethod', RelationMap::MANY_TO_ONE, array('id_order_payment_method' => 'id',), null, null);
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// ReceivedOrderTableMap
