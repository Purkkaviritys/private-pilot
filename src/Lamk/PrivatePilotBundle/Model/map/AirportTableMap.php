<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'airport' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class AirportTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.AirportTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('airport');
        $this->setPhpName('Airport');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\Airport');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 255, null);
        $this->addColumn('latitude', 'Latitude', 'DOUBLE', false, null, null);
        $this->addColumn('longitude', 'Longitude', 'DOUBLE', false, null, null);
        $this->addColumn('elevation', 'Elevation', 'INTEGER', false, null, null);
        $this->addColumn('state', 'State', 'VARCHAR', false, 255, null);
        $this->addColumn('city', 'City', 'VARCHAR', false, 255, null);
        $this->addColumn('municipality', 'Municipality', 'VARCHAR', false, 255, null);
        $this->addColumn('country', 'Country', 'CHAR', false, 2, null);
        $this->addColumn('continent', 'Continent', 'CHAR', false, 3, null);
        $this->addColumn('scheduled_service', 'ScheduledService', 'CHAR', false, 3, null);
        $this->addColumn('gps_code', 'GpsCode', 'VARCHAR', false, 255, null);
        $this->addColumn('region', 'Region', 'VARCHAR', false, 255, null);
        $this->addColumn('faa', 'Faa', 'CHAR', false, 3, null);
        $this->addColumn('iata', 'Iata', 'CHAR', false, 3, null);
        $this->addColumn('icao', 'Icao', 'CHAR', false, 4, null);
        $this->addColumn('role', 'Role', 'CHAR', false, 3, null);
        $this->addColumn('enplanements', 'Enplanements', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('FlightRouteRelatedByIdDepartureLocation', 'Lamk\\PrivatePilotBundle\\Model\\FlightRoute', RelationMap::ONE_TO_MANY, array('id' => 'id_departure_location',), null, null, 'FlightRoutesRelatedByIdDepartureLocation');
        $this->addRelation('FlightRouteRelatedByIdDestinationLocation', 'Lamk\\PrivatePilotBundle\\Model\\FlightRoute', RelationMap::ONE_TO_MANY, array('id' => 'id_destination_location',), null, null, 'FlightRoutesRelatedByIdDestinationLocation');
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// AirportTableMap
