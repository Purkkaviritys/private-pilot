<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class UserTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.UserTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user');
        $this->setPhpName('User');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\User');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('active', 'Active', 'BOOLEAN', true, 1, null);
        $this->addForeignKey('id_image', 'IdImage', 'INTEGER', 'file', 'id', false, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 200, null);
        $this->addColumn('email_address', 'EmailAddress', 'VARCHAR', true, 200, null);
        $this->addForeignKey('id_role', 'IdRole', 'INTEGER', 'user_role', 'id', true, null, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', false, 255, null);
        $this->addForeignKey('id_plane', 'IdPlane', 'INTEGER', 'airplane', 'id', false, null, null);
        $this->addForeignKey('id_personal_info', 'IdPersonalInfo', 'INTEGER', 'user_personal_info', 'id', false, null, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('timezone', 'Timezone', 'VARCHAR', false, 255, null);
        $this->addColumn('last_login', 'LastLogin', 'DATE', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('File', 'Lamk\\PrivatePilotBundle\\Model\\File', RelationMap::MANY_TO_ONE, array('id_image' => 'id',), null, null);
        $this->addRelation('UserRole', 'Lamk\\PrivatePilotBundle\\Model\\UserRole', RelationMap::MANY_TO_ONE, array('id_role' => 'id',), null, null);
        $this->addRelation('Airplane', 'Lamk\\PrivatePilotBundle\\Model\\Airplane', RelationMap::MANY_TO_ONE, array('id_plane' => 'id',), null, null);
        $this->addRelation('UserPersonalInfo', 'Lamk\\PrivatePilotBundle\\Model\\UserPersonalInfo', RelationMap::MANY_TO_ONE, array('id_personal_info' => 'id',), null, null);
        $this->addRelation('FlightRoute', 'Lamk\\PrivatePilotBundle\\Model\\FlightRoute', RelationMap::ONE_TO_MANY, array('id' => 'id_pilot',), null, null, 'FlightRoutes');
        $this->addRelation('UserComment', 'Lamk\\PrivatePilotBundle\\Model\\UserComment', RelationMap::ONE_TO_MANY, array('id' => 'id_user',), null, null, 'UserComments');
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// UserTableMap
