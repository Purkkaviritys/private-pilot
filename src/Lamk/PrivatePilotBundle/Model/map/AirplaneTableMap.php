<?php

namespace Lamk\PrivatePilotBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'airplane' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Lamk.PrivatePilotBundle.Model.map
 */
class AirplaneTableMap extends TableMap
{
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Lamk.PrivatePilotBundle.Model.map.AirplaneTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('airplane');
        $this->setPhpName('Airplane');
        $this->setClassname('Lamk\\PrivatePilotBundle\\Model\\Airplane');
        $this->setPackage('src.Lamk.PrivatePilotBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_image', 'IdImage', 'INTEGER', 'file', 'id', false, null, null);
        $this->addColumn('airplane_model', 'AirplaneModel', 'VARCHAR', false, 200, null);
        $this->addColumn('airplane_manufacturing_date', 'AirplaneManufacturingDate', 'DATE', false, null, null);
        $this->addColumn('airplane_country', 'AirplaneCountry', 'VARCHAR', false, 200, null);
        $this->addColumn('airplane_serial_number', 'AirplaneSerialNumber', 'VARCHAR', false, 200, null);
        $this->addColumn('airplane_ttaf', 'AirplaneTtaf', 'VARCHAR', false, 200, null);
        $this->addColumn('number_of_seats_available', 'NumberOfSeatsAvailable', 'INTEGER', false, null, null);
        $this->addColumn('number_of_seats', 'NumberOfSeats', 'INTEGER', false, null, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        // validators
    }
// initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('File', 'Lamk\\PrivatePilotBundle\\Model\\File', RelationMap::MANY_TO_ONE, array('id_image' => 'id',), null, null);
        $this->addRelation('FlightRoute', 'Lamk\\PrivatePilotBundle\\Model\\FlightRoute', RelationMap::ONE_TO_MANY, array('id' => 'id_plane',), null, null, 'FlightRoutes');
        $this->addRelation('User', 'Lamk\\PrivatePilotBundle\\Model\\User', RelationMap::ONE_TO_MANY, array('id' => 'id_plane',), null, null, 'Users');
    }
// buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
          'timestampable' => array(
            'create_column'      => 'created_at',
            'update_column'      => 'updated_at',
            'disable_updated_at' => 'false',
          ),
        );
    }
// getBehaviors()
}

// AirplaneTableMap
