<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\FlightRoutePeer;
use Lamk\PrivatePilotBundle\Model\PaymentMethodPeer;
use Lamk\PrivatePilotBundle\Model\ReceivedOrder;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderPeer;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoPeer;
use Lamk\PrivatePilotBundle\Model\map\ReceivedOrderTableMap;

abstract class BaseReceivedOrderPeer
{
    /** the default database name for this class */
    const DATABASE_NAME = 'private_pilot';
    /** the table name for this class */
    const TABLE_NAME = 'received_order';
    /** the related Propel class for this table */
    const OM_CLASS = 'Lamk\\PrivatePilotBundle\\Model\\ReceivedOrder';
    /** the related TableMap class for this table */
    const TM_CLASS = 'Lamk\\PrivatePilotBundle\\Model\\map\\ReceivedOrderTableMap';
    /** The total number of columns. */
    const NUM_COLUMNS = 7;
    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;
    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 7;
    /** the column name for the id field */
    const ID = 'received_order.id';
    /** the column name for the id_user_personal_info field */
    const ID_USER_PERSONAL_INFO = 'received_order.id_user_personal_info';
    /** the column name for the id_flight_route field */
    const ID_FLIGHT_ROUTE = 'received_order.id_flight_route';
    /** the column name for the id_order_payment_method field */
    const ID_ORDER_PAYMENT_METHOD = 'received_order.id_order_payment_method';
    /** the column name for the number_of_reserved_seats field */
    const NUMBER_OF_RESERVED_SEATS = 'received_order.number_of_reserved_seats';
    /** the column name for the created_at field */
    const CREATED_AT = 'received_order.created_at';
    /** the column name for the updated_at field */
    const UPDATED_AT = 'received_order.updated_at';
    /** The default string format for model objects of the related table * */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of ReceivedOrder objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array ReceivedOrder[]
     */
    public static $instances = array();
    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. ReceivedOrderPeer::$fieldNames[ReceivedOrderPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array(
      BasePeer::TYPE_PHPNAME       => array('Id', 'IdUserPersonalInfo', 'IdFlightRoute', 'IdOrderPaymentMethod', 'NumberOfReservedSeats', 'CreatedAt', 'UpdatedAt',),
      BasePeer::TYPE_STUDLYPHPNAME => array('id', 'idUserPersonalInfo', 'idFlightRoute', 'idOrderPaymentMethod', 'numberOfReservedSeats', 'createdAt', 'updatedAt',),
      BasePeer::TYPE_COLNAME       => array(ReceivedOrderPeer::ID, ReceivedOrderPeer::ID_USER_PERSONAL_INFO, ReceivedOrderPeer::ID_FLIGHT_ROUTE, ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, ReceivedOrderPeer::NUMBER_OF_RESERVED_SEATS, ReceivedOrderPeer::CREATED_AT, ReceivedOrderPeer::UPDATED_AT,),
      BasePeer::TYPE_RAW_COLNAME   => array('ID', 'ID_USER_PERSONAL_INFO', 'ID_FLIGHT_ROUTE', 'ID_ORDER_PAYMENT_METHOD', 'NUMBER_OF_RESERVED_SEATS', 'CREATED_AT', 'UPDATED_AT',),
      BasePeer::TYPE_FIELDNAME     => array('id', 'id_user_personal_info', 'id_flight_route', 'id_order_payment_method', 'number_of_reserved_seats', 'created_at', 'updated_at',),
      BasePeer::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6,)
    );
    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. ReceivedOrderPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array(
      BasePeer::TYPE_PHPNAME       => array('Id' => 0, 'IdUserPersonalInfo' => 1, 'IdFlightRoute' => 2, 'IdOrderPaymentMethod' => 3, 'NumberOfReservedSeats' => 4, 'CreatedAt' => 5, 'UpdatedAt' => 6,),
      BasePeer::TYPE_STUDLYPHPNAME => array('id' => 0, 'idUserPersonalInfo' => 1, 'idFlightRoute' => 2, 'idOrderPaymentMethod' => 3, 'numberOfReservedSeats' => 4, 'createdAt' => 5, 'updatedAt' => 6,),
      BasePeer::TYPE_COLNAME       => array(ReceivedOrderPeer::ID => 0, ReceivedOrderPeer::ID_USER_PERSONAL_INFO => 1, ReceivedOrderPeer::ID_FLIGHT_ROUTE => 2, ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD => 3, ReceivedOrderPeer::NUMBER_OF_RESERVED_SEATS => 4, ReceivedOrderPeer::CREATED_AT => 5, ReceivedOrderPeer::UPDATED_AT => 6,),
      BasePeer::TYPE_RAW_COLNAME   => array('ID' => 0, 'ID_USER_PERSONAL_INFO' => 1, 'ID_FLIGHT_ROUTE' => 2, 'ID_ORDER_PAYMENT_METHOD' => 3, 'NUMBER_OF_RESERVED_SEATS' => 4, 'CREATED_AT' => 5, 'UPDATED_AT' => 6,),
      BasePeer::TYPE_FIELDNAME     => array('id' => 0, 'id_user_personal_info' => 1, 'id_flight_route' => 2, 'id_order_payment_method' => 3, 'number_of_reserved_seats' => 4, 'created_at' => 5, 'updated_at' => 6,),
      BasePeer::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6,)
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = ReceivedOrderPeer::getFieldNames($toType);
        $key = isset(ReceivedOrderPeer::$fieldKeys[$fromType][$name]) ? ReceivedOrderPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(ReceivedOrderPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, ReceivedOrderPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return ReceivedOrderPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     * 		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     * 		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. ReceivedOrderPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(ReceivedOrderPeer::TABLE_NAME . '.', $alias . '.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ReceivedOrderPeer::ID);
            $criteria->addSelectColumn(ReceivedOrderPeer::ID_USER_PERSONAL_INFO);
            $criteria->addSelectColumn(ReceivedOrderPeer::ID_FLIGHT_ROUTE);
            $criteria->addSelectColumn(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD);
            $criteria->addSelectColumn(ReceivedOrderPeer::NUMBER_OF_RESERVED_SEATS);
            $criteria->addSelectColumn(ReceivedOrderPeer::CREATED_AT);
            $criteria->addSelectColumn(ReceivedOrderPeer::UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_user_personal_info');
            $criteria->addSelectColumn($alias . '.id_flight_route');
            $criteria->addSelectColumn($alias . '.id_order_payment_method');
            $criteria->addSelectColumn($alias . '.number_of_reserved_seats');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return ReceivedOrder
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = ReceivedOrderPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }

    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return ReceivedOrderPeer::populateObjects(ReceivedOrderPeer::doSelectStmt($criteria, $con));
    }

    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param ReceivedOrder $obj A ReceivedOrder object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            ReceivedOrderPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A ReceivedOrder object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof ReceivedOrder) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or ReceivedOrder object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(ReceivedOrderPeer::$instances[$key]);
        }
    }
// removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return ReceivedOrder Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(ReceivedOrderPeer::$instances[$key])) {
                return ReceivedOrderPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
        if ($and_clear_all_references) {
            foreach (ReceivedOrderPeer::$instances as $instance) {
                $instance->clearAllReferences(true);
            }
        }
        ReceivedOrderPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to received_order
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = ReceivedOrderPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = ReceivedOrderPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ReceivedOrderPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     * @return array (ReceivedOrder object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = ReceivedOrderPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ReceivedOrderPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            ReceivedOrderPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the number of rows matching criteria, joining the related UserPersonalInfo table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinUserPersonalInfo(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Returns the number of rows matching criteria, joining the related FlightRoute table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinFlightRoute(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Returns the number of rows matching criteria, joining the related PaymentMethod table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPaymentMethod(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with their UserPersonalInfo objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinUserPersonalInfo(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;
        UserPersonalInfoPeer::addSelectColumns($criteria);

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = UserPersonalInfoPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = UserPersonalInfoPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = UserPersonalInfoPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    UserPersonalInfoPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded
                // Add the $obj1 (ReceivedOrder) to $obj2 (UserPersonalInfo)
                $obj2->addReceivedOrder($obj1);
            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with their FlightRoute objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinFlightRoute(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;
        FlightRoutePeer::addSelectColumns($criteria);

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = FlightRoutePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = FlightRoutePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = FlightRoutePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    FlightRoutePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded
                // Add the $obj1 (ReceivedOrder) to $obj2 (FlightRoute)
                $obj2->addReceivedOrder($obj1);
            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with their PaymentMethod objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPaymentMethod(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;
        PaymentMethodPeer::addSelectColumns($criteria);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PaymentMethodPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PaymentMethodPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PaymentMethodPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PaymentMethodPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded
                // Add the $obj1 (ReceivedOrder) to $obj2 (PaymentMethod)
                $obj2->addReceivedOrder($obj1);
            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol2 = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;

        UserPersonalInfoPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + UserPersonalInfoPeer::NUM_HYDRATE_COLUMNS;

        FlightRoutePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + FlightRoutePeer::NUM_HYDRATE_COLUMNS;

        PaymentMethodPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + PaymentMethodPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded
            // Add objects for joined UserPersonalInfo rows

            $key2 = UserPersonalInfoPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = UserPersonalInfoPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = UserPersonalInfoPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    UserPersonalInfoPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj2 (UserPersonalInfo)
                $obj2->addReceivedOrder($obj1);
            } // if joined row not null
            // Add objects for joined FlightRoute rows

            $key3 = FlightRoutePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = FlightRoutePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = FlightRoutePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    FlightRoutePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj3 (FlightRoute)
                $obj3->addReceivedOrder($obj1);
            } // if joined row not null
            // Add objects for joined PaymentMethod rows

            $key4 = PaymentMethodPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = PaymentMethodPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = PaymentMethodPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    PaymentMethodPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj4 (PaymentMethod)
                $obj4->addReceivedOrder($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the number of rows matching criteria, joining the related UserPersonalInfo table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptUserPersonalInfo(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Returns the number of rows matching criteria, joining the related FlightRoute table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptFlightRoute(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Returns the number of rows matching criteria, joining the related PaymentMethod table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPaymentMethod(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ReceivedOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count
        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with all related objects except UserPersonalInfo.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptUserPersonalInfo(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol2 = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;

        FlightRoutePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + FlightRoutePeer::NUM_HYDRATE_COLUMNS;

        PaymentMethodPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PaymentMethodPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded
            // Add objects for joined FlightRoute rows

            $key2 = FlightRoutePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = FlightRoutePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = FlightRoutePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    FlightRoutePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj2 (FlightRoute)
                $obj2->addReceivedOrder($obj1);
            } // if joined row is not null
            // Add objects for joined PaymentMethod rows

            $key3 = PaymentMethodPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = PaymentMethodPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = PaymentMethodPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PaymentMethodPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj3 (PaymentMethod)
                $obj3->addReceivedOrder($obj1);
            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with all related objects except FlightRoute.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptFlightRoute(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol2 = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;

        UserPersonalInfoPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + UserPersonalInfoPeer::NUM_HYDRATE_COLUMNS;

        PaymentMethodPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + PaymentMethodPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, PaymentMethodPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded
            // Add objects for joined UserPersonalInfo rows

            $key2 = UserPersonalInfoPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = UserPersonalInfoPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = UserPersonalInfoPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    UserPersonalInfoPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj2 (UserPersonalInfo)
                $obj2->addReceivedOrder($obj1);
            } // if joined row is not null
            // Add objects for joined PaymentMethod rows

            $key3 = PaymentMethodPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = PaymentMethodPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = PaymentMethodPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    PaymentMethodPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj3 (PaymentMethod)
                $obj3->addReceivedOrder($obj1);
            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Selects a collection of ReceivedOrder objects pre-filled with all related objects except PaymentMethod.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of ReceivedOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPaymentMethod(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);
        }

        ReceivedOrderPeer::addSelectColumns($criteria);
        $startcol2 = ReceivedOrderPeer::NUM_HYDRATE_COLUMNS;

        UserPersonalInfoPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + UserPersonalInfoPeer::NUM_HYDRATE_COLUMNS;

        FlightRoutePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + FlightRoutePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, UserPersonalInfoPeer::ID, $join_behavior);

        $criteria->addJoin(ReceivedOrderPeer::ID_FLIGHT_ROUTE, FlightRoutePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = ReceivedOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = ReceivedOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = ReceivedOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                ReceivedOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded
            // Add objects for joined UserPersonalInfo rows

            $key2 = UserPersonalInfoPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = UserPersonalInfoPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = UserPersonalInfoPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    UserPersonalInfoPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj2 (UserPersonalInfo)
                $obj2->addReceivedOrder($obj1);
            } // if joined row is not null
            // Add objects for joined FlightRoute rows

            $key3 = FlightRoutePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = FlightRoutePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = FlightRoutePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    FlightRoutePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded
                // Add the $obj1 (ReceivedOrder) to the collection in $obj3 (FlightRoute)
                $obj3->addReceivedOrder($obj1);
            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(ReceivedOrderPeer::DATABASE_NAME)->getTable(ReceivedOrderPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getDatabaseMap(BaseReceivedOrderPeer::DATABASE_NAME);
        if (!$dbMap->hasTable(BaseReceivedOrderPeer::TABLE_NAME)) {
            $dbMap->addTableObject(new \Lamk\PrivatePilotBundle\Model\map\ReceivedOrderTableMap());
        }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return ReceivedOrderPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a ReceivedOrder or Criteria object.
     *
     * @param      mixed $values Criteria or ReceivedOrder object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from ReceivedOrder object
        }

        if ($criteria->containsKey(ReceivedOrderPeer::ID) && $criteria->keyContainsValue(ReceivedOrderPeer::ID)) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ReceivedOrderPeer::ID . ')');
        }


        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a ReceivedOrder or Criteria object.
     *
     * @param      mixed $values Criteria or ReceivedOrder object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(ReceivedOrderPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(ReceivedOrderPeer::ID);
            $value = $criteria->remove(ReceivedOrderPeer::ID);
            if ($value) {
                $selectCriteria->add(ReceivedOrderPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(ReceivedOrderPeer::TABLE_NAME);
            }
        } else { // $values is ReceivedOrder object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the received_order table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(ReceivedOrderPeer::TABLE_NAME, $con, ReceivedOrderPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ReceivedOrderPeer::clearInstancePool();
            ReceivedOrderPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a ReceivedOrder or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or ReceivedOrder object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     * 				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doDelete($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            ReceivedOrderPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof ReceivedOrder) { // it's a model object
            // invalidate the cache for this single object
            ReceivedOrderPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ReceivedOrderPeer::DATABASE_NAME);
            $criteria->add(ReceivedOrderPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                ReceivedOrderPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(ReceivedOrderPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            ReceivedOrderPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given ReceivedOrder object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param ReceivedOrder $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(ReceivedOrderPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(ReceivedOrderPeer::TABLE_NAME);

            if (!is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {
            
        }

        return BasePeer::doValidate(ReceivedOrderPeer::DATABASE_NAME, ReceivedOrderPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return ReceivedOrder
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = ReceivedOrderPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(ReceivedOrderPeer::DATABASE_NAME);
        $criteria->add(ReceivedOrderPeer::ID, $pk);

        $v = ReceivedOrderPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return ReceivedOrder[]
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(ReceivedOrderPeer::DATABASE_NAME);
            $criteria->add(ReceivedOrderPeer::ID, $pks, Criteria::IN);
            $objs = ReceivedOrderPeer::doSelect($criteria, $con);
        }

        return $objs;
    }
}

// BaseReceivedOrderPeer
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseReceivedOrderPeer::buildTableMap();

