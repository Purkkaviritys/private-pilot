<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\AirplanePeer;
use Lamk\PrivatePilotBundle\Model\AirplaneQuery;
use Lamk\PrivatePilotBundle\Model\File;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\User;

/**
 * @method AirplaneQuery orderById($order = Criteria::ASC) Order by the id column
 * @method AirplaneQuery orderByIdImage($order = Criteria::ASC) Order by the id_image column
 * @method AirplaneQuery orderByAirplaneModel($order = Criteria::ASC) Order by the airplane_model column
 * @method AirplaneQuery orderByAirplaneManufacturingDate($order = Criteria::ASC) Order by the airplane_manufacturing_date column
 * @method AirplaneQuery orderByAirplaneCountry($order = Criteria::ASC) Order by the airplane_country column
 * @method AirplaneQuery orderByAirplaneSerialNumber($order = Criteria::ASC) Order by the airplane_serial_number column
 * @method AirplaneQuery orderByAirplaneTtaf($order = Criteria::ASC) Order by the airplane_ttaf column
 * @method AirplaneQuery orderByNumberOfSeatsAvailable($order = Criteria::ASC) Order by the number_of_seats_available column
 * @method AirplaneQuery orderByNumberOfSeats($order = Criteria::ASC) Order by the number_of_seats column
 * @method AirplaneQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method AirplaneQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method AirplaneQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method AirplaneQuery groupById() Group by the id column
 * @method AirplaneQuery groupByIdImage() Group by the id_image column
 * @method AirplaneQuery groupByAirplaneModel() Group by the airplane_model column
 * @method AirplaneQuery groupByAirplaneManufacturingDate() Group by the airplane_manufacturing_date column
 * @method AirplaneQuery groupByAirplaneCountry() Group by the airplane_country column
 * @method AirplaneQuery groupByAirplaneSerialNumber() Group by the airplane_serial_number column
 * @method AirplaneQuery groupByAirplaneTtaf() Group by the airplane_ttaf column
 * @method AirplaneQuery groupByNumberOfSeatsAvailable() Group by the number_of_seats_available column
 * @method AirplaneQuery groupByNumberOfSeats() Group by the number_of_seats column
 * @method AirplaneQuery groupByDescription() Group by the description column
 * @method AirplaneQuery groupByCreatedAt() Group by the created_at column
 * @method AirplaneQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method AirplaneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method AirplaneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method AirplaneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method AirplaneQuery leftJoinFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the File relation
 * @method AirplaneQuery rightJoinFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the File relation
 * @method AirplaneQuery innerJoinFile($relationAlias = null) Adds a INNER JOIN clause to the query using the File relation
 *
 * @method AirplaneQuery leftJoinFlightRoute($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlightRoute relation
 * @method AirplaneQuery rightJoinFlightRoute($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlightRoute relation
 * @method AirplaneQuery innerJoinFlightRoute($relationAlias = null) Adds a INNER JOIN clause to the query using the FlightRoute relation
 *
 * @method AirplaneQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method AirplaneQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method AirplaneQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method Airplane findOne(PropelPDO $con = null) Return the first Airplane matching the query
 * @method Airplane findOneOrCreate(PropelPDO $con = null) Return the first Airplane matching the query, or a new Airplane object populated from the query conditions when no match is found
 *
 * @method Airplane findOneByIdImage(int $id_image) Return the first Airplane filtered by the id_image column
 * @method Airplane findOneByAirplaneModel(string $airplane_model) Return the first Airplane filtered by the airplane_model column
 * @method Airplane findOneByAirplaneManufacturingDate(string $airplane_manufacturing_date) Return the first Airplane filtered by the airplane_manufacturing_date column
 * @method Airplane findOneByAirplaneCountry(string $airplane_country) Return the first Airplane filtered by the airplane_country column
 * @method Airplane findOneByAirplaneSerialNumber(string $airplane_serial_number) Return the first Airplane filtered by the airplane_serial_number column
 * @method Airplane findOneByAirplaneTtaf(string $airplane_ttaf) Return the first Airplane filtered by the airplane_ttaf column
 * @method Airplane findOneByNumberOfSeatsAvailable(int $number_of_seats_available) Return the first Airplane filtered by the number_of_seats_available column
 * @method Airplane findOneByNumberOfSeats(int $number_of_seats) Return the first Airplane filtered by the number_of_seats column
 * @method Airplane findOneByDescription(string $description) Return the first Airplane filtered by the description column
 * @method Airplane findOneByCreatedAt(string $created_at) Return the first Airplane filtered by the created_at column
 * @method Airplane findOneByUpdatedAt(string $updated_at) Return the first Airplane filtered by the updated_at column
 *
 * @method array findById(int $id) Return Airplane objects filtered by the id column
 * @method array findByIdImage(int $id_image) Return Airplane objects filtered by the id_image column
 * @method array findByAirplaneModel(string $airplane_model) Return Airplane objects filtered by the airplane_model column
 * @method array findByAirplaneManufacturingDate(string $airplane_manufacturing_date) Return Airplane objects filtered by the airplane_manufacturing_date column
 * @method array findByAirplaneCountry(string $airplane_country) Return Airplane objects filtered by the airplane_country column
 * @method array findByAirplaneSerialNumber(string $airplane_serial_number) Return Airplane objects filtered by the airplane_serial_number column
 * @method array findByAirplaneTtaf(string $airplane_ttaf) Return Airplane objects filtered by the airplane_ttaf column
 * @method array findByNumberOfSeatsAvailable(int $number_of_seats_available) Return Airplane objects filtered by the number_of_seats_available column
 * @method array findByNumberOfSeats(int $number_of_seats) Return Airplane objects filtered by the number_of_seats column
 * @method array findByDescription(string $description) Return Airplane objects filtered by the description column
 * @method array findByCreatedAt(string $created_at) Return Airplane objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return Airplane objects filtered by the updated_at column
 */
abstract class BaseAirplaneQuery extends ModelCriteria
{

    /**
     * Initializes internal state of BaseAirplaneQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'private_pilot';
        }
        if (null === $modelName) {
            $modelName = 'Lamk\\PrivatePilotBundle\\Model\\Airplane';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new AirplaneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   AirplaneQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return AirplaneQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof AirplaneQuery) {
            return $criteria;
        }
        $query = new AirplaneQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Airplane|Airplane[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AirplanePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(AirplanePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select || $this->selectColumns || $this->asColumns || $this->selectModifiers || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Airplane A model object, or null if the key is not found
     * @throws PropelException
     */
    public function findOneById($key, $con = null)
    {
        return $this->findPk($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Airplane A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_image`, `airplane_model`, `airplane_manufacturing_date`, `airplane_country`, `airplane_serial_number`, `airplane_ttaf`, `number_of_seats_available`, `number_of_seats`, `description`, `created_at`, `updated_at` FROM `airplane` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Airplane();
            $obj->hydrate($row);
            AirplanePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Airplane|Airplane[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Airplane[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AirplanePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AirplanePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AirplanePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AirplanePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_image column
     *
     * Example usage:
     * <code>
     * $query->filterByIdImage(1234); // WHERE id_image = 1234
     * $query->filterByIdImage(array(12, 34)); // WHERE id_image IN (12, 34)
     * $query->filterByIdImage(array('min' => 12)); // WHERE id_image >= 12
     * $query->filterByIdImage(array('max' => 12)); // WHERE id_image <= 12
     * </code>
     *
     * @see       filterByFile()
     *
     * @param     mixed $idImage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByIdImage($idImage = null, $comparison = null)
    {
        if (is_array($idImage)) {
            $useMinMax = false;
            if (isset($idImage['min'])) {
                $this->addUsingAlias(AirplanePeer::ID_IMAGE, $idImage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idImage['max'])) {
                $this->addUsingAlias(AirplanePeer::ID_IMAGE, $idImage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::ID_IMAGE, $idImage, $comparison);
    }

    /**
     * Filter the query on the airplane_model column
     *
     * Example usage:
     * <code>
     * $query->filterByAirplaneModel('fooValue');   // WHERE airplane_model = 'fooValue'
     * $query->filterByAirplaneModel('%fooValue%'); // WHERE airplane_model LIKE '%fooValue%'
     * </code>
     *
     * @param     string $airplaneModel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByAirplaneModel($airplaneModel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($airplaneModel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $airplaneModel)) {
                $airplaneModel = str_replace('*', '%', $airplaneModel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirplanePeer::AIRPLANE_MODEL, $airplaneModel, $comparison);
    }

    /**
     * Filter the query on the airplane_manufacturing_date column
     *
     * Example usage:
     * <code>
     * $query->filterByAirplaneManufacturingDate('2011-03-14'); // WHERE airplane_manufacturing_date = '2011-03-14'
     * $query->filterByAirplaneManufacturingDate('now'); // WHERE airplane_manufacturing_date = '2011-03-14'
     * $query->filterByAirplaneManufacturingDate(array('max' => 'yesterday')); // WHERE airplane_manufacturing_date < '2011-03-13'
     * </code>
     *
     * @param     mixed $airplaneManufacturingDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByAirplaneManufacturingDate($airplaneManufacturingDate = null, $comparison = null)
    {
        if (is_array($airplaneManufacturingDate)) {
            $useMinMax = false;
            if (isset($airplaneManufacturingDate['min'])) {
                $this->addUsingAlias(AirplanePeer::AIRPLANE_MANUFACTURING_DATE, $airplaneManufacturingDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($airplaneManufacturingDate['max'])) {
                $this->addUsingAlias(AirplanePeer::AIRPLANE_MANUFACTURING_DATE, $airplaneManufacturingDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::AIRPLANE_MANUFACTURING_DATE, $airplaneManufacturingDate, $comparison);
    }

    /**
     * Filter the query on the airplane_country column
     *
     * Example usage:
     * <code>
     * $query->filterByAirplaneCountry('fooValue');   // WHERE airplane_country = 'fooValue'
     * $query->filterByAirplaneCountry('%fooValue%'); // WHERE airplane_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $airplaneCountry The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByAirplaneCountry($airplaneCountry = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($airplaneCountry)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $airplaneCountry)) {
                $airplaneCountry = str_replace('*', '%', $airplaneCountry);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirplanePeer::AIRPLANE_COUNTRY, $airplaneCountry, $comparison);
    }

    /**
     * Filter the query on the airplane_serial_number column
     *
     * Example usage:
     * <code>
     * $query->filterByAirplaneSerialNumber('fooValue');   // WHERE airplane_serial_number = 'fooValue'
     * $query->filterByAirplaneSerialNumber('%fooValue%'); // WHERE airplane_serial_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $airplaneSerialNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByAirplaneSerialNumber($airplaneSerialNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($airplaneSerialNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $airplaneSerialNumber)) {
                $airplaneSerialNumber = str_replace('*', '%', $airplaneSerialNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirplanePeer::AIRPLANE_SERIAL_NUMBER, $airplaneSerialNumber, $comparison);
    }

    /**
     * Filter the query on the airplane_ttaf column
     *
     * Example usage:
     * <code>
     * $query->filterByAirplaneTtaf('fooValue');   // WHERE airplane_ttaf = 'fooValue'
     * $query->filterByAirplaneTtaf('%fooValue%'); // WHERE airplane_ttaf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $airplaneTtaf The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByAirplaneTtaf($airplaneTtaf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($airplaneTtaf)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $airplaneTtaf)) {
                $airplaneTtaf = str_replace('*', '%', $airplaneTtaf);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirplanePeer::AIRPLANE_TTAF, $airplaneTtaf, $comparison);
    }

    /**
     * Filter the query on the number_of_seats_available column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfSeatsAvailable(1234); // WHERE number_of_seats_available = 1234
     * $query->filterByNumberOfSeatsAvailable(array(12, 34)); // WHERE number_of_seats_available IN (12, 34)
     * $query->filterByNumberOfSeatsAvailable(array('min' => 12)); // WHERE number_of_seats_available >= 12
     * $query->filterByNumberOfSeatsAvailable(array('max' => 12)); // WHERE number_of_seats_available <= 12
     * </code>
     *
     * @param     mixed $numberOfSeatsAvailable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByNumberOfSeatsAvailable($numberOfSeatsAvailable = null, $comparison = null)
    {
        if (is_array($numberOfSeatsAvailable)) {
            $useMinMax = false;
            if (isset($numberOfSeatsAvailable['min'])) {
                $this->addUsingAlias(AirplanePeer::NUMBER_OF_SEATS_AVAILABLE, $numberOfSeatsAvailable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberOfSeatsAvailable['max'])) {
                $this->addUsingAlias(AirplanePeer::NUMBER_OF_SEATS_AVAILABLE, $numberOfSeatsAvailable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::NUMBER_OF_SEATS_AVAILABLE, $numberOfSeatsAvailable, $comparison);
    }

    /**
     * Filter the query on the number_of_seats column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfSeats(1234); // WHERE number_of_seats = 1234
     * $query->filterByNumberOfSeats(array(12, 34)); // WHERE number_of_seats IN (12, 34)
     * $query->filterByNumberOfSeats(array('min' => 12)); // WHERE number_of_seats >= 12
     * $query->filterByNumberOfSeats(array('max' => 12)); // WHERE number_of_seats <= 12
     * </code>
     *
     * @param     mixed $numberOfSeats The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByNumberOfSeats($numberOfSeats = null, $comparison = null)
    {
        if (is_array($numberOfSeats)) {
            $useMinMax = false;
            if (isset($numberOfSeats['min'])) {
                $this->addUsingAlias(AirplanePeer::NUMBER_OF_SEATS, $numberOfSeats['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberOfSeats['max'])) {
                $this->addUsingAlias(AirplanePeer::NUMBER_OF_SEATS, $numberOfSeats['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::NUMBER_OF_SEATS, $numberOfSeats, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirplanePeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AirplanePeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AirplanePeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AirplanePeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AirplanePeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirplanePeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related File object
     *
     * @param   File|PropelObjectCollection $file The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AirplaneQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFile($file, $comparison = null)
    {
        if ($file instanceof File) {
            return $this
                    ->addUsingAlias(AirplanePeer::ID_IMAGE, $file->getId(), $comparison);
        } elseif ($file instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(AirplanePeer::ID_IMAGE, $file->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFile() only accepts arguments of type File or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the File relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function joinFile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('File');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'File');
        }

        return $this;
    }

    /**
     * Use the File relation File object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\FileQuery A secondary query class using the current class as primary query
     */
    public function useFileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinFile($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'File', '\Lamk\PrivatePilotBundle\Model\FileQuery');
    }

    /**
     * Filter the query by a related FlightRoute object
     *
     * @param   FlightRoute|PropelObjectCollection $flightRoute  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AirplaneQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFlightRoute($flightRoute, $comparison = null)
    {
        if ($flightRoute instanceof FlightRoute) {
            return $this
                    ->addUsingAlias(AirplanePeer::ID, $flightRoute->getIdPlane(), $comparison);
        } elseif ($flightRoute instanceof PropelObjectCollection) {
            return $this
                    ->useFlightRouteQuery()
                    ->filterByPrimaryKeys($flightRoute->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByFlightRoute() only accepts arguments of type FlightRoute or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlightRoute relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function joinFlightRoute($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlightRoute');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlightRoute');
        }

        return $this;
    }

    /**
     * Use the FlightRoute relation FlightRoute object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\FlightRouteQuery A secondary query class using the current class as primary query
     */
    public function useFlightRouteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinFlightRoute($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'FlightRoute', '\Lamk\PrivatePilotBundle\Model\FlightRouteQuery');
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AirplaneQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                    ->addUsingAlias(AirplanePeer::ID, $user->getIdPlane(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            return $this
                    ->useUserQuery()
                    ->filterByPrimaryKeys($user->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinUser($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'User', '\Lamk\PrivatePilotBundle\Model\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Airplane $airplane Object to remove from the list of results
     *
     * @return AirplaneQuery The current query, for fluid interface
     */
    public function prune($airplane = null)
    {
        if ($airplane) {
            $this->addUsingAlias(AirplanePeer::ID, $airplane->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }
    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     AirplaneQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(AirplanePeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     AirplaneQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(AirplanePeer::UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     AirplaneQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(AirplanePeer::UPDATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     AirplaneQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(AirplanePeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date desc
     *
     * @return     AirplaneQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(AirplanePeer::CREATED_AT);
    }

    /**
     * Order by create date asc
     *
     * @return     AirplaneQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(AirplanePeer::CREATED_AT);
    }
}
