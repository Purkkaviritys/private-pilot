<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\AirplaneQuery;
use Lamk\PrivatePilotBundle\Model\File;
use Lamk\PrivatePilotBundle\Model\FileQuery;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserComment;
use Lamk\PrivatePilotBundle\Model\UserCommentQuery;
use Lamk\PrivatePilotBundle\Model\UserPeer;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoQuery;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\UserRole;
use Lamk\PrivatePilotBundle\Model\UserRoleQuery;

abstract class BaseUser extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Lamk\\PrivatePilotBundle\\Model\\UserPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        UserPeer
     */
    protected static $peer;
    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;
    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;
    /**
     * The value for the active field.
     * @var        boolean
     */
    protected $active;
    /**
     * The value for the id_image field.
     * @var        int
     */
    protected $id_image;
    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;
    /**
     * The value for the email_address field.
     * @var        string
     */
    protected $email_address;
    /**
     * The value for the id_role field.
     * @var        int
     */
    protected $id_role;
    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;
    /**
     * The value for the salt field.
     * @var        string
     */
    protected $salt;
    /**
     * The value for the id_plane field.
     * @var        int
     */
    protected $id_plane;
    /**
     * The value for the id_personal_info field.
     * @var        int
     */
    protected $id_personal_info;
    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;
    /**
     * The value for the timezone field.
     * @var        string
     */
    protected $timezone;
    /**
     * The value for the last_login field.
     * @var        string
     */
    protected $last_login;
    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;
    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;
    /**
     * @var        File
     */
    protected $aFile;
    /**
     * @var        UserRole
     */
    protected $aUserRole;
    /**
     * @var        Airplane
     */
    protected $aAirplane;
    /**
     * @var        UserPersonalInfo
     */
    protected $aUserPersonalInfo;
    /**
     * @var        PropelObjectCollection|FlightRoute[] Collection to store aggregation of FlightRoute objects.
     */
    protected $collFlightRoutes;
    protected $collFlightRoutesPartial;
    /**
     * @var        PropelObjectCollection|UserComment[] Collection to store aggregation of UserComment objects.
     */
    protected $collUserComments;
    protected $collUserCommentsPartial;
    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;
    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;
    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $flightRoutesScheduledForDeletion = null;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $userCommentsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [active] column value.
     *
     * @return boolean
     */
    public function getActive()
    {

        return $this->active;
    }

    /**
     * Get the [id_image] column value.
     *
     * @return int
     */
    public function getIdImage()
    {

        return $this->id_image;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {

        return $this->username;
    }

    /**
     * Get the [email_address] column value.
     *
     * @return string
     */
    public function getEmailAddress()
    {

        return $this->email_address;
    }

    /**
     * Get the [id_role] column value.
     *
     * @return int
     */
    public function getIdRole()
    {

        return $this->id_role;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Get the [salt] column value.
     *
     * @return string
     */
    public function getSalt()
    {

        return $this->salt;
    }

    /**
     * Get the [id_plane] column value.
     *
     * @return int
     */
    public function getIdPlane()
    {

        return $this->id_plane;
    }

    /**
     * Get the [id_personal_info] column value.
     *
     * @return int
     */
    public function getIdPersonalInfo()
    {

        return $this->id_personal_info;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [timezone] column value.
     *
     * @return string
     */
    public function getTimezone()
    {

        return $this->timezone;
    }

    /**
     * Get the [optionally formatted] temporal [last_login] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastLogin($format = null)
    {
        if ($this->last_login === null) {
            return null;
        }

        if ($this->last_login === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->last_login);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_login, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($this->updated_at === null) {
            return null;
        }

        if ($this->updated_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = UserPeer::ID;
        }


        return $this;
    }
// setId()

    /**
     * Sets the value of the [active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return User The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[] = UserPeer::ACTIVE;
        }


        return $this;
    }
// setActive()

    /**
     * Set the value of [id_image] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setIdImage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_image !== $v) {
            $this->id_image = $v;
            $this->modifiedColumns[] = UserPeer::ID_IMAGE;
        }

        if ($this->aFile !== null && $this->aFile->getId() !== $v) {
            $this->aFile = null;
        }


        return $this;
    }
// setIdImage()

    /**
     * Set the value of [username] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = UserPeer::USERNAME;
        }


        return $this;
    }
// setUsername()

    /**
     * Set the value of [email_address] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setEmailAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_address !== $v) {
            $this->email_address = $v;
            $this->modifiedColumns[] = UserPeer::EMAIL_ADDRESS;
        }


        return $this;
    }
// setEmailAddress()

    /**
     * Set the value of [id_role] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setIdRole($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_role !== $v) {
            $this->id_role = $v;
            $this->modifiedColumns[] = UserPeer::ID_ROLE;
        }

        if ($this->aUserRole !== null && $this->aUserRole->getId() !== $v) {
            $this->aUserRole = null;
        }


        return $this;
    }
// setIdRole()

    /**
     * Set the value of [password] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = UserPeer::PASSWORD;
        }


        return $this;
    }
// setPassword()

    /**
     * Set the value of [salt] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setSalt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salt !== $v) {
            $this->salt = $v;
            $this->modifiedColumns[] = UserPeer::SALT;
        }


        return $this;
    }
// setSalt()

    /**
     * Set the value of [id_plane] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setIdPlane($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_plane !== $v) {
            $this->id_plane = $v;
            $this->modifiedColumns[] = UserPeer::ID_PLANE;
        }

        if ($this->aAirplane !== null && $this->aAirplane->getId() !== $v) {
            $this->aAirplane = null;
        }


        return $this;
    }
// setIdPlane()

    /**
     * Set the value of [id_personal_info] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setIdPersonalInfo($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_personal_info !== $v) {
            $this->id_personal_info = $v;
            $this->modifiedColumns[] = UserPeer::ID_PERSONAL_INFO;
        }

        if ($this->aUserPersonalInfo !== null && $this->aUserPersonalInfo->getId() !== $v) {
            $this->aUserPersonalInfo = null;
        }


        return $this;
    }
// setIdPersonalInfo()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = UserPeer::DESCRIPTION;
        }


        return $this;
    }
// setDescription()

    /**
     * Set the value of [timezone] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setTimezone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->timezone !== $v) {
            $this->timezone = $v;
            $this->modifiedColumns[] = UserPeer::TIMEZONE;
        }


        return $this;
    }
// setTimezone()

    /**
     * Sets the value of [last_login] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return User The current object (for fluent API support)
     */
    public function setLastLogin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_login !== null || $dt !== null) {
            $currentDateAsString = ($this->last_login !== null && $tmpDt = new DateTime($this->last_login)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_login = $newDateAsString;
                $this->modifiedColumns[] = UserPeer::LAST_LOGIN;
            }
        } // if either are not null


        return $this;
    }
// setLastLogin()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return User The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = UserPeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return User The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = UserPeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    }
// hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->active = ($row[$startcol + 1] !== null) ? (boolean) $row[$startcol + 1] : null;
            $this->id_image = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->username = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->email_address = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->id_role = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->password = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->salt = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->id_plane = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->id_personal_info = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->description = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->timezone = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->last_login = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->created_at = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->updated_at = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 15; // 15 = UserPeer::NUM_HYDRATE_COLUMNS.
        } catch (Exception $e) {
            throw new PropelException("Error populating User object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aFile !== null && $this->id_image !== $this->aFile->getId()) {
            $this->aFile = null;
        }
        if ($this->aUserRole !== null && $this->id_role !== $this->aUserRole->getId()) {
            $this->aUserRole = null;
        }
        if ($this->aAirplane !== null && $this->id_plane !== $this->aAirplane->getId()) {
            $this->aAirplane = null;
        }
        if ($this->aUserPersonalInfo !== null && $this->id_personal_info !== $this->aUserPersonalInfo->getId()) {
            $this->aUserPersonalInfo = null;
        }
    }
// ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = UserPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?
            $this->aFile = null;
            $this->aUserRole = null;
            $this->aAirplane = null;
            $this->aUserPersonalInfo = null;
            $this->collFlightRoutes = null;

            $this->collUserComments = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = UserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                if (!$this->isColumnModified(UserPeer::CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(UserPeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(UserPeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFile !== null) {
                if ($this->aFile->isModified() || $this->aFile->isNew()) {
                    $affectedRows += $this->aFile->save($con);
                }
                $this->setFile($this->aFile);
            }

            if ($this->aUserRole !== null) {
                if ($this->aUserRole->isModified() || $this->aUserRole->isNew()) {
                    $affectedRows += $this->aUserRole->save($con);
                }
                $this->setUserRole($this->aUserRole);
            }

            if ($this->aAirplane !== null) {
                if ($this->aAirplane->isModified() || $this->aAirplane->isNew()) {
                    $affectedRows += $this->aAirplane->save($con);
                }
                $this->setAirplane($this->aAirplane);
            }

            if ($this->aUserPersonalInfo !== null) {
                if ($this->aUserPersonalInfo->isModified() || $this->aUserPersonalInfo->isNew()) {
                    $affectedRows += $this->aUserPersonalInfo->save($con);
                }
                $this->setUserPersonalInfo($this->aUserPersonalInfo);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->flightRoutesScheduledForDeletion !== null) {
                if (!$this->flightRoutesScheduledForDeletion->isEmpty()) {
                    foreach ($this->flightRoutesScheduledForDeletion as $flightRoute) {
                        // need to save related object because we set the relation to null
                        $flightRoute->save($con);
                    }
                    $this->flightRoutesScheduledForDeletion = null;
                }
            }

            if ($this->collFlightRoutes !== null) {
                foreach ($this->collFlightRoutes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userCommentsScheduledForDeletion !== null) {
                if (!$this->userCommentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->userCommentsScheduledForDeletion as $userComment) {
                        // need to save related object because we set the relation to null
                        $userComment->save($con);
                    }
                    $this->userCommentsScheduledForDeletion = null;
                }
            }

            if ($this->collUserComments !== null) {
                foreach ($this->collUserComments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;
        }

        return $affectedRows;
    }
// doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = UserPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserPeer::ID . ')');
        }

        // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserPeer::ID)) {
            $modifiedColumns[':p' . $index++] = '`id`';
        }
        if ($this->isColumnModified(UserPeer::ACTIVE)) {
            $modifiedColumns[':p' . $index++] = '`active`';
        }
        if ($this->isColumnModified(UserPeer::ID_IMAGE)) {
            $modifiedColumns[':p' . $index++] = '`id_image`';
        }
        if ($this->isColumnModified(UserPeer::USERNAME)) {
            $modifiedColumns[':p' . $index++] = '`username`';
        }
        if ($this->isColumnModified(UserPeer::EMAIL_ADDRESS)) {
            $modifiedColumns[':p' . $index++] = '`email_address`';
        }
        if ($this->isColumnModified(UserPeer::ID_ROLE)) {
            $modifiedColumns[':p' . $index++] = '`id_role`';
        }
        if ($this->isColumnModified(UserPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++] = '`password`';
        }
        if ($this->isColumnModified(UserPeer::SALT)) {
            $modifiedColumns[':p' . $index++] = '`salt`';
        }
        if ($this->isColumnModified(UserPeer::ID_PLANE)) {
            $modifiedColumns[':p' . $index++] = '`id_plane`';
        }
        if ($this->isColumnModified(UserPeer::ID_PERSONAL_INFO)) {
            $modifiedColumns[':p' . $index++] = '`id_personal_info`';
        }
        if ($this->isColumnModified(UserPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++] = '`description`';
        }
        if ($this->isColumnModified(UserPeer::TIMEZONE)) {
            $modifiedColumns[':p' . $index++] = '`timezone`';
        }
        if ($this->isColumnModified(UserPeer::LAST_LOGIN)) {
            $modifiedColumns[':p' . $index++] = '`last_login`';
        }
        if ($this->isColumnModified(UserPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`created_at`';
        }
        if ($this->isColumnModified(UserPeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `user` (%s) VALUES (%s)', implode(', ', $modifiedColumns), implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`active`':
                        $stmt->bindValue($identifier, (int) $this->active, PDO::PARAM_INT);
                        break;
                    case '`id_image`':
                        $stmt->bindValue($identifier, $this->id_image, PDO::PARAM_INT);
                        break;
                    case '`username`':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case '`email_address`':
                        $stmt->bindValue($identifier, $this->email_address, PDO::PARAM_STR);
                        break;
                    case '`id_role`':
                        $stmt->bindValue($identifier, $this->id_role, PDO::PARAM_INT);
                        break;
                    case '`password`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case '`salt`':
                        $stmt->bindValue($identifier, $this->salt, PDO::PARAM_STR);
                        break;
                    case '`id_plane`':
                        $stmt->bindValue($identifier, $this->id_plane, PDO::PARAM_INT);
                        break;
                    case '`id_personal_info`':
                        $stmt->bindValue($identifier, $this->id_personal_info, PDO::PARAM_INT);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`timezone`':
                        $stmt->bindValue($identifier, $this->timezone, PDO::PARAM_STR);
                        break;
                    case '`last_login`':
                        $stmt->bindValue($identifier, $this->last_login, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }
    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFile !== null) {
                if (!$this->aFile->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aFile->getValidationFailures());
                }
            }

            if ($this->aUserRole !== null) {
                if (!$this->aUserRole->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aUserRole->getValidationFailures());
                }
            }

            if ($this->aAirplane !== null) {
                if (!$this->aAirplane->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAirplane->getValidationFailures());
                }
            }

            if ($this->aUserPersonalInfo !== null) {
                if (!$this->aUserPersonalInfo->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aUserPersonalInfo->getValidationFailures());
                }
            }


            if (($retval = UserPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


            if ($this->collFlightRoutes !== null) {
                foreach ($this->collFlightRoutes as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }

            if ($this->collUserComments !== null) {
                foreach ($this->collUserComments as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = UserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getActive();
                break;
            case 2:
                return $this->getIdImage();
                break;
            case 3:
                return $this->getUsername();
                break;
            case 4:
                return $this->getEmailAddress();
                break;
            case 5:
                return $this->getIdRole();
                break;
            case 6:
                return $this->getPassword();
                break;
            case 7:
                return $this->getSalt();
                break;
            case 8:
                return $this->getIdPlane();
                break;
            case 9:
                return $this->getIdPersonalInfo();
                break;
            case 10:
                return $this->getDescription();
                break;
            case 11:
                return $this->getTimezone();
                break;
            case 12:
                return $this->getLastLogin();
                break;
            case 13:
                return $this->getCreatedAt();
                break;
            case 14:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['User'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['User'][$this->getPrimaryKey()] = true;
        $keys = UserPeer::getFieldNames($keyType);
        $result = array(
          $keys[0]  => $this->getId(),
          $keys[1]  => $this->getActive(),
          $keys[2]  => $this->getIdImage(),
          $keys[3]  => $this->getUsername(),
          $keys[4]  => $this->getEmailAddress(),
          $keys[5]  => $this->getIdRole(),
          $keys[6]  => $this->getPassword(),
          $keys[7]  => $this->getSalt(),
          $keys[8]  => $this->getIdPlane(),
          $keys[9]  => $this->getIdPersonalInfo(),
          $keys[10] => $this->getDescription(),
          $keys[11] => $this->getTimezone(),
          $keys[12] => $this->getLastLogin(),
          $keys[13] => $this->getCreatedAt(),
          $keys[14] => $this->getUpdatedAt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFile) {
                $result['File'] = $this->aFile->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUserRole) {
                $result['UserRole'] = $this->aUserRole->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAirplane) {
                $result['Airplane'] = $this->aAirplane->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUserPersonalInfo) {
                $result['UserPersonalInfo'] = $this->aUserPersonalInfo->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collFlightRoutes) {
                $result['FlightRoutes'] = $this->collFlightRoutes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserComments) {
                $result['UserComments'] = $this->collUserComments->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = UserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setActive($value);
                break;
            case 2:
                $this->setIdImage($value);
                break;
            case 3:
                $this->setUsername($value);
                break;
            case 4:
                $this->setEmailAddress($value);
                break;
            case 5:
                $this->setIdRole($value);
                break;
            case 6:
                $this->setPassword($value);
                break;
            case 7:
                $this->setSalt($value);
                break;
            case 8:
                $this->setIdPlane($value);
                break;
            case 9:
                $this->setIdPersonalInfo($value);
                break;
            case 10:
                $this->setDescription($value);
                break;
            case 11:
                $this->setTimezone($value);
                break;
            case 12:
                $this->setLastLogin($value);
                break;
            case 13:
                $this->setCreatedAt($value);
                break;
            case 14:
                $this->setUpdatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = UserPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr))
            $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr))
            $this->setActive($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr))
            $this->setIdImage($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr))
            $this->setUsername($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr))
            $this->setEmailAddress($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr))
            $this->setIdRole($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr))
            $this->setPassword($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr))
            $this->setSalt($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr))
            $this->setIdPlane($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr))
            $this->setIdPersonalInfo($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr))
            $this->setDescription($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr))
            $this->setTimezone($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr))
            $this->setLastLogin($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr))
            $this->setCreatedAt($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr))
            $this->setUpdatedAt($arr[$keys[14]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserPeer::DATABASE_NAME);

        if ($this->isColumnModified(UserPeer::ID))
            $criteria->add(UserPeer::ID, $this->id);
        if ($this->isColumnModified(UserPeer::ACTIVE))
            $criteria->add(UserPeer::ACTIVE, $this->active);
        if ($this->isColumnModified(UserPeer::ID_IMAGE))
            $criteria->add(UserPeer::ID_IMAGE, $this->id_image);
        if ($this->isColumnModified(UserPeer::USERNAME))
            $criteria->add(UserPeer::USERNAME, $this->username);
        if ($this->isColumnModified(UserPeer::EMAIL_ADDRESS))
            $criteria->add(UserPeer::EMAIL_ADDRESS, $this->email_address);
        if ($this->isColumnModified(UserPeer::ID_ROLE))
            $criteria->add(UserPeer::ID_ROLE, $this->id_role);
        if ($this->isColumnModified(UserPeer::PASSWORD))
            $criteria->add(UserPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(UserPeer::SALT))
            $criteria->add(UserPeer::SALT, $this->salt);
        if ($this->isColumnModified(UserPeer::ID_PLANE))
            $criteria->add(UserPeer::ID_PLANE, $this->id_plane);
        if ($this->isColumnModified(UserPeer::ID_PERSONAL_INFO))
            $criteria->add(UserPeer::ID_PERSONAL_INFO, $this->id_personal_info);
        if ($this->isColumnModified(UserPeer::DESCRIPTION))
            $criteria->add(UserPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(UserPeer::TIMEZONE))
            $criteria->add(UserPeer::TIMEZONE, $this->timezone);
        if ($this->isColumnModified(UserPeer::LAST_LOGIN))
            $criteria->add(UserPeer::LAST_LOGIN, $this->last_login);
        if ($this->isColumnModified(UserPeer::CREATED_AT))
            $criteria->add(UserPeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(UserPeer::UPDATED_AT))
            $criteria->add(UserPeer::UPDATED_AT, $this->updated_at);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(UserPeer::DATABASE_NAME);
        $criteria->add(UserPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of User (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setActive($this->getActive());
        $copyObj->setIdImage($this->getIdImage());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setEmailAddress($this->getEmailAddress());
        $copyObj->setIdRole($this->getIdRole());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setSalt($this->getSalt());
        $copyObj->setIdPlane($this->getIdPlane());
        $copyObj->setIdPersonalInfo($this->getIdPersonalInfo());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setTimezone($this->getTimezone());
        $copyObj->setLastLogin($this->getLastLogin());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getFlightRoutes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlightRoute($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserComments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserComment($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return User Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return UserPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new UserPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a File object.
     *
     * @param                  File $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFile(File $v = null)
    {
        if ($v === null) {
            $this->setIdImage(NULL);
        } else {
            $this->setIdImage($v->getId());
        }

        $this->aFile = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the File object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }

    /**
     * Get the associated File object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return File The associated File object.
     * @throws PropelException
     */
    public function getFile(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aFile === null && ($this->id_image !== null) && $doQuery) {
            $this->aFile = FileQuery::create()->findPk($this->id_image, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aFile->addUsers($this);
             */
        }

        return $this->aFile;
    }

    /**
     * Declares an association between this object and a UserRole object.
     *
     * @param                  UserRole $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUserRole(UserRole $v = null)
    {
        if ($v === null) {
            $this->setIdRole(NULL);
        } else {
            $this->setIdRole($v->getId());
        }

        $this->aUserRole = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the UserRole object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }

    /**
     * Get the associated UserRole object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return UserRole The associated UserRole object.
     * @throws PropelException
     */
    public function getUserRole(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUserRole === null && ($this->id_role !== null) && $doQuery) {
            $this->aUserRole = UserRoleQuery::create()->findPk($this->id_role, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aUserRole->addUsers($this);
             */
        }

        return $this->aUserRole;
    }

    /**
     * Declares an association between this object and a Airplane object.
     *
     * @param                  Airplane $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAirplane(Airplane $v = null)
    {
        if ($v === null) {
            $this->setIdPlane(NULL);
        } else {
            $this->setIdPlane($v->getId());
        }

        $this->aAirplane = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Airplane object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }

    /**
     * Get the associated Airplane object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Airplane The associated Airplane object.
     * @throws PropelException
     */
    public function getAirplane(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAirplane === null && ($this->id_plane !== null) && $doQuery) {
            $this->aAirplane = AirplaneQuery::create()->findPk($this->id_plane, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aAirplane->addUsers($this);
             */
        }

        return $this->aAirplane;
    }

    /**
     * Declares an association between this object and a UserPersonalInfo object.
     *
     * @param                  UserPersonalInfo $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUserPersonalInfo(UserPersonalInfo $v = null)
    {
        if ($v === null) {
            $this->setIdPersonalInfo(NULL);
        } else {
            $this->setIdPersonalInfo($v->getId());
        }

        $this->aUserPersonalInfo = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the UserPersonalInfo object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }

    /**
     * Get the associated UserPersonalInfo object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return UserPersonalInfo The associated UserPersonalInfo object.
     * @throws PropelException
     */
    public function getUserPersonalInfo(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUserPersonalInfo === null && ($this->id_personal_info !== null) && $doQuery) {
            $this->aUserPersonalInfo = UserPersonalInfoQuery::create()->findPk($this->id_personal_info, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aUserPersonalInfo->addUsers($this);
             */
        }

        return $this->aUserPersonalInfo;
    }

    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('FlightRoute' == $relationName) {
            $this->initFlightRoutes();
        }
        if ('UserComment' == $relationName) {
            $this->initUserComments();
        }
    }

    /**
     * Clears out the collFlightRoutes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addFlightRoutes()
     */
    public function clearFlightRoutes()
    {
        $this->collFlightRoutes = null; // important to set this to null since that means it is uninitialized
        $this->collFlightRoutesPartial = null;

        return $this;
    }

    /**
     * reset is the collFlightRoutes collection loaded partially
     *
     * @return void
     */
    public function resetPartialFlightRoutes($v = true)
    {
        $this->collFlightRoutesPartial = $v;
    }

    /**
     * Initializes the collFlightRoutes collection.
     *
     * By default this just sets the collFlightRoutes collection to an empty array (like clearcollFlightRoutes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlightRoutes($overrideExisting = true)
    {
        if (null !== $this->collFlightRoutes && !$overrideExisting) {
            return;
        }
        $this->collFlightRoutes = new PropelObjectCollection();
        $this->collFlightRoutes->setModel('FlightRoute');
    }

    /**
     * Gets an array of FlightRoute objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     * @throws PropelException
     */
    public function getFlightRoutes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesPartial && !$this->isNew();
        if (null === $this->collFlightRoutes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutes) {
                // return empty collection
                $this->initFlightRoutes();
            } else {
                $collFlightRoutes = FlightRouteQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collFlightRoutesPartial && count($collFlightRoutes)) {
                        $this->initFlightRoutes(false);

                        foreach ($collFlightRoutes as $obj) {
                            if (false == $this->collFlightRoutes->contains($obj)) {
                                $this->collFlightRoutes->append($obj);
                            }
                        }

                        $this->collFlightRoutesPartial = true;
                    }

                    $collFlightRoutes->getInternalIterator()->rewind();

                    return $collFlightRoutes;
                }

                if ($partial && $this->collFlightRoutes) {
                    foreach ($this->collFlightRoutes as $obj) {
                        if ($obj->isNew()) {
                            $collFlightRoutes[] = $obj;
                        }
                    }
                }

                $this->collFlightRoutes = $collFlightRoutes;
                $this->collFlightRoutesPartial = false;
            }
        }

        return $this->collFlightRoutes;
    }

    /**
     * Sets a collection of FlightRoute objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $flightRoutes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setFlightRoutes(PropelCollection $flightRoutes, PropelPDO $con = null)
    {
        $flightRoutesToDelete = $this->getFlightRoutes(new Criteria(), $con)->diff($flightRoutes);


        $this->flightRoutesScheduledForDeletion = $flightRoutesToDelete;

        foreach ($flightRoutesToDelete as $flightRouteRemoved) {
            $flightRouteRemoved->setUser(null);
        }

        $this->collFlightRoutes = null;
        foreach ($flightRoutes as $flightRoute) {
            $this->addFlightRoute($flightRoute);
        }

        $this->collFlightRoutes = $flightRoutes;
        $this->collFlightRoutesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlightRoute objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related FlightRoute objects.
     * @throws PropelException
     */
    public function countFlightRoutes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesPartial && !$this->isNew();
        if (null === $this->collFlightRoutes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlightRoutes());
            }
            $query = FlightRouteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByUser($this)
                    ->count($con);
        }

        return count($this->collFlightRoutes);
    }

    /**
     * Method called to associate a FlightRoute object to this object
     * through the FlightRoute foreign key attribute.
     *
     * @param    FlightRoute $l FlightRoute
     * @return User The current object (for fluent API support)
     */
    public function addFlightRoute(FlightRoute $l)
    {
        if ($this->collFlightRoutes === null) {
            $this->initFlightRoutes();
            $this->collFlightRoutesPartial = true;
        }

        if (!in_array($l, $this->collFlightRoutes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddFlightRoute($l);

            if ($this->flightRoutesScheduledForDeletion and $this->flightRoutesScheduledForDeletion->contains($l)) {
                $this->flightRoutesScheduledForDeletion->remove($this->flightRoutesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	FlightRoute $flightRoute The flightRoute object to add.
     */
    protected function doAddFlightRoute($flightRoute)
    {
        $this->collFlightRoutes[] = $flightRoute;
        $flightRoute->setUser($this);
    }

    /**
     * @param	FlightRoute $flightRoute The flightRoute object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeFlightRoute($flightRoute)
    {
        if ($this->getFlightRoutes()->contains($flightRoute)) {
            $this->collFlightRoutes->remove($this->collFlightRoutes->search($flightRoute));
            if (null === $this->flightRoutesScheduledForDeletion) {
                $this->flightRoutesScheduledForDeletion = clone $this->collFlightRoutes;
                $this->flightRoutesScheduledForDeletion->clear();
            }
            $this->flightRoutesScheduledForDeletion[] = $flightRoute;
            $flightRoute->setUser(null);
        }

        return $this;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related FlightRoutes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesJoinAirplane($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('Airplane', $join_behavior);

        return $this->getFlightRoutes($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related FlightRoutes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesJoinAirportRelatedByIdDepartureLocation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('AirportRelatedByIdDepartureLocation', $join_behavior);

        return $this->getFlightRoutes($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related FlightRoutes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesJoinAirportRelatedByIdDestinationLocation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('AirportRelatedByIdDestinationLocation', $join_behavior);

        return $this->getFlightRoutes($query, $con);
    }

    /**
     * Clears out the collUserComments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addUserComments()
     */
    public function clearUserComments()
    {
        $this->collUserComments = null; // important to set this to null since that means it is uninitialized
        $this->collUserCommentsPartial = null;

        return $this;
    }

    /**
     * reset is the collUserComments collection loaded partially
     *
     * @return void
     */
    public function resetPartialUserComments($v = true)
    {
        $this->collUserCommentsPartial = $v;
    }

    /**
     * Initializes the collUserComments collection.
     *
     * By default this just sets the collUserComments collection to an empty array (like clearcollUserComments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserComments($overrideExisting = true)
    {
        if (null !== $this->collUserComments && !$overrideExisting) {
            return;
        }
        $this->collUserComments = new PropelObjectCollection();
        $this->collUserComments->setModel('UserComment');
    }

    /**
     * Gets an array of UserComment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UserComment[] List of UserComment objects
     * @throws PropelException
     */
    public function getUserComments($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUserCommentsPartial && !$this->isNew();
        if (null === $this->collUserComments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserComments) {
                // return empty collection
                $this->initUserComments();
            } else {
                $collUserComments = UserCommentQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUserCommentsPartial && count($collUserComments)) {
                        $this->initUserComments(false);

                        foreach ($collUserComments as $obj) {
                            if (false == $this->collUserComments->contains($obj)) {
                                $this->collUserComments->append($obj);
                            }
                        }

                        $this->collUserCommentsPartial = true;
                    }

                    $collUserComments->getInternalIterator()->rewind();

                    return $collUserComments;
                }

                if ($partial && $this->collUserComments) {
                    foreach ($this->collUserComments as $obj) {
                        if ($obj->isNew()) {
                            $collUserComments[] = $obj;
                        }
                    }
                }

                $this->collUserComments = $collUserComments;
                $this->collUserCommentsPartial = false;
            }
        }

        return $this->collUserComments;
    }

    /**
     * Sets a collection of UserComment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $userComments A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setUserComments(PropelCollection $userComments, PropelPDO $con = null)
    {
        $userCommentsToDelete = $this->getUserComments(new Criteria(), $con)->diff($userComments);


        $this->userCommentsScheduledForDeletion = $userCommentsToDelete;

        foreach ($userCommentsToDelete as $userCommentRemoved) {
            $userCommentRemoved->setUser(null);
        }

        $this->collUserComments = null;
        foreach ($userComments as $userComment) {
            $this->addUserComment($userComment);
        }

        $this->collUserComments = $userComments;
        $this->collUserCommentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserComment objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UserComment objects.
     * @throws PropelException
     */
    public function countUserComments(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUserCommentsPartial && !$this->isNew();
        if (null === $this->collUserComments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserComments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserComments());
            }
            $query = UserCommentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByUser($this)
                    ->count($con);
        }

        return count($this->collUserComments);
    }

    /**
     * Method called to associate a UserComment object to this object
     * through the UserComment foreign key attribute.
     *
     * @param    UserComment $l UserComment
     * @return User The current object (for fluent API support)
     */
    public function addUserComment(UserComment $l)
    {
        if ($this->collUserComments === null) {
            $this->initUserComments();
            $this->collUserCommentsPartial = true;
        }

        if (!in_array($l, $this->collUserComments->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUserComment($l);

            if ($this->userCommentsScheduledForDeletion and $this->userCommentsScheduledForDeletion->contains($l)) {
                $this->userCommentsScheduledForDeletion->remove($this->userCommentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UserComment $userComment The userComment object to add.
     */
    protected function doAddUserComment($userComment)
    {
        $this->collUserComments[] = $userComment;
        $userComment->setUser($this);
    }

    /**
     * @param	UserComment $userComment The userComment object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeUserComment($userComment)
    {
        if ($this->getUserComments()->contains($userComment)) {
            $this->collUserComments->remove($this->collUserComments->search($userComment));
            if (null === $this->userCommentsScheduledForDeletion) {
                $this->userCommentsScheduledForDeletion = clone $this->collUserComments;
                $this->userCommentsScheduledForDeletion->clear();
            }
            $this->userCommentsScheduledForDeletion[] = $userComment;
            $userComment->setUser(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->active = null;
        $this->id_image = null;
        $this->username = null;
        $this->email_address = null;
        $this->id_role = null;
        $this->password = null;
        $this->salt = null;
        $this->id_plane = null;
        $this->id_personal_info = null;
        $this->description = null;
        $this->timezone = null;
        $this->last_login = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collFlightRoutes) {
                foreach ($this->collFlightRoutes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserComments) {
                foreach ($this->collUserComments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aFile instanceof Persistent) {
                $this->aFile->clearAllReferences($deep);
            }
            if ($this->aUserRole instanceof Persistent) {
                $this->aUserRole->clearAllReferences($deep);
            }
            if ($this->aAirplane instanceof Persistent) {
                $this->aAirplane->clearAllReferences($deep);
            }
            if ($this->aUserPersonalInfo instanceof Persistent) {
                $this->aUserPersonalInfo->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collFlightRoutes instanceof PropelCollection) {
            $this->collFlightRoutes->clearIterator();
        }
        $this->collFlightRoutes = null;
        if ($this->collUserComments instanceof PropelCollection) {
            $this->collUserComments->clearIterator();
        }
        $this->collUserComments = null;
        $this->aFile = null;
        $this->aUserRole = null;
        $this->aAirplane = null;
        $this->aUserPersonalInfo = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }
    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     User The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[] = UserPeer::UPDATED_AT;

        return $this;
    }
}
