<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\PaymentMethod;
use Lamk\PrivatePilotBundle\Model\ReceivedOrder;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderPeer;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;

/**
 * @method ReceivedOrderQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ReceivedOrderQuery orderByIdUserPersonalInfo($order = Criteria::ASC) Order by the id_user_personal_info column
 * @method ReceivedOrderQuery orderByIdFlightRoute($order = Criteria::ASC) Order by the id_flight_route column
 * @method ReceivedOrderQuery orderByIdOrderPaymentMethod($order = Criteria::ASC) Order by the id_order_payment_method column
 * @method ReceivedOrderQuery orderByNumberOfReservedSeats($order = Criteria::ASC) Order by the number_of_reserved_seats column
 * @method ReceivedOrderQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method ReceivedOrderQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method ReceivedOrderQuery groupById() Group by the id column
 * @method ReceivedOrderQuery groupByIdUserPersonalInfo() Group by the id_user_personal_info column
 * @method ReceivedOrderQuery groupByIdFlightRoute() Group by the id_flight_route column
 * @method ReceivedOrderQuery groupByIdOrderPaymentMethod() Group by the id_order_payment_method column
 * @method ReceivedOrderQuery groupByNumberOfReservedSeats() Group by the number_of_reserved_seats column
 * @method ReceivedOrderQuery groupByCreatedAt() Group by the created_at column
 * @method ReceivedOrderQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method ReceivedOrderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ReceivedOrderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ReceivedOrderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ReceivedOrderQuery leftJoinUserPersonalInfo($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserPersonalInfo relation
 * @method ReceivedOrderQuery rightJoinUserPersonalInfo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserPersonalInfo relation
 * @method ReceivedOrderQuery innerJoinUserPersonalInfo($relationAlias = null) Adds a INNER JOIN clause to the query using the UserPersonalInfo relation
 *
 * @method ReceivedOrderQuery leftJoinFlightRoute($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlightRoute relation
 * @method ReceivedOrderQuery rightJoinFlightRoute($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlightRoute relation
 * @method ReceivedOrderQuery innerJoinFlightRoute($relationAlias = null) Adds a INNER JOIN clause to the query using the FlightRoute relation
 *
 * @method ReceivedOrderQuery leftJoinPaymentMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the PaymentMethod relation
 * @method ReceivedOrderQuery rightJoinPaymentMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PaymentMethod relation
 * @method ReceivedOrderQuery innerJoinPaymentMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the PaymentMethod relation
 *
 * @method ReceivedOrder findOne(PropelPDO $con = null) Return the first ReceivedOrder matching the query
 * @method ReceivedOrder findOneOrCreate(PropelPDO $con = null) Return the first ReceivedOrder matching the query, or a new ReceivedOrder object populated from the query conditions when no match is found
 *
 * @method ReceivedOrder findOneByIdUserPersonalInfo(int $id_user_personal_info) Return the first ReceivedOrder filtered by the id_user_personal_info column
 * @method ReceivedOrder findOneByIdFlightRoute(int $id_flight_route) Return the first ReceivedOrder filtered by the id_flight_route column
 * @method ReceivedOrder findOneByIdOrderPaymentMethod(int $id_order_payment_method) Return the first ReceivedOrder filtered by the id_order_payment_method column
 * @method ReceivedOrder findOneByNumberOfReservedSeats(int $number_of_reserved_seats) Return the first ReceivedOrder filtered by the number_of_reserved_seats column
 * @method ReceivedOrder findOneByCreatedAt(string $created_at) Return the first ReceivedOrder filtered by the created_at column
 * @method ReceivedOrder findOneByUpdatedAt(string $updated_at) Return the first ReceivedOrder filtered by the updated_at column
 *
 * @method array findById(int $id) Return ReceivedOrder objects filtered by the id column
 * @method array findByIdUserPersonalInfo(int $id_user_personal_info) Return ReceivedOrder objects filtered by the id_user_personal_info column
 * @method array findByIdFlightRoute(int $id_flight_route) Return ReceivedOrder objects filtered by the id_flight_route column
 * @method array findByIdOrderPaymentMethod(int $id_order_payment_method) Return ReceivedOrder objects filtered by the id_order_payment_method column
 * @method array findByNumberOfReservedSeats(int $number_of_reserved_seats) Return ReceivedOrder objects filtered by the number_of_reserved_seats column
 * @method array findByCreatedAt(string $created_at) Return ReceivedOrder objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return ReceivedOrder objects filtered by the updated_at column
 */
abstract class BaseReceivedOrderQuery extends ModelCriteria
{

    /**
     * Initializes internal state of BaseReceivedOrderQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'private_pilot';
        }
        if (null === $modelName) {
            $modelName = 'Lamk\\PrivatePilotBundle\\Model\\ReceivedOrder';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ReceivedOrderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ReceivedOrderQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ReceivedOrderQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ReceivedOrderQuery) {
            return $criteria;
        }
        $query = new ReceivedOrderQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ReceivedOrder|ReceivedOrder[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ReceivedOrderPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ReceivedOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select || $this->selectColumns || $this->asColumns || $this->selectModifiers || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ReceivedOrder A model object, or null if the key is not found
     * @throws PropelException
     */
    public function findOneById($key, $con = null)
    {
        return $this->findPk($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ReceivedOrder A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_user_personal_info`, `id_flight_route`, `id_order_payment_method`, `number_of_reserved_seats`, `created_at`, `updated_at` FROM `received_order` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ReceivedOrder();
            $obj->hydrate($row);
            ReceivedOrderPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ReceivedOrder|ReceivedOrder[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ReceivedOrder[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ReceivedOrderPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ReceivedOrderPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_user_personal_info column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUserPersonalInfo(1234); // WHERE id_user_personal_info = 1234
     * $query->filterByIdUserPersonalInfo(array(12, 34)); // WHERE id_user_personal_info IN (12, 34)
     * $query->filterByIdUserPersonalInfo(array('min' => 12)); // WHERE id_user_personal_info >= 12
     * $query->filterByIdUserPersonalInfo(array('max' => 12)); // WHERE id_user_personal_info <= 12
     * </code>
     *
     * @see       filterByUserPersonalInfo()
     *
     * @param     mixed $idUserPersonalInfo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByIdUserPersonalInfo($idUserPersonalInfo = null, $comparison = null)
    {
        if (is_array($idUserPersonalInfo)) {
            $useMinMax = false;
            if (isset($idUserPersonalInfo['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, $idUserPersonalInfo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUserPersonalInfo['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, $idUserPersonalInfo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, $idUserPersonalInfo, $comparison);
    }

    /**
     * Filter the query on the id_flight_route column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFlightRoute(1234); // WHERE id_flight_route = 1234
     * $query->filterByIdFlightRoute(array(12, 34)); // WHERE id_flight_route IN (12, 34)
     * $query->filterByIdFlightRoute(array('min' => 12)); // WHERE id_flight_route >= 12
     * $query->filterByIdFlightRoute(array('max' => 12)); // WHERE id_flight_route <= 12
     * </code>
     *
     * @see       filterByFlightRoute()
     *
     * @param     mixed $idFlightRoute The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByIdFlightRoute($idFlightRoute = null, $comparison = null)
    {
        if (is_array($idFlightRoute)) {
            $useMinMax = false;
            if (isset($idFlightRoute['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID_FLIGHT_ROUTE, $idFlightRoute['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFlightRoute['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID_FLIGHT_ROUTE, $idFlightRoute['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::ID_FLIGHT_ROUTE, $idFlightRoute, $comparison);
    }

    /**
     * Filter the query on the id_order_payment_method column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOrderPaymentMethod(1234); // WHERE id_order_payment_method = 1234
     * $query->filterByIdOrderPaymentMethod(array(12, 34)); // WHERE id_order_payment_method IN (12, 34)
     * $query->filterByIdOrderPaymentMethod(array('min' => 12)); // WHERE id_order_payment_method >= 12
     * $query->filterByIdOrderPaymentMethod(array('max' => 12)); // WHERE id_order_payment_method <= 12
     * </code>
     *
     * @see       filterByPaymentMethod()
     *
     * @param     mixed $idOrderPaymentMethod The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByIdOrderPaymentMethod($idOrderPaymentMethod = null, $comparison = null)
    {
        if (is_array($idOrderPaymentMethod)) {
            $useMinMax = false;
            if (isset($idOrderPaymentMethod['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, $idOrderPaymentMethod['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOrderPaymentMethod['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, $idOrderPaymentMethod['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, $idOrderPaymentMethod, $comparison);
    }

    /**
     * Filter the query on the number_of_reserved_seats column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfReservedSeats(1234); // WHERE number_of_reserved_seats = 1234
     * $query->filterByNumberOfReservedSeats(array(12, 34)); // WHERE number_of_reserved_seats IN (12, 34)
     * $query->filterByNumberOfReservedSeats(array('min' => 12)); // WHERE number_of_reserved_seats >= 12
     * $query->filterByNumberOfReservedSeats(array('max' => 12)); // WHERE number_of_reserved_seats <= 12
     * </code>
     *
     * @param     mixed $numberOfReservedSeats The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByNumberOfReservedSeats($numberOfReservedSeats = null, $comparison = null)
    {
        if (is_array($numberOfReservedSeats)) {
            $useMinMax = false;
            if (isset($numberOfReservedSeats['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::NUMBER_OF_RESERVED_SEATS, $numberOfReservedSeats['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberOfReservedSeats['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::NUMBER_OF_RESERVED_SEATS, $numberOfReservedSeats['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::NUMBER_OF_RESERVED_SEATS, $numberOfReservedSeats, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ReceivedOrderPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ReceivedOrderPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceivedOrderPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related UserPersonalInfo object
     *
     * @param   UserPersonalInfo|PropelObjectCollection $userPersonalInfo The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ReceivedOrderQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserPersonalInfo($userPersonalInfo, $comparison = null)
    {
        if ($userPersonalInfo instanceof UserPersonalInfo) {
            return $this
                    ->addUsingAlias(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, $userPersonalInfo->getId(), $comparison);
        } elseif ($userPersonalInfo instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(ReceivedOrderPeer::ID_USER_PERSONAL_INFO, $userPersonalInfo->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserPersonalInfo() only accepts arguments of type UserPersonalInfo or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserPersonalInfo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function joinUserPersonalInfo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserPersonalInfo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserPersonalInfo');
        }

        return $this;
    }

    /**
     * Use the UserPersonalInfo relation UserPersonalInfo object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\UserPersonalInfoQuery A secondary query class using the current class as primary query
     */
    public function useUserPersonalInfoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinUserPersonalInfo($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'UserPersonalInfo', '\Lamk\PrivatePilotBundle\Model\UserPersonalInfoQuery');
    }

    /**
     * Filter the query by a related FlightRoute object
     *
     * @param   FlightRoute|PropelObjectCollection $flightRoute The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ReceivedOrderQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFlightRoute($flightRoute, $comparison = null)
    {
        if ($flightRoute instanceof FlightRoute) {
            return $this
                    ->addUsingAlias(ReceivedOrderPeer::ID_FLIGHT_ROUTE, $flightRoute->getId(), $comparison);
        } elseif ($flightRoute instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(ReceivedOrderPeer::ID_FLIGHT_ROUTE, $flightRoute->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFlightRoute() only accepts arguments of type FlightRoute or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlightRoute relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function joinFlightRoute($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlightRoute');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlightRoute');
        }

        return $this;
    }

    /**
     * Use the FlightRoute relation FlightRoute object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\FlightRouteQuery A secondary query class using the current class as primary query
     */
    public function useFlightRouteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinFlightRoute($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'FlightRoute', '\Lamk\PrivatePilotBundle\Model\FlightRouteQuery');
    }

    /**
     * Filter the query by a related PaymentMethod object
     *
     * @param   PaymentMethod|PropelObjectCollection $paymentMethod The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ReceivedOrderQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPaymentMethod($paymentMethod, $comparison = null)
    {
        if ($paymentMethod instanceof PaymentMethod) {
            return $this
                    ->addUsingAlias(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, $paymentMethod->getId(), $comparison);
        } elseif ($paymentMethod instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(ReceivedOrderPeer::ID_ORDER_PAYMENT_METHOD, $paymentMethod->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPaymentMethod() only accepts arguments of type PaymentMethod or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PaymentMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function joinPaymentMethod($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PaymentMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PaymentMethod');
        }

        return $this;
    }

    /**
     * Use the PaymentMethod relation PaymentMethod object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\PaymentMethodQuery A secondary query class using the current class as primary query
     */
    public function usePaymentMethodQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinPaymentMethod($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'PaymentMethod', '\Lamk\PrivatePilotBundle\Model\PaymentMethodQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ReceivedOrder $receivedOrder Object to remove from the list of results
     *
     * @return ReceivedOrderQuery The current query, for fluid interface
     */
    public function prune($receivedOrder = null)
    {
        if ($receivedOrder) {
            $this->addUsingAlias(ReceivedOrderPeer::ID, $receivedOrder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }
    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     ReceivedOrderQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ReceivedOrderPeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     ReceivedOrderQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ReceivedOrderPeer::UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     ReceivedOrderQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ReceivedOrderPeer::UPDATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     ReceivedOrderQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ReceivedOrderPeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date desc
     *
     * @return     ReceivedOrderQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ReceivedOrderPeer::CREATED_AT);
    }

    /**
     * Order by create date asc
     *
     * @return     ReceivedOrderQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ReceivedOrderPeer::CREATED_AT);
    }
}
