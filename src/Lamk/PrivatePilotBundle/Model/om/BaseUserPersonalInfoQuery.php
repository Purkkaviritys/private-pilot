<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\ReceivedOrder;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoPeer;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoQuery;

/**
 * @method UserPersonalInfoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method UserPersonalInfoQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method UserPersonalInfoQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method UserPersonalInfoQuery orderByLine1($order = Criteria::ASC) Order by the line1 column
 * @method UserPersonalInfoQuery orderByLine2($order = Criteria::ASC) Order by the line2 column
 * @method UserPersonalInfoQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method UserPersonalInfoQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method UserPersonalInfoQuery orderByPostalCode($order = Criteria::ASC) Order by the postal_code column
 * @method UserPersonalInfoQuery orderByCountryCode($order = Criteria::ASC) Order by the country_code column
 * @method UserPersonalInfoQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method UserPersonalInfoQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method UserPersonalInfoQuery groupById() Group by the id column
 * @method UserPersonalInfoQuery groupByFirstName() Group by the first_name column
 * @method UserPersonalInfoQuery groupByLastName() Group by the last_name column
 * @method UserPersonalInfoQuery groupByLine1() Group by the line1 column
 * @method UserPersonalInfoQuery groupByLine2() Group by the line2 column
 * @method UserPersonalInfoQuery groupByCity() Group by the city column
 * @method UserPersonalInfoQuery groupByState() Group by the state column
 * @method UserPersonalInfoQuery groupByPostalCode() Group by the postal_code column
 * @method UserPersonalInfoQuery groupByCountryCode() Group by the country_code column
 * @method UserPersonalInfoQuery groupByCreatedAt() Group by the created_at column
 * @method UserPersonalInfoQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method UserPersonalInfoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method UserPersonalInfoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method UserPersonalInfoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method UserPersonalInfoQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method UserPersonalInfoQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method UserPersonalInfoQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method UserPersonalInfoQuery leftJoinReceivedOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the ReceivedOrder relation
 * @method UserPersonalInfoQuery rightJoinReceivedOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ReceivedOrder relation
 * @method UserPersonalInfoQuery innerJoinReceivedOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the ReceivedOrder relation
 *
 * @method UserPersonalInfo findOne(PropelPDO $con = null) Return the first UserPersonalInfo matching the query
 * @method UserPersonalInfo findOneOrCreate(PropelPDO $con = null) Return the first UserPersonalInfo matching the query, or a new UserPersonalInfo object populated from the query conditions when no match is found
 *
 * @method UserPersonalInfo findOneByFirstName(string $first_name) Return the first UserPersonalInfo filtered by the first_name column
 * @method UserPersonalInfo findOneByLastName(string $last_name) Return the first UserPersonalInfo filtered by the last_name column
 * @method UserPersonalInfo findOneByLine1(string $line1) Return the first UserPersonalInfo filtered by the line1 column
 * @method UserPersonalInfo findOneByLine2(string $line2) Return the first UserPersonalInfo filtered by the line2 column
 * @method UserPersonalInfo findOneByCity(string $city) Return the first UserPersonalInfo filtered by the city column
 * @method UserPersonalInfo findOneByState(string $state) Return the first UserPersonalInfo filtered by the state column
 * @method UserPersonalInfo findOneByPostalCode(int $postal_code) Return the first UserPersonalInfo filtered by the postal_code column
 * @method UserPersonalInfo findOneByCountryCode(string $country_code) Return the first UserPersonalInfo filtered by the country_code column
 * @method UserPersonalInfo findOneByCreatedAt(string $created_at) Return the first UserPersonalInfo filtered by the created_at column
 * @method UserPersonalInfo findOneByUpdatedAt(string $updated_at) Return the first UserPersonalInfo filtered by the updated_at column
 *
 * @method array findById(int $id) Return UserPersonalInfo objects filtered by the id column
 * @method array findByFirstName(string $first_name) Return UserPersonalInfo objects filtered by the first_name column
 * @method array findByLastName(string $last_name) Return UserPersonalInfo objects filtered by the last_name column
 * @method array findByLine1(string $line1) Return UserPersonalInfo objects filtered by the line1 column
 * @method array findByLine2(string $line2) Return UserPersonalInfo objects filtered by the line2 column
 * @method array findByCity(string $city) Return UserPersonalInfo objects filtered by the city column
 * @method array findByState(string $state) Return UserPersonalInfo objects filtered by the state column
 * @method array findByPostalCode(int $postal_code) Return UserPersonalInfo objects filtered by the postal_code column
 * @method array findByCountryCode(string $country_code) Return UserPersonalInfo objects filtered by the country_code column
 * @method array findByCreatedAt(string $created_at) Return UserPersonalInfo objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return UserPersonalInfo objects filtered by the updated_at column
 */
abstract class BaseUserPersonalInfoQuery extends ModelCriteria
{

    /**
     * Initializes internal state of BaseUserPersonalInfoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'private_pilot';
        }
        if (null === $modelName) {
            $modelName = 'Lamk\\PrivatePilotBundle\\Model\\UserPersonalInfo';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UserPersonalInfoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   UserPersonalInfoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UserPersonalInfoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UserPersonalInfoQuery) {
            return $criteria;
        }
        $query = new UserPersonalInfoQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   UserPersonalInfo|UserPersonalInfo[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UserPersonalInfoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select || $this->selectColumns || $this->asColumns || $this->selectModifiers || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 UserPersonalInfo A model object, or null if the key is not found
     * @throws PropelException
     */
    public function findOneById($key, $con = null)
    {
        return $this->findPk($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 UserPersonalInfo A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `first_name`, `last_name`, `line1`, `line2`, `city`, `state`, `postal_code`, `country_code`, `created_at`, `updated_at` FROM `user_personal_info` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new UserPersonalInfo();
            $obj->hydrate($row);
            UserPersonalInfoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return UserPersonalInfo|UserPersonalInfo[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|UserPersonalInfo[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserPersonalInfoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserPersonalInfoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%'); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstName)) {
                $firstName = str_replace('*', '%', $firstName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%'); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastName)) {
                $lastName = str_replace('*', '%', $lastName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the line1 column
     *
     * Example usage:
     * <code>
     * $query->filterByLine1('fooValue');   // WHERE line1 = 'fooValue'
     * $query->filterByLine1('%fooValue%'); // WHERE line1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $line1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByLine1($line1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($line1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $line1)) {
                $line1 = str_replace('*', '%', $line1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::LINE1, $line1, $comparison);
    }

    /**
     * Filter the query on the line2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLine2('fooValue');   // WHERE line2 = 'fooValue'
     * $query->filterByLine2('%fooValue%'); // WHERE line2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $line2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByLine2($line2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($line2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $line2)) {
                $line2 = str_replace('*', '%', $line2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::LINE2, $line2, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $city)) {
                $city = str_replace('*', '%', $city);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::CITY, $city, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the postal_code column
     *
     * Example usage:
     * <code>
     * $query->filterByPostalCode(1234); // WHERE postal_code = 1234
     * $query->filterByPostalCode(array(12, 34)); // WHERE postal_code IN (12, 34)
     * $query->filterByPostalCode(array('min' => 12)); // WHERE postal_code >= 12
     * $query->filterByPostalCode(array('max' => 12)); // WHERE postal_code <= 12
     * </code>
     *
     * @param     mixed $postalCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByPostalCode($postalCode = null, $comparison = null)
    {
        if (is_array($postalCode)) {
            $useMinMax = false;
            if (isset($postalCode['min'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::POSTAL_CODE, $postalCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postalCode['max'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::POSTAL_CODE, $postalCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::POSTAL_CODE, $postalCode, $comparison);
    }

    /**
     * Filter the query on the country_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCountryCode('fooValue');   // WHERE country_code = 'fooValue'
     * $query->filterByCountryCode('%fooValue%'); // WHERE country_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $countryCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByCountryCode($countryCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($countryCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $countryCode)) {
                $countryCode = str_replace('*', '%', $countryCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::COUNTRY_CODE, $countryCode, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserPersonalInfoPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPersonalInfoPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserPersonalInfoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                    ->addUsingAlias(UserPersonalInfoPeer::ID, $user->getIdPersonalInfo(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            return $this
                    ->useUserQuery()
                    ->filterByPrimaryKeys($user->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinUser($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'User', '\Lamk\PrivatePilotBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related ReceivedOrder object
     *
     * @param   ReceivedOrder|PropelObjectCollection $receivedOrder  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserPersonalInfoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByReceivedOrder($receivedOrder, $comparison = null)
    {
        if ($receivedOrder instanceof ReceivedOrder) {
            return $this
                    ->addUsingAlias(UserPersonalInfoPeer::ID, $receivedOrder->getIdUserPersonalInfo(), $comparison);
        } elseif ($receivedOrder instanceof PropelObjectCollection) {
            return $this
                    ->useReceivedOrderQuery()
                    ->filterByPrimaryKeys($receivedOrder->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByReceivedOrder() only accepts arguments of type ReceivedOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ReceivedOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function joinReceivedOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ReceivedOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ReceivedOrder');
        }

        return $this;
    }

    /**
     * Use the ReceivedOrder relation ReceivedOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery A secondary query class using the current class as primary query
     */
    public function useReceivedOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinReceivedOrder($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'ReceivedOrder', '\Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   UserPersonalInfo $userPersonalInfo Object to remove from the list of results
     *
     * @return UserPersonalInfoQuery The current query, for fluid interface
     */
    public function prune($userPersonalInfo = null)
    {
        if ($userPersonalInfo) {
            $this->addUsingAlias(UserPersonalInfoPeer::ID, $userPersonalInfo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }
    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     UserPersonalInfoQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UserPersonalInfoPeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     UserPersonalInfoQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserPersonalInfoPeer::UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     UserPersonalInfoQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserPersonalInfoPeer::UPDATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     UserPersonalInfoQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UserPersonalInfoPeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date desc
     *
     * @return     UserPersonalInfoQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserPersonalInfoPeer::CREATED_AT);
    }

    /**
     * Order by create date asc
     *
     * @return     UserPersonalInfoQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserPersonalInfoPeer::CREATED_AT);
    }
}
