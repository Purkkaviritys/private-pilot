<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\Airport;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\FlightRoutePeer;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\ReceivedOrder;
use Lamk\PrivatePilotBundle\Model\User;

/**
 * @method FlightRouteQuery orderById($order = Criteria::ASC) Order by the id column
 * @method FlightRouteQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method FlightRouteQuery orderByNumberOfSeats($order = Criteria::ASC) Order by the number_of_seats column
 * @method FlightRouteQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method FlightRouteQuery orderByIdPilot($order = Criteria::ASC) Order by the id_pilot column
 * @method FlightRouteQuery orderByIdPlane($order = Criteria::ASC) Order by the id_plane column
 * @method FlightRouteQuery orderByExpiresAt($order = Criteria::ASC) Order by the expires_at column
 * @method FlightRouteQuery orderByPrice($order = Criteria::ASC) Order by the price column
 * @method FlightRouteQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method FlightRouteQuery orderByDepartureLocation($order = Criteria::ASC) Order by the departure_location column
 * @method FlightRouteQuery orderByDestinationLocation($order = Criteria::ASC) Order by the destination_location column
 * @method FlightRouteQuery orderByIdDepartureLocation($order = Criteria::ASC) Order by the id_departure_location column
 * @method FlightRouteQuery orderByIdDestinationLocation($order = Criteria::ASC) Order by the id_destination_location column
 * @method FlightRouteQuery orderByEstimatedFlightTime($order = Criteria::ASC) Order by the estimated_flight_time column
 * @method FlightRouteQuery orderByRating($order = Criteria::ASC) Order by the rating column
 * @method FlightRouteQuery orderByDepartureDate($order = Criteria::ASC) Order by the departure_date column
 * @method FlightRouteQuery orderByDestinationDate($order = Criteria::ASC) Order by the destination_date column
 * @method FlightRouteQuery orderByDepartureTime($order = Criteria::ASC) Order by the departure_time column
 * @method FlightRouteQuery orderByDestinationTime($order = Criteria::ASC) Order by the destination_time column
 * @method FlightRouteQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method FlightRouteQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method FlightRouteQuery groupById() Group by the id column
 * @method FlightRouteQuery groupByActive() Group by the active column
 * @method FlightRouteQuery groupByNumberOfSeats() Group by the number_of_seats column
 * @method FlightRouteQuery groupByName() Group by the name column
 * @method FlightRouteQuery groupByIdPilot() Group by the id_pilot column
 * @method FlightRouteQuery groupByIdPlane() Group by the id_plane column
 * @method FlightRouteQuery groupByExpiresAt() Group by the expires_at column
 * @method FlightRouteQuery groupByPrice() Group by the price column
 * @method FlightRouteQuery groupByDescription() Group by the description column
 * @method FlightRouteQuery groupByDepartureLocation() Group by the departure_location column
 * @method FlightRouteQuery groupByDestinationLocation() Group by the destination_location column
 * @method FlightRouteQuery groupByIdDepartureLocation() Group by the id_departure_location column
 * @method FlightRouteQuery groupByIdDestinationLocation() Group by the id_destination_location column
 * @method FlightRouteQuery groupByEstimatedFlightTime() Group by the estimated_flight_time column
 * @method FlightRouteQuery groupByRating() Group by the rating column
 * @method FlightRouteQuery groupByDepartureDate() Group by the departure_date column
 * @method FlightRouteQuery groupByDestinationDate() Group by the destination_date column
 * @method FlightRouteQuery groupByDepartureTime() Group by the departure_time column
 * @method FlightRouteQuery groupByDestinationTime() Group by the destination_time column
 * @method FlightRouteQuery groupByCreatedAt() Group by the created_at column
 * @method FlightRouteQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method FlightRouteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FlightRouteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FlightRouteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FlightRouteQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method FlightRouteQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method FlightRouteQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method FlightRouteQuery leftJoinAirplane($relationAlias = null) Adds a LEFT JOIN clause to the query using the Airplane relation
 * @method FlightRouteQuery rightJoinAirplane($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Airplane relation
 * @method FlightRouteQuery innerJoinAirplane($relationAlias = null) Adds a INNER JOIN clause to the query using the Airplane relation
 *
 * @method FlightRouteQuery leftJoinAirportRelatedByIdDepartureLocation($relationAlias = null) Adds a LEFT JOIN clause to the query using the AirportRelatedByIdDepartureLocation relation
 * @method FlightRouteQuery rightJoinAirportRelatedByIdDepartureLocation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AirportRelatedByIdDepartureLocation relation
 * @method FlightRouteQuery innerJoinAirportRelatedByIdDepartureLocation($relationAlias = null) Adds a INNER JOIN clause to the query using the AirportRelatedByIdDepartureLocation relation
 *
 * @method FlightRouteQuery leftJoinAirportRelatedByIdDestinationLocation($relationAlias = null) Adds a LEFT JOIN clause to the query using the AirportRelatedByIdDestinationLocation relation
 * @method FlightRouteQuery rightJoinAirportRelatedByIdDestinationLocation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AirportRelatedByIdDestinationLocation relation
 * @method FlightRouteQuery innerJoinAirportRelatedByIdDestinationLocation($relationAlias = null) Adds a INNER JOIN clause to the query using the AirportRelatedByIdDestinationLocation relation
 *
 * @method FlightRouteQuery leftJoinReceivedOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the ReceivedOrder relation
 * @method FlightRouteQuery rightJoinReceivedOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ReceivedOrder relation
 * @method FlightRouteQuery innerJoinReceivedOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the ReceivedOrder relation
 *
 * @method FlightRoute findOne(PropelPDO $con = null) Return the first FlightRoute matching the query
 * @method FlightRoute findOneOrCreate(PropelPDO $con = null) Return the first FlightRoute matching the query, or a new FlightRoute object populated from the query conditions when no match is found
 *
 * @method FlightRoute findOneByActive(boolean $active) Return the first FlightRoute filtered by the active column
 * @method FlightRoute findOneByNumberOfSeats(int $number_of_seats) Return the first FlightRoute filtered by the number_of_seats column
 * @method FlightRoute findOneByName(string $name) Return the first FlightRoute filtered by the name column
 * @method FlightRoute findOneByIdPilot(int $id_pilot) Return the first FlightRoute filtered by the id_pilot column
 * @method FlightRoute findOneByIdPlane(int $id_plane) Return the first FlightRoute filtered by the id_plane column
 * @method FlightRoute findOneByExpiresAt(string $expires_at) Return the first FlightRoute filtered by the expires_at column
 * @method FlightRoute findOneByPrice(string $price) Return the first FlightRoute filtered by the price column
 * @method FlightRoute findOneByDescription(string $description) Return the first FlightRoute filtered by the description column
 * @method FlightRoute findOneByDepartureLocation(string $departure_location) Return the first FlightRoute filtered by the departure_location column
 * @method FlightRoute findOneByDestinationLocation(string $destination_location) Return the first FlightRoute filtered by the destination_location column
 * @method FlightRoute findOneByIdDepartureLocation(int $id_departure_location) Return the first FlightRoute filtered by the id_departure_location column
 * @method FlightRoute findOneByIdDestinationLocation(int $id_destination_location) Return the first FlightRoute filtered by the id_destination_location column
 * @method FlightRoute findOneByEstimatedFlightTime(string $estimated_flight_time) Return the first FlightRoute filtered by the estimated_flight_time column
 * @method FlightRoute findOneByRating(double $rating) Return the first FlightRoute filtered by the rating column
 * @method FlightRoute findOneByDepartureDate(string $departure_date) Return the first FlightRoute filtered by the departure_date column
 * @method FlightRoute findOneByDestinationDate(string $destination_date) Return the first FlightRoute filtered by the destination_date column
 * @method FlightRoute findOneByDepartureTime(string $departure_time) Return the first FlightRoute filtered by the departure_time column
 * @method FlightRoute findOneByDestinationTime(string $destination_time) Return the first FlightRoute filtered by the destination_time column
 * @method FlightRoute findOneByCreatedAt(string $created_at) Return the first FlightRoute filtered by the created_at column
 * @method FlightRoute findOneByUpdatedAt(string $updated_at) Return the first FlightRoute filtered by the updated_at column
 *
 * @method array findById(int $id) Return FlightRoute objects filtered by the id column
 * @method array findByActive(boolean $active) Return FlightRoute objects filtered by the active column
 * @method array findByNumberOfSeats(int $number_of_seats) Return FlightRoute objects filtered by the number_of_seats column
 * @method array findByName(string $name) Return FlightRoute objects filtered by the name column
 * @method array findByIdPilot(int $id_pilot) Return FlightRoute objects filtered by the id_pilot column
 * @method array findByIdPlane(int $id_plane) Return FlightRoute objects filtered by the id_plane column
 * @method array findByExpiresAt(string $expires_at) Return FlightRoute objects filtered by the expires_at column
 * @method array findByPrice(string $price) Return FlightRoute objects filtered by the price column
 * @method array findByDescription(string $description) Return FlightRoute objects filtered by the description column
 * @method array findByDepartureLocation(string $departure_location) Return FlightRoute objects filtered by the departure_location column
 * @method array findByDestinationLocation(string $destination_location) Return FlightRoute objects filtered by the destination_location column
 * @method array findByIdDepartureLocation(int $id_departure_location) Return FlightRoute objects filtered by the id_departure_location column
 * @method array findByIdDestinationLocation(int $id_destination_location) Return FlightRoute objects filtered by the id_destination_location column
 * @method array findByEstimatedFlightTime(string $estimated_flight_time) Return FlightRoute objects filtered by the estimated_flight_time column
 * @method array findByRating(double $rating) Return FlightRoute objects filtered by the rating column
 * @method array findByDepartureDate(string $departure_date) Return FlightRoute objects filtered by the departure_date column
 * @method array findByDestinationDate(string $destination_date) Return FlightRoute objects filtered by the destination_date column
 * @method array findByDepartureTime(string $departure_time) Return FlightRoute objects filtered by the departure_time column
 * @method array findByDestinationTime(string $destination_time) Return FlightRoute objects filtered by the destination_time column
 * @method array findByCreatedAt(string $created_at) Return FlightRoute objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return FlightRoute objects filtered by the updated_at column
 */
abstract class BaseFlightRouteQuery extends ModelCriteria
{

    /**
     * Initializes internal state of BaseFlightRouteQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'private_pilot';
        }
        if (null === $modelName) {
            $modelName = 'Lamk\\PrivatePilotBundle\\Model\\FlightRoute';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FlightRouteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FlightRouteQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FlightRouteQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FlightRouteQuery) {
            return $criteria;
        }
        $query = new FlightRouteQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   FlightRoute|FlightRoute[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FlightRoutePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FlightRoutePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select || $this->selectColumns || $this->asColumns || $this->selectModifiers || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FlightRoute A model object, or null if the key is not found
     * @throws PropelException
     */
    public function findOneById($key, $con = null)
    {
        return $this->findPk($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FlightRoute A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `active`, `number_of_seats`, `name`, `id_pilot`, `id_plane`, `expires_at`, `price`, `description`, `departure_location`, `destination_location`, `id_departure_location`, `id_destination_location`, `estimated_flight_time`, `rating`, `departure_date`, `destination_date`, `departure_time`, `destination_time`, `created_at`, `updated_at` FROM `flight_route` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new FlightRoute();
            $obj->hydrate($row);
            FlightRoutePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return FlightRoute|FlightRoute[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|FlightRoute[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FlightRoutePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FlightRoutePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FlightRoutePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FlightRoutePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FlightRoutePeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the number_of_seats column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfSeats(1234); // WHERE number_of_seats = 1234
     * $query->filterByNumberOfSeats(array(12, 34)); // WHERE number_of_seats IN (12, 34)
     * $query->filterByNumberOfSeats(array('min' => 12)); // WHERE number_of_seats >= 12
     * $query->filterByNumberOfSeats(array('max' => 12)); // WHERE number_of_seats <= 12
     * </code>
     *
     * @param     mixed $numberOfSeats The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByNumberOfSeats($numberOfSeats = null, $comparison = null)
    {
        if (is_array($numberOfSeats)) {
            $useMinMax = false;
            if (isset($numberOfSeats['min'])) {
                $this->addUsingAlias(FlightRoutePeer::NUMBER_OF_SEATS, $numberOfSeats['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberOfSeats['max'])) {
                $this->addUsingAlias(FlightRoutePeer::NUMBER_OF_SEATS, $numberOfSeats['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::NUMBER_OF_SEATS, $numberOfSeats, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the id_pilot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPilot(1234); // WHERE id_pilot = 1234
     * $query->filterByIdPilot(array(12, 34)); // WHERE id_pilot IN (12, 34)
     * $query->filterByIdPilot(array('min' => 12)); // WHERE id_pilot >= 12
     * $query->filterByIdPilot(array('max' => 12)); // WHERE id_pilot <= 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $idPilot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByIdPilot($idPilot = null, $comparison = null)
    {
        if (is_array($idPilot)) {
            $useMinMax = false;
            if (isset($idPilot['min'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_PILOT, $idPilot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPilot['max'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_PILOT, $idPilot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::ID_PILOT, $idPilot, $comparison);
    }

    /**
     * Filter the query on the id_plane column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPlane(1234); // WHERE id_plane = 1234
     * $query->filterByIdPlane(array(12, 34)); // WHERE id_plane IN (12, 34)
     * $query->filterByIdPlane(array('min' => 12)); // WHERE id_plane >= 12
     * $query->filterByIdPlane(array('max' => 12)); // WHERE id_plane <= 12
     * </code>
     *
     * @see       filterByAirplane()
     *
     * @param     mixed $idPlane The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByIdPlane($idPlane = null, $comparison = null)
    {
        if (is_array($idPlane)) {
            $useMinMax = false;
            if (isset($idPlane['min'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_PLANE, $idPlane['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPlane['max'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_PLANE, $idPlane['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::ID_PLANE, $idPlane, $comparison);
    }

    /**
     * Filter the query on the expires_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiresAt('2011-03-14'); // WHERE expires_at = '2011-03-14'
     * $query->filterByExpiresAt('now'); // WHERE expires_at = '2011-03-14'
     * $query->filterByExpiresAt(array('max' => 'yesterday')); // WHERE expires_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $expiresAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByExpiresAt($expiresAt = null, $comparison = null)
    {
        if (is_array($expiresAt)) {
            $useMinMax = false;
            if (isset($expiresAt['min'])) {
                $this->addUsingAlias(FlightRoutePeer::EXPIRES_AT, $expiresAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiresAt['max'])) {
                $this->addUsingAlias(FlightRoutePeer::EXPIRES_AT, $expiresAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::EXPIRES_AT, $expiresAt, $comparison);
    }

    /**
     * Filter the query on the price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE price = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE price IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE price >= 12
     * $query->filterByPrice(array('max' => 12)); // WHERE price <= 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(FlightRoutePeer::PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(FlightRoutePeer::PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the departure_location column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartureLocation('fooValue');   // WHERE departure_location = 'fooValue'
     * $query->filterByDepartureLocation('%fooValue%'); // WHERE departure_location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departureLocation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDepartureLocation($departureLocation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departureLocation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departureLocation)) {
                $departureLocation = str_replace('*', '%', $departureLocation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DEPARTURE_LOCATION, $departureLocation, $comparison);
    }

    /**
     * Filter the query on the destination_location column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationLocation('fooValue');   // WHERE destination_location = 'fooValue'
     * $query->filterByDestinationLocation('%fooValue%'); // WHERE destination_location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinationLocation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDestinationLocation($destinationLocation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinationLocation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinationLocation)) {
                $destinationLocation = str_replace('*', '%', $destinationLocation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DESTINATION_LOCATION, $destinationLocation, $comparison);
    }

    /**
     * Filter the query on the id_departure_location column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDepartureLocation(1234); // WHERE id_departure_location = 1234
     * $query->filterByIdDepartureLocation(array(12, 34)); // WHERE id_departure_location IN (12, 34)
     * $query->filterByIdDepartureLocation(array('min' => 12)); // WHERE id_departure_location >= 12
     * $query->filterByIdDepartureLocation(array('max' => 12)); // WHERE id_departure_location <= 12
     * </code>
     *
     * @see       filterByAirportRelatedByIdDepartureLocation()
     *
     * @param     mixed $idDepartureLocation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByIdDepartureLocation($idDepartureLocation = null, $comparison = null)
    {
        if (is_array($idDepartureLocation)) {
            $useMinMax = false;
            if (isset($idDepartureLocation['min'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_DEPARTURE_LOCATION, $idDepartureLocation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDepartureLocation['max'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_DEPARTURE_LOCATION, $idDepartureLocation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::ID_DEPARTURE_LOCATION, $idDepartureLocation, $comparison);
    }

    /**
     * Filter the query on the id_destination_location column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDestinationLocation(1234); // WHERE id_destination_location = 1234
     * $query->filterByIdDestinationLocation(array(12, 34)); // WHERE id_destination_location IN (12, 34)
     * $query->filterByIdDestinationLocation(array('min' => 12)); // WHERE id_destination_location >= 12
     * $query->filterByIdDestinationLocation(array('max' => 12)); // WHERE id_destination_location <= 12
     * </code>
     *
     * @see       filterByAirportRelatedByIdDestinationLocation()
     *
     * @param     mixed $idDestinationLocation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByIdDestinationLocation($idDestinationLocation = null, $comparison = null)
    {
        if (is_array($idDestinationLocation)) {
            $useMinMax = false;
            if (isset($idDestinationLocation['min'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_DESTINATION_LOCATION, $idDestinationLocation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDestinationLocation['max'])) {
                $this->addUsingAlias(FlightRoutePeer::ID_DESTINATION_LOCATION, $idDestinationLocation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::ID_DESTINATION_LOCATION, $idDestinationLocation, $comparison);
    }

    /**
     * Filter the query on the estimated_flight_time column
     *
     * Example usage:
     * <code>
     * $query->filterByEstimatedFlightTime('2011-03-14'); // WHERE estimated_flight_time = '2011-03-14'
     * $query->filterByEstimatedFlightTime('now'); // WHERE estimated_flight_time = '2011-03-14'
     * $query->filterByEstimatedFlightTime(array('max' => 'yesterday')); // WHERE estimated_flight_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $estimatedFlightTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByEstimatedFlightTime($estimatedFlightTime = null, $comparison = null)
    {
        if (is_array($estimatedFlightTime)) {
            $useMinMax = false;
            if (isset($estimatedFlightTime['min'])) {
                $this->addUsingAlias(FlightRoutePeer::ESTIMATED_FLIGHT_TIME, $estimatedFlightTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($estimatedFlightTime['max'])) {
                $this->addUsingAlias(FlightRoutePeer::ESTIMATED_FLIGHT_TIME, $estimatedFlightTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::ESTIMATED_FLIGHT_TIME, $estimatedFlightTime, $comparison);
    }

    /**
     * Filter the query on the rating column
     *
     * Example usage:
     * <code>
     * $query->filterByRating(1234); // WHERE rating = 1234
     * $query->filterByRating(array(12, 34)); // WHERE rating IN (12, 34)
     * $query->filterByRating(array('min' => 12)); // WHERE rating >= 12
     * $query->filterByRating(array('max' => 12)); // WHERE rating <= 12
     * </code>
     *
     * @param     mixed $rating The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByRating($rating = null, $comparison = null)
    {
        if (is_array($rating)) {
            $useMinMax = false;
            if (isset($rating['min'])) {
                $this->addUsingAlias(FlightRoutePeer::RATING, $rating['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rating['max'])) {
                $this->addUsingAlias(FlightRoutePeer::RATING, $rating['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::RATING, $rating, $comparison);
    }

    /**
     * Filter the query on the departure_date column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartureDate('2011-03-14'); // WHERE departure_date = '2011-03-14'
     * $query->filterByDepartureDate('now'); // WHERE departure_date = '2011-03-14'
     * $query->filterByDepartureDate(array('max' => 'yesterday')); // WHERE departure_date < '2011-03-13'
     * </code>
     *
     * @param     mixed $departureDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDepartureDate($departureDate = null, $comparison = null)
    {
        if (is_array($departureDate)) {
            $useMinMax = false;
            if (isset($departureDate['min'])) {
                $this->addUsingAlias(FlightRoutePeer::DEPARTURE_DATE, $departureDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($departureDate['max'])) {
                $this->addUsingAlias(FlightRoutePeer::DEPARTURE_DATE, $departureDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DEPARTURE_DATE, $departureDate, $comparison);
    }

    /**
     * Filter the query on the destination_date column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationDate('2011-03-14'); // WHERE destination_date = '2011-03-14'
     * $query->filterByDestinationDate('now'); // WHERE destination_date = '2011-03-14'
     * $query->filterByDestinationDate(array('max' => 'yesterday')); // WHERE destination_date < '2011-03-13'
     * </code>
     *
     * @param     mixed $destinationDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDestinationDate($destinationDate = null, $comparison = null)
    {
        if (is_array($destinationDate)) {
            $useMinMax = false;
            if (isset($destinationDate['min'])) {
                $this->addUsingAlias(FlightRoutePeer::DESTINATION_DATE, $destinationDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($destinationDate['max'])) {
                $this->addUsingAlias(FlightRoutePeer::DESTINATION_DATE, $destinationDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DESTINATION_DATE, $destinationDate, $comparison);
    }

    /**
     * Filter the query on the departure_time column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartureTime('2011-03-14'); // WHERE departure_time = '2011-03-14'
     * $query->filterByDepartureTime('now'); // WHERE departure_time = '2011-03-14'
     * $query->filterByDepartureTime(array('max' => 'yesterday')); // WHERE departure_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $departureTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDepartureTime($departureTime = null, $comparison = null)
    {
        if (is_array($departureTime)) {
            $useMinMax = false;
            if (isset($departureTime['min'])) {
                $this->addUsingAlias(FlightRoutePeer::DEPARTURE_TIME, $departureTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($departureTime['max'])) {
                $this->addUsingAlias(FlightRoutePeer::DEPARTURE_TIME, $departureTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DEPARTURE_TIME, $departureTime, $comparison);
    }

    /**
     * Filter the query on the destination_time column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinationTime('2011-03-14'); // WHERE destination_time = '2011-03-14'
     * $query->filterByDestinationTime('now'); // WHERE destination_time = '2011-03-14'
     * $query->filterByDestinationTime(array('max' => 'yesterday')); // WHERE destination_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $destinationTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByDestinationTime($destinationTime = null, $comparison = null)
    {
        if (is_array($destinationTime)) {
            $useMinMax = false;
            if (isset($destinationTime['min'])) {
                $this->addUsingAlias(FlightRoutePeer::DESTINATION_TIME, $destinationTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($destinationTime['max'])) {
                $this->addUsingAlias(FlightRoutePeer::DESTINATION_TIME, $destinationTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::DESTINATION_TIME, $destinationTime, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FlightRoutePeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FlightRoutePeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FlightRoutePeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FlightRoutePeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlightRoutePeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FlightRouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_PILOT, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_PILOT, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinUser($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'User', '\Lamk\PrivatePilotBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related Airplane object
     *
     * @param   Airplane|PropelObjectCollection $airplane The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FlightRouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAirplane($airplane, $comparison = null)
    {
        if ($airplane instanceof Airplane) {
            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_PLANE, $airplane->getId(), $comparison);
        } elseif ($airplane instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_PLANE, $airplane->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAirplane() only accepts arguments of type Airplane or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Airplane relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function joinAirplane($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Airplane');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Airplane');
        }

        return $this;
    }

    /**
     * Use the Airplane relation Airplane object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\AirplaneQuery A secondary query class using the current class as primary query
     */
    public function useAirplaneQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinAirplane($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'Airplane', '\Lamk\PrivatePilotBundle\Model\AirplaneQuery');
    }

    /**
     * Filter the query by a related Airport object
     *
     * @param   Airport|PropelObjectCollection $airport The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FlightRouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAirportRelatedByIdDepartureLocation($airport, $comparison = null)
    {
        if ($airport instanceof Airport) {
            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_DEPARTURE_LOCATION, $airport->getId(), $comparison);
        } elseif ($airport instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_DEPARTURE_LOCATION, $airport->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAirportRelatedByIdDepartureLocation() only accepts arguments of type Airport or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AirportRelatedByIdDepartureLocation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function joinAirportRelatedByIdDepartureLocation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AirportRelatedByIdDepartureLocation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AirportRelatedByIdDepartureLocation');
        }

        return $this;
    }

    /**
     * Use the AirportRelatedByIdDepartureLocation relation Airport object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\AirportQuery A secondary query class using the current class as primary query
     */
    public function useAirportRelatedByIdDepartureLocationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinAirportRelatedByIdDepartureLocation($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'AirportRelatedByIdDepartureLocation', '\Lamk\PrivatePilotBundle\Model\AirportQuery');
    }

    /**
     * Filter the query by a related Airport object
     *
     * @param   Airport|PropelObjectCollection $airport The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FlightRouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAirportRelatedByIdDestinationLocation($airport, $comparison = null)
    {
        if ($airport instanceof Airport) {
            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_DESTINATION_LOCATION, $airport->getId(), $comparison);
        } elseif ($airport instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                    ->addUsingAlias(FlightRoutePeer::ID_DESTINATION_LOCATION, $airport->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAirportRelatedByIdDestinationLocation() only accepts arguments of type Airport or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AirportRelatedByIdDestinationLocation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function joinAirportRelatedByIdDestinationLocation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AirportRelatedByIdDestinationLocation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AirportRelatedByIdDestinationLocation');
        }

        return $this;
    }

    /**
     * Use the AirportRelatedByIdDestinationLocation relation Airport object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\AirportQuery A secondary query class using the current class as primary query
     */
    public function useAirportRelatedByIdDestinationLocationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinAirportRelatedByIdDestinationLocation($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'AirportRelatedByIdDestinationLocation', '\Lamk\PrivatePilotBundle\Model\AirportQuery');
    }

    /**
     * Filter the query by a related ReceivedOrder object
     *
     * @param   ReceivedOrder|PropelObjectCollection $receivedOrder  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FlightRouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByReceivedOrder($receivedOrder, $comparison = null)
    {
        if ($receivedOrder instanceof ReceivedOrder) {
            return $this
                    ->addUsingAlias(FlightRoutePeer::ID, $receivedOrder->getIdFlightRoute(), $comparison);
        } elseif ($receivedOrder instanceof PropelObjectCollection) {
            return $this
                    ->useReceivedOrderQuery()
                    ->filterByPrimaryKeys($receivedOrder->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByReceivedOrder() only accepts arguments of type ReceivedOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ReceivedOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function joinReceivedOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ReceivedOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ReceivedOrder');
        }

        return $this;
    }

    /**
     * Use the ReceivedOrder relation ReceivedOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery A secondary query class using the current class as primary query
     */
    public function useReceivedOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinReceivedOrder($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'ReceivedOrder', '\Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   FlightRoute $flightRoute Object to remove from the list of results
     *
     * @return FlightRouteQuery The current query, for fluid interface
     */
    public function prune($flightRoute = null)
    {
        if ($flightRoute) {
            $this->addUsingAlias(FlightRoutePeer::ID, $flightRoute->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }
    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     FlightRouteQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(FlightRoutePeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     FlightRouteQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(FlightRoutePeer::UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     FlightRouteQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(FlightRoutePeer::UPDATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     FlightRouteQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(FlightRoutePeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date desc
     *
     * @return     FlightRouteQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(FlightRoutePeer::CREATED_AT);
    }

    /**
     * Order by create date asc
     *
     * @return     FlightRouteQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(FlightRoutePeer::CREATED_AT);
    }
}
