<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\AirplanePeer;
use Lamk\PrivatePilotBundle\Model\AirplaneQuery;
use Lamk\PrivatePilotBundle\Model\File;
use Lamk\PrivatePilotBundle\Model\FileQuery;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserQuery;

abstract class BaseAirplane extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Lamk\\PrivatePilotBundle\\Model\\AirplanePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AirplanePeer
     */
    protected static $peer;
    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;
    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;
    /**
     * The value for the id_image field.
     * @var        int
     */
    protected $id_image;
    /**
     * The value for the airplane_model field.
     * @var        string
     */
    protected $airplane_model;
    /**
     * The value for the airplane_manufacturing_date field.
     * @var        string
     */
    protected $airplane_manufacturing_date;
    /**
     * The value for the airplane_country field.
     * @var        string
     */
    protected $airplane_country;
    /**
     * The value for the airplane_serial_number field.
     * @var        string
     */
    protected $airplane_serial_number;
    /**
     * The value for the airplane_ttaf field.
     * @var        string
     */
    protected $airplane_ttaf;
    /**
     * The value for the number_of_seats_available field.
     * @var        int
     */
    protected $number_of_seats_available;
    /**
     * The value for the number_of_seats field.
     * @var        int
     */
    protected $number_of_seats;
    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;
    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;
    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;
    /**
     * @var        File
     */
    protected $aFile;
    /**
     * @var        PropelObjectCollection|FlightRoute[] Collection to store aggregation of FlightRoute objects.
     */
    protected $collFlightRoutes;
    protected $collFlightRoutesPartial;
    /**
     * @var        PropelObjectCollection|User[] Collection to store aggregation of User objects.
     */
    protected $collUsers;
    protected $collUsersPartial;
    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;
    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;
    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $flightRoutesScheduledForDeletion = null;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $usersScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_image] column value.
     *
     * @return int
     */
    public function getIdImage()
    {

        return $this->id_image;
    }

    /**
     * Get the [airplane_model] column value.
     *
     * @return string
     */
    public function getAirplaneModel()
    {

        return $this->airplane_model;
    }

    /**
     * Get the [optionally formatted] temporal [airplane_manufacturing_date] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getAirplaneManufacturingDate($format = null)
    {
        if ($this->airplane_manufacturing_date === null) {
            return null;
        }

        if ($this->airplane_manufacturing_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->airplane_manufacturing_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->airplane_manufacturing_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [airplane_country] column value.
     *
     * @return string
     */
    public function getAirplaneCountry()
    {

        return $this->airplane_country;
    }

    /**
     * Get the [airplane_serial_number] column value.
     *
     * @return string
     */
    public function getAirplaneSerialNumber()
    {

        return $this->airplane_serial_number;
    }

    /**
     * Get the [airplane_ttaf] column value.
     *
     * @return string
     */
    public function getAirplaneTtaf()
    {

        return $this->airplane_ttaf;
    }

    /**
     * Get the [number_of_seats_available] column value.
     *
     * @return int
     */
    public function getNumberOfSeatsAvailable()
    {

        return $this->number_of_seats_available;
    }

    /**
     * Get the [number_of_seats] column value.
     *
     * @return int
     */
    public function getNumberOfSeats()
    {

        return $this->number_of_seats;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($this->updated_at === null) {
            return null;
        }

        if ($this->updated_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = AirplanePeer::ID;
        }


        return $this;
    }
// setId()

    /**
     * Set the value of [id_image] column.
     *
     * @param  int $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setIdImage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_image !== $v) {
            $this->id_image = $v;
            $this->modifiedColumns[] = AirplanePeer::ID_IMAGE;
        }

        if ($this->aFile !== null && $this->aFile->getId() !== $v) {
            $this->aFile = null;
        }


        return $this;
    }
// setIdImage()

    /**
     * Set the value of [airplane_model] column.
     *
     * @param  string $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setAirplaneModel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->airplane_model !== $v) {
            $this->airplane_model = $v;
            $this->modifiedColumns[] = AirplanePeer::AIRPLANE_MODEL;
        }


        return $this;
    }
// setAirplaneModel()

    /**
     * Sets the value of [airplane_manufacturing_date] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Airplane The current object (for fluent API support)
     */
    public function setAirplaneManufacturingDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->airplane_manufacturing_date !== null || $dt !== null) {
            $currentDateAsString = ($this->airplane_manufacturing_date !== null && $tmpDt = new DateTime($this->airplane_manufacturing_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->airplane_manufacturing_date = $newDateAsString;
                $this->modifiedColumns[] = AirplanePeer::AIRPLANE_MANUFACTURING_DATE;
            }
        } // if either are not null


        return $this;
    }
// setAirplaneManufacturingDate()

    /**
     * Set the value of [airplane_country] column.
     *
     * @param  string $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setAirplaneCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->airplane_country !== $v) {
            $this->airplane_country = $v;
            $this->modifiedColumns[] = AirplanePeer::AIRPLANE_COUNTRY;
        }


        return $this;
    }
// setAirplaneCountry()

    /**
     * Set the value of [airplane_serial_number] column.
     *
     * @param  string $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setAirplaneSerialNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->airplane_serial_number !== $v) {
            $this->airplane_serial_number = $v;
            $this->modifiedColumns[] = AirplanePeer::AIRPLANE_SERIAL_NUMBER;
        }


        return $this;
    }
// setAirplaneSerialNumber()

    /**
     * Set the value of [airplane_ttaf] column.
     *
     * @param  string $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setAirplaneTtaf($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->airplane_ttaf !== $v) {
            $this->airplane_ttaf = $v;
            $this->modifiedColumns[] = AirplanePeer::AIRPLANE_TTAF;
        }


        return $this;
    }
// setAirplaneTtaf()

    /**
     * Set the value of [number_of_seats_available] column.
     *
     * @param  int $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setNumberOfSeatsAvailable($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->number_of_seats_available !== $v) {
            $this->number_of_seats_available = $v;
            $this->modifiedColumns[] = AirplanePeer::NUMBER_OF_SEATS_AVAILABLE;
        }


        return $this;
    }
// setNumberOfSeatsAvailable()

    /**
     * Set the value of [number_of_seats] column.
     *
     * @param  int $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setNumberOfSeats($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->number_of_seats !== $v) {
            $this->number_of_seats = $v;
            $this->modifiedColumns[] = AirplanePeer::NUMBER_OF_SEATS;
        }


        return $this;
    }
// setNumberOfSeats()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return Airplane The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = AirplanePeer::DESCRIPTION;
        }


        return $this;
    }
// setDescription()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Airplane The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = AirplanePeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Airplane The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = AirplanePeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    }
// hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_image = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->airplane_model = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->airplane_manufacturing_date = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->airplane_country = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->airplane_serial_number = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->airplane_ttaf = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->number_of_seats_available = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->number_of_seats = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->description = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->created_at = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->updated_at = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = AirplanePeer::NUM_HYDRATE_COLUMNS.
        } catch (Exception $e) {
            throw new PropelException("Error populating Airplane object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aFile !== null && $this->id_image !== $this->aFile->getId()) {
            $this->aFile = null;
        }
    }
// ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AirplanePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AirplanePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?
            $this->aFile = null;
            $this->collFlightRoutes = null;

            $this->collUsers = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AirplanePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AirplaneQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AirplanePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                if (!$this->isColumnModified(AirplanePeer::CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(AirplanePeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(AirplanePeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AirplanePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFile !== null) {
                if ($this->aFile->isModified() || $this->aFile->isNew()) {
                    $affectedRows += $this->aFile->save($con);
                }
                $this->setFile($this->aFile);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->flightRoutesScheduledForDeletion !== null) {
                if (!$this->flightRoutesScheduledForDeletion->isEmpty()) {
                    foreach ($this->flightRoutesScheduledForDeletion as $flightRoute) {
                        // need to save related object because we set the relation to null
                        $flightRoute->save($con);
                    }
                    $this->flightRoutesScheduledForDeletion = null;
                }
            }

            if ($this->collFlightRoutes !== null) {
                foreach ($this->collFlightRoutes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usersScheduledForDeletion !== null) {
                if (!$this->usersScheduledForDeletion->isEmpty()) {
                    foreach ($this->usersScheduledForDeletion as $user) {
                        // need to save related object because we set the relation to null
                        $user->save($con);
                    }
                    $this->usersScheduledForDeletion = null;
                }
            }

            if ($this->collUsers !== null) {
                foreach ($this->collUsers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;
        }

        return $affectedRows;
    }
// doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = AirplanePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . AirplanePeer::ID . ')');
        }

        // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AirplanePeer::ID)) {
            $modifiedColumns[':p' . $index++] = '`id`';
        }
        if ($this->isColumnModified(AirplanePeer::ID_IMAGE)) {
            $modifiedColumns[':p' . $index++] = '`id_image`';
        }
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_MODEL)) {
            $modifiedColumns[':p' . $index++] = '`airplane_model`';
        }
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_MANUFACTURING_DATE)) {
            $modifiedColumns[':p' . $index++] = '`airplane_manufacturing_date`';
        }
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_COUNTRY)) {
            $modifiedColumns[':p' . $index++] = '`airplane_country`';
        }
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_SERIAL_NUMBER)) {
            $modifiedColumns[':p' . $index++] = '`airplane_serial_number`';
        }
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_TTAF)) {
            $modifiedColumns[':p' . $index++] = '`airplane_ttaf`';
        }
        if ($this->isColumnModified(AirplanePeer::NUMBER_OF_SEATS_AVAILABLE)) {
            $modifiedColumns[':p' . $index++] = '`number_of_seats_available`';
        }
        if ($this->isColumnModified(AirplanePeer::NUMBER_OF_SEATS)) {
            $modifiedColumns[':p' . $index++] = '`number_of_seats`';
        }
        if ($this->isColumnModified(AirplanePeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++] = '`description`';
        }
        if ($this->isColumnModified(AirplanePeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`created_at`';
        }
        if ($this->isColumnModified(AirplanePeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `airplane` (%s) VALUES (%s)', implode(', ', $modifiedColumns), implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_image`':
                        $stmt->bindValue($identifier, $this->id_image, PDO::PARAM_INT);
                        break;
                    case '`airplane_model`':
                        $stmt->bindValue($identifier, $this->airplane_model, PDO::PARAM_STR);
                        break;
                    case '`airplane_manufacturing_date`':
                        $stmt->bindValue($identifier, $this->airplane_manufacturing_date, PDO::PARAM_STR);
                        break;
                    case '`airplane_country`':
                        $stmt->bindValue($identifier, $this->airplane_country, PDO::PARAM_STR);
                        break;
                    case '`airplane_serial_number`':
                        $stmt->bindValue($identifier, $this->airplane_serial_number, PDO::PARAM_STR);
                        break;
                    case '`airplane_ttaf`':
                        $stmt->bindValue($identifier, $this->airplane_ttaf, PDO::PARAM_STR);
                        break;
                    case '`number_of_seats_available`':
                        $stmt->bindValue($identifier, $this->number_of_seats_available, PDO::PARAM_INT);
                        break;
                    case '`number_of_seats`':
                        $stmt->bindValue($identifier, $this->number_of_seats, PDO::PARAM_INT);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }
    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFile !== null) {
                if (!$this->aFile->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aFile->getValidationFailures());
                }
            }


            if (($retval = AirplanePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


            if ($this->collFlightRoutes !== null) {
                foreach ($this->collFlightRoutes as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }

            if ($this->collUsers !== null) {
                foreach ($this->collUsers as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AirplanePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdImage();
                break;
            case 2:
                return $this->getAirplaneModel();
                break;
            case 3:
                return $this->getAirplaneManufacturingDate();
                break;
            case 4:
                return $this->getAirplaneCountry();
                break;
            case 5:
                return $this->getAirplaneSerialNumber();
                break;
            case 6:
                return $this->getAirplaneTtaf();
                break;
            case 7:
                return $this->getNumberOfSeatsAvailable();
                break;
            case 8:
                return $this->getNumberOfSeats();
                break;
            case 9:
                return $this->getDescription();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Airplane'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Airplane'][$this->getPrimaryKey()] = true;
        $keys = AirplanePeer::getFieldNames($keyType);
        $result = array(
          $keys[0]  => $this->getId(),
          $keys[1]  => $this->getIdImage(),
          $keys[2]  => $this->getAirplaneModel(),
          $keys[3]  => $this->getAirplaneManufacturingDate(),
          $keys[4]  => $this->getAirplaneCountry(),
          $keys[5]  => $this->getAirplaneSerialNumber(),
          $keys[6]  => $this->getAirplaneTtaf(),
          $keys[7]  => $this->getNumberOfSeatsAvailable(),
          $keys[8]  => $this->getNumberOfSeats(),
          $keys[9]  => $this->getDescription(),
          $keys[10] => $this->getCreatedAt(),
          $keys[11] => $this->getUpdatedAt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFile) {
                $result['File'] = $this->aFile->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collFlightRoutes) {
                $result['FlightRoutes'] = $this->collFlightRoutes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsers) {
                $result['Users'] = $this->collUsers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AirplanePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdImage($value);
                break;
            case 2:
                $this->setAirplaneModel($value);
                break;
            case 3:
                $this->setAirplaneManufacturingDate($value);
                break;
            case 4:
                $this->setAirplaneCountry($value);
                break;
            case 5:
                $this->setAirplaneSerialNumber($value);
                break;
            case 6:
                $this->setAirplaneTtaf($value);
                break;
            case 7:
                $this->setNumberOfSeatsAvailable($value);
                break;
            case 8:
                $this->setNumberOfSeats($value);
                break;
            case 9:
                $this->setDescription($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AirplanePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr))
            $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr))
            $this->setIdImage($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr))
            $this->setAirplaneModel($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr))
            $this->setAirplaneManufacturingDate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr))
            $this->setAirplaneCountry($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr))
            $this->setAirplaneSerialNumber($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr))
            $this->setAirplaneTtaf($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr))
            $this->setNumberOfSeatsAvailable($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr))
            $this->setNumberOfSeats($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr))
            $this->setDescription($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr))
            $this->setCreatedAt($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr))
            $this->setUpdatedAt($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AirplanePeer::DATABASE_NAME);

        if ($this->isColumnModified(AirplanePeer::ID))
            $criteria->add(AirplanePeer::ID, $this->id);
        if ($this->isColumnModified(AirplanePeer::ID_IMAGE))
            $criteria->add(AirplanePeer::ID_IMAGE, $this->id_image);
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_MODEL))
            $criteria->add(AirplanePeer::AIRPLANE_MODEL, $this->airplane_model);
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_MANUFACTURING_DATE))
            $criteria->add(AirplanePeer::AIRPLANE_MANUFACTURING_DATE, $this->airplane_manufacturing_date);
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_COUNTRY))
            $criteria->add(AirplanePeer::AIRPLANE_COUNTRY, $this->airplane_country);
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_SERIAL_NUMBER))
            $criteria->add(AirplanePeer::AIRPLANE_SERIAL_NUMBER, $this->airplane_serial_number);
        if ($this->isColumnModified(AirplanePeer::AIRPLANE_TTAF))
            $criteria->add(AirplanePeer::AIRPLANE_TTAF, $this->airplane_ttaf);
        if ($this->isColumnModified(AirplanePeer::NUMBER_OF_SEATS_AVAILABLE))
            $criteria->add(AirplanePeer::NUMBER_OF_SEATS_AVAILABLE, $this->number_of_seats_available);
        if ($this->isColumnModified(AirplanePeer::NUMBER_OF_SEATS))
            $criteria->add(AirplanePeer::NUMBER_OF_SEATS, $this->number_of_seats);
        if ($this->isColumnModified(AirplanePeer::DESCRIPTION))
            $criteria->add(AirplanePeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(AirplanePeer::CREATED_AT))
            $criteria->add(AirplanePeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(AirplanePeer::UPDATED_AT))
            $criteria->add(AirplanePeer::UPDATED_AT, $this->updated_at);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AirplanePeer::DATABASE_NAME);
        $criteria->add(AirplanePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Airplane (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdImage($this->getIdImage());
        $copyObj->setAirplaneModel($this->getAirplaneModel());
        $copyObj->setAirplaneManufacturingDate($this->getAirplaneManufacturingDate());
        $copyObj->setAirplaneCountry($this->getAirplaneCountry());
        $copyObj->setAirplaneSerialNumber($this->getAirplaneSerialNumber());
        $copyObj->setAirplaneTtaf($this->getAirplaneTtaf());
        $copyObj->setNumberOfSeatsAvailable($this->getNumberOfSeatsAvailable());
        $copyObj->setNumberOfSeats($this->getNumberOfSeats());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getFlightRoutes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlightRoute($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUser($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Airplane Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AirplanePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AirplanePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a File object.
     *
     * @param                  File $v
     * @return Airplane The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFile(File $v = null)
    {
        if ($v === null) {
            $this->setIdImage(NULL);
        } else {
            $this->setIdImage($v->getId());
        }

        $this->aFile = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the File object, it will not be re-added.
        if ($v !== null) {
            $v->addAirplane($this);
        }


        return $this;
    }

    /**
     * Get the associated File object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return File The associated File object.
     * @throws PropelException
     */
    public function getFile(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aFile === null && ($this->id_image !== null) && $doQuery) {
            $this->aFile = FileQuery::create()->findPk($this->id_image, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aFile->addAirplanes($this);
             */
        }

        return $this->aFile;
    }

    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('FlightRoute' == $relationName) {
            $this->initFlightRoutes();
        }
        if ('User' == $relationName) {
            $this->initUsers();
        }
    }

    /**
     * Clears out the collFlightRoutes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Airplane The current object (for fluent API support)
     * @see        addFlightRoutes()
     */
    public function clearFlightRoutes()
    {
        $this->collFlightRoutes = null; // important to set this to null since that means it is uninitialized
        $this->collFlightRoutesPartial = null;

        return $this;
    }

    /**
     * reset is the collFlightRoutes collection loaded partially
     *
     * @return void
     */
    public function resetPartialFlightRoutes($v = true)
    {
        $this->collFlightRoutesPartial = $v;
    }

    /**
     * Initializes the collFlightRoutes collection.
     *
     * By default this just sets the collFlightRoutes collection to an empty array (like clearcollFlightRoutes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlightRoutes($overrideExisting = true)
    {
        if (null !== $this->collFlightRoutes && !$overrideExisting) {
            return;
        }
        $this->collFlightRoutes = new PropelObjectCollection();
        $this->collFlightRoutes->setModel('FlightRoute');
    }

    /**
     * Gets an array of FlightRoute objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Airplane is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     * @throws PropelException
     */
    public function getFlightRoutes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesPartial && !$this->isNew();
        if (null === $this->collFlightRoutes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutes) {
                // return empty collection
                $this->initFlightRoutes();
            } else {
                $collFlightRoutes = FlightRouteQuery::create(null, $criteria)
                    ->filterByAirplane($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collFlightRoutesPartial && count($collFlightRoutes)) {
                        $this->initFlightRoutes(false);

                        foreach ($collFlightRoutes as $obj) {
                            if (false == $this->collFlightRoutes->contains($obj)) {
                                $this->collFlightRoutes->append($obj);
                            }
                        }

                        $this->collFlightRoutesPartial = true;
                    }

                    $collFlightRoutes->getInternalIterator()->rewind();

                    return $collFlightRoutes;
                }

                if ($partial && $this->collFlightRoutes) {
                    foreach ($this->collFlightRoutes as $obj) {
                        if ($obj->isNew()) {
                            $collFlightRoutes[] = $obj;
                        }
                    }
                }

                $this->collFlightRoutes = $collFlightRoutes;
                $this->collFlightRoutesPartial = false;
            }
        }

        return $this->collFlightRoutes;
    }

    /**
     * Sets a collection of FlightRoute objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $flightRoutes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Airplane The current object (for fluent API support)
     */
    public function setFlightRoutes(PropelCollection $flightRoutes, PropelPDO $con = null)
    {
        $flightRoutesToDelete = $this->getFlightRoutes(new Criteria(), $con)->diff($flightRoutes);


        $this->flightRoutesScheduledForDeletion = $flightRoutesToDelete;

        foreach ($flightRoutesToDelete as $flightRouteRemoved) {
            $flightRouteRemoved->setAirplane(null);
        }

        $this->collFlightRoutes = null;
        foreach ($flightRoutes as $flightRoute) {
            $this->addFlightRoute($flightRoute);
        }

        $this->collFlightRoutes = $flightRoutes;
        $this->collFlightRoutesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlightRoute objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related FlightRoute objects.
     * @throws PropelException
     */
    public function countFlightRoutes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesPartial && !$this->isNew();
        if (null === $this->collFlightRoutes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlightRoutes());
            }
            $query = FlightRouteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByAirplane($this)
                    ->count($con);
        }

        return count($this->collFlightRoutes);
    }

    /**
     * Method called to associate a FlightRoute object to this object
     * through the FlightRoute foreign key attribute.
     *
     * @param    FlightRoute $l FlightRoute
     * @return Airplane The current object (for fluent API support)
     */
    public function addFlightRoute(FlightRoute $l)
    {
        if ($this->collFlightRoutes === null) {
            $this->initFlightRoutes();
            $this->collFlightRoutesPartial = true;
        }

        if (!in_array($l, $this->collFlightRoutes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddFlightRoute($l);

            if ($this->flightRoutesScheduledForDeletion and $this->flightRoutesScheduledForDeletion->contains($l)) {
                $this->flightRoutesScheduledForDeletion->remove($this->flightRoutesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	FlightRoute $flightRoute The flightRoute object to add.
     */
    protected function doAddFlightRoute($flightRoute)
    {
        $this->collFlightRoutes[] = $flightRoute;
        $flightRoute->setAirplane($this);
    }

    /**
     * @param	FlightRoute $flightRoute The flightRoute object to remove.
     * @return Airplane The current object (for fluent API support)
     */
    public function removeFlightRoute($flightRoute)
    {
        if ($this->getFlightRoutes()->contains($flightRoute)) {
            $this->collFlightRoutes->remove($this->collFlightRoutes->search($flightRoute));
            if (null === $this->flightRoutesScheduledForDeletion) {
                $this->flightRoutesScheduledForDeletion = clone $this->collFlightRoutes;
                $this->flightRoutesScheduledForDeletion->clear();
            }
            $this->flightRoutesScheduledForDeletion[] = $flightRoute;
            $flightRoute->setAirplane(null);
        }

        return $this;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airplane is new, it will return
     * an empty collection; or if this Airplane has previously
     * been saved, it will retrieve related FlightRoutes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airplane.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getFlightRoutes($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airplane is new, it will return
     * an empty collection; or if this Airplane has previously
     * been saved, it will retrieve related FlightRoutes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airplane.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesJoinAirportRelatedByIdDepartureLocation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('AirportRelatedByIdDepartureLocation', $join_behavior);

        return $this->getFlightRoutes($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airplane is new, it will return
     * an empty collection; or if this Airplane has previously
     * been saved, it will retrieve related FlightRoutes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airplane.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesJoinAirportRelatedByIdDestinationLocation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('AirportRelatedByIdDestinationLocation', $join_behavior);

        return $this->getFlightRoutes($query, $con);
    }

    /**
     * Clears out the collUsers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Airplane The current object (for fluent API support)
     * @see        addUsers()
     */
    public function clearUsers()
    {
        $this->collUsers = null; // important to set this to null since that means it is uninitialized
        $this->collUsersPartial = null;

        return $this;
    }

    /**
     * reset is the collUsers collection loaded partially
     *
     * @return void
     */
    public function resetPartialUsers($v = true)
    {
        $this->collUsersPartial = $v;
    }

    /**
     * Initializes the collUsers collection.
     *
     * By default this just sets the collUsers collection to an empty array (like clearcollUsers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsers($overrideExisting = true)
    {
        if (null !== $this->collUsers && !$overrideExisting) {
            return;
        }
        $this->collUsers = new PropelObjectCollection();
        $this->collUsers->setModel('User');
    }

    /**
     * Gets an array of User objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Airplane is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|User[] List of User objects
     * @throws PropelException
     */
    public function getUsers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUsersPartial && !$this->isNew();
        if (null === $this->collUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsers) {
                // return empty collection
                $this->initUsers();
            } else {
                $collUsers = UserQuery::create(null, $criteria)
                    ->filterByAirplane($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUsersPartial && count($collUsers)) {
                        $this->initUsers(false);

                        foreach ($collUsers as $obj) {
                            if (false == $this->collUsers->contains($obj)) {
                                $this->collUsers->append($obj);
                            }
                        }

                        $this->collUsersPartial = true;
                    }

                    $collUsers->getInternalIterator()->rewind();

                    return $collUsers;
                }

                if ($partial && $this->collUsers) {
                    foreach ($this->collUsers as $obj) {
                        if ($obj->isNew()) {
                            $collUsers[] = $obj;
                        }
                    }
                }

                $this->collUsers = $collUsers;
                $this->collUsersPartial = false;
            }
        }

        return $this->collUsers;
    }

    /**
     * Sets a collection of User objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $users A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Airplane The current object (for fluent API support)
     */
    public function setUsers(PropelCollection $users, PropelPDO $con = null)
    {
        $usersToDelete = $this->getUsers(new Criteria(), $con)->diff($users);


        $this->usersScheduledForDeletion = $usersToDelete;

        foreach ($usersToDelete as $userRemoved) {
            $userRemoved->setAirplane(null);
        }

        $this->collUsers = null;
        foreach ($users as $user) {
            $this->addUser($user);
        }

        $this->collUsers = $users;
        $this->collUsersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related User objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related User objects.
     * @throws PropelException
     */
    public function countUsers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUsersPartial && !$this->isNew();
        if (null === $this->collUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsers());
            }
            $query = UserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByAirplane($this)
                    ->count($con);
        }

        return count($this->collUsers);
    }

    /**
     * Method called to associate a User object to this object
     * through the User foreign key attribute.
     *
     * @param    User $l User
     * @return Airplane The current object (for fluent API support)
     */
    public function addUser(User $l)
    {
        if ($this->collUsers === null) {
            $this->initUsers();
            $this->collUsersPartial = true;
        }

        if (!in_array($l, $this->collUsers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUser($l);

            if ($this->usersScheduledForDeletion and $this->usersScheduledForDeletion->contains($l)) {
                $this->usersScheduledForDeletion->remove($this->usersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	User $user The user object to add.
     */
    protected function doAddUser($user)
    {
        $this->collUsers[] = $user;
        $user->setAirplane($this);
    }

    /**
     * @param	User $user The user object to remove.
     * @return Airplane The current object (for fluent API support)
     */
    public function removeUser($user)
    {
        if ($this->getUsers()->contains($user)) {
            $this->collUsers->remove($this->collUsers->search($user));
            if (null === $this->usersScheduledForDeletion) {
                $this->usersScheduledForDeletion = clone $this->collUsers;
                $this->usersScheduledForDeletion->clear();
            }
            $this->usersScheduledForDeletion[] = $user;
            $user->setAirplane(null);
        }

        return $this;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airplane is new, it will return
     * an empty collection; or if this Airplane has previously
     * been saved, it will retrieve related Users from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airplane.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|User[] List of User objects
     */
    public function getUsersJoinFile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserQuery::create(null, $criteria);
        $query->joinWith('File', $join_behavior);

        return $this->getUsers($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airplane is new, it will return
     * an empty collection; or if this Airplane has previously
     * been saved, it will retrieve related Users from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airplane.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|User[] List of User objects
     */
    public function getUsersJoinUserRole($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserQuery::create(null, $criteria);
        $query->joinWith('UserRole', $join_behavior);

        return $this->getUsers($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airplane is new, it will return
     * an empty collection; or if this Airplane has previously
     * been saved, it will retrieve related Users from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airplane.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|User[] List of User objects
     */
    public function getUsersJoinUserPersonalInfo($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserQuery::create(null, $criteria);
        $query->joinWith('UserPersonalInfo', $join_behavior);

        return $this->getUsers($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_image = null;
        $this->airplane_model = null;
        $this->airplane_manufacturing_date = null;
        $this->airplane_country = null;
        $this->airplane_serial_number = null;
        $this->airplane_ttaf = null;
        $this->number_of_seats_available = null;
        $this->number_of_seats = null;
        $this->description = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collFlightRoutes) {
                foreach ($this->collFlightRoutes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsers) {
                foreach ($this->collUsers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aFile instanceof Persistent) {
                $this->aFile->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collFlightRoutes instanceof PropelCollection) {
            $this->collFlightRoutes->clearIterator();
        }
        $this->collFlightRoutes = null;
        if ($this->collUsers instanceof PropelCollection) {
            $this->collUsers->clearIterator();
        }
        $this->collUsers = null;
        $this->aFile = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AirplanePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }
    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     Airplane The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[] = AirplanePeer::UPDATED_AT;

        return $this;
    }
}
