<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airport;
use Lamk\PrivatePilotBundle\Model\AirportPeer;
use Lamk\PrivatePilotBundle\Model\AirportQuery;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;

abstract class BaseAirport extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Lamk\\PrivatePilotBundle\\Model\\AirportPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AirportPeer
     */
    protected static $peer;
    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;
    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;
    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;
    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;
    /**
     * The value for the type field.
     * @var        string
     */
    protected $type;
    /**
     * The value for the latitude field.
     * @var        double
     */
    protected $latitude;
    /**
     * The value for the longitude field.
     * @var        double
     */
    protected $longitude;
    /**
     * The value for the elevation field.
     * @var        int
     */
    protected $elevation;
    /**
     * The value for the state field.
     * @var        string
     */
    protected $state;
    /**
     * The value for the city field.
     * @var        string
     */
    protected $city;
    /**
     * The value for the municipality field.
     * @var        string
     */
    protected $municipality;
    /**
     * The value for the country field.
     * @var        string
     */
    protected $country;
    /**
     * The value for the continent field.
     * @var        string
     */
    protected $continent;
    /**
     * The value for the scheduled_service field.
     * @var        string
     */
    protected $scheduled_service;
    /**
     * The value for the gps_code field.
     * @var        string
     */
    protected $gps_code;
    /**
     * The value for the region field.
     * @var        string
     */
    protected $region;
    /**
     * The value for the faa field.
     * @var        string
     */
    protected $faa;
    /**
     * The value for the iata field.
     * @var        string
     */
    protected $iata;
    /**
     * The value for the icao field.
     * @var        string
     */
    protected $icao;
    /**
     * The value for the role field.
     * @var        string
     */
    protected $role;
    /**
     * The value for the enplanements field.
     * @var        int
     */
    protected $enplanements;
    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;
    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;
    /**
     * @var        PropelObjectCollection|FlightRoute[] Collection to store aggregation of FlightRoute objects.
     */
    protected $collFlightRoutesRelatedByIdDepartureLocation;
    protected $collFlightRoutesRelatedByIdDepartureLocationPartial;
    /**
     * @var        PropelObjectCollection|FlightRoute[] Collection to store aggregation of FlightRoute objects.
     */
    protected $collFlightRoutesRelatedByIdDestinationLocation;
    protected $collFlightRoutesRelatedByIdDestinationLocationPartial;
    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;
    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;
    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $flightRoutesRelatedByIdDepartureLocationScheduledForDeletion = null;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $flightRoutesRelatedByIdDestinationLocationScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * Get the [latitude] column value.
     *
     * @return double
     */
    public function getLatitude()
    {

        return $this->latitude;
    }

    /**
     * Get the [longitude] column value.
     *
     * @return double
     */
    public function getLongitude()
    {

        return $this->longitude;
    }

    /**
     * Get the [elevation] column value.
     *
     * @return int
     */
    public function getElevation()
    {

        return $this->elevation;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [city] column value.
     *
     * @return string
     */
    public function getCity()
    {

        return $this->city;
    }

    /**
     * Get the [municipality] column value.
     *
     * @return string
     */
    public function getMunicipality()
    {

        return $this->municipality;
    }

    /**
     * Get the [country] column value.
     *
     * @return string
     */
    public function getCountry()
    {

        return $this->country;
    }

    /**
     * Get the [continent] column value.
     *
     * @return string
     */
    public function getContinent()
    {

        return $this->continent;
    }

    /**
     * Get the [scheduled_service] column value.
     *
     * @return string
     */
    public function getScheduledService()
    {

        return $this->scheduled_service;
    }

    /**
     * Get the [gps_code] column value.
     *
     * @return string
     */
    public function getGpsCode()
    {

        return $this->gps_code;
    }

    /**
     * Get the [region] column value.
     *
     * @return string
     */
    public function getRegion()
    {

        return $this->region;
    }

    /**
     * Get the [faa] column value.
     *
     * @return string
     */
    public function getFaa()
    {

        return $this->faa;
    }

    /**
     * Get the [iata] column value.
     *
     * @return string
     */
    public function getIata()
    {

        return $this->iata;
    }

    /**
     * Get the [icao] column value.
     *
     * @return string
     */
    public function getIcao()
    {

        return $this->icao;
    }

    /**
     * Get the [role] column value.
     *
     * @return string
     */
    public function getRole()
    {

        return $this->role;
    }

    /**
     * Get the [enplanements] column value.
     *
     * @return int
     */
    public function getEnplanements()
    {

        return $this->enplanements;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($this->updated_at === null) {
            return null;
        }

        if ($this->updated_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = AirportPeer::ID;
        }


        return $this;
    }
// setId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = AirportPeer::NAME;
        }


        return $this;
    }
// setName()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = AirportPeer::DESCRIPTION;
        }


        return $this;
    }
// setDescription()

    /**
     * Set the value of [type] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[] = AirportPeer::TYPE;
        }


        return $this;
    }
// setType()

    /**
     * Set the value of [latitude] column.
     *
     * @param  double $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setLatitude($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->latitude !== $v) {
            $this->latitude = $v;
            $this->modifiedColumns[] = AirportPeer::LATITUDE;
        }


        return $this;
    }
// setLatitude()

    /**
     * Set the value of [longitude] column.
     *
     * @param  double $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setLongitude($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->longitude !== $v) {
            $this->longitude = $v;
            $this->modifiedColumns[] = AirportPeer::LONGITUDE;
        }


        return $this;
    }
// setLongitude()

    /**
     * Set the value of [elevation] column.
     *
     * @param  int $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setElevation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->elevation !== $v) {
            $this->elevation = $v;
            $this->modifiedColumns[] = AirportPeer::ELEVATION;
        }


        return $this;
    }
// setElevation()

    /**
     * Set the value of [state] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = AirportPeer::STATE;
        }


        return $this;
    }
// setState()

    /**
     * Set the value of [city] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city !== $v) {
            $this->city = $v;
            $this->modifiedColumns[] = AirportPeer::CITY;
        }


        return $this;
    }
// setCity()

    /**
     * Set the value of [municipality] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setMunicipality($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->municipality !== $v) {
            $this->municipality = $v;
            $this->modifiedColumns[] = AirportPeer::MUNICIPALITY;
        }


        return $this;
    }
// setMunicipality()

    /**
     * Set the value of [country] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->country !== $v) {
            $this->country = $v;
            $this->modifiedColumns[] = AirportPeer::COUNTRY;
        }


        return $this;
    }
// setCountry()

    /**
     * Set the value of [continent] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setContinent($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->continent !== $v) {
            $this->continent = $v;
            $this->modifiedColumns[] = AirportPeer::CONTINENT;
        }


        return $this;
    }
// setContinent()

    /**
     * Set the value of [scheduled_service] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setScheduledService($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->scheduled_service !== $v) {
            $this->scheduled_service = $v;
            $this->modifiedColumns[] = AirportPeer::SCHEDULED_SERVICE;
        }


        return $this;
    }
// setScheduledService()

    /**
     * Set the value of [gps_code] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setGpsCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gps_code !== $v) {
            $this->gps_code = $v;
            $this->modifiedColumns[] = AirportPeer::GPS_CODE;
        }


        return $this;
    }
// setGpsCode()

    /**
     * Set the value of [region] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setRegion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->region !== $v) {
            $this->region = $v;
            $this->modifiedColumns[] = AirportPeer::REGION;
        }


        return $this;
    }
// setRegion()

    /**
     * Set the value of [faa] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setFaa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->faa !== $v) {
            $this->faa = $v;
            $this->modifiedColumns[] = AirportPeer::FAA;
        }


        return $this;
    }
// setFaa()

    /**
     * Set the value of [iata] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setIata($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iata !== $v) {
            $this->iata = $v;
            $this->modifiedColumns[] = AirportPeer::IATA;
        }


        return $this;
    }
// setIata()

    /**
     * Set the value of [icao] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setIcao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icao !== $v) {
            $this->icao = $v;
            $this->modifiedColumns[] = AirportPeer::ICAO;
        }


        return $this;
    }
// setIcao()

    /**
     * Set the value of [role] column.
     *
     * @param  string $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setRole($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->role !== $v) {
            $this->role = $v;
            $this->modifiedColumns[] = AirportPeer::ROLE;
        }


        return $this;
    }
// setRole()

    /**
     * Set the value of [enplanements] column.
     *
     * @param  int $v new value
     * @return Airport The current object (for fluent API support)
     */
    public function setEnplanements($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->enplanements !== $v) {
            $this->enplanements = $v;
            $this->modifiedColumns[] = AirportPeer::ENPLANEMENTS;
        }


        return $this;
    }
// setEnplanements()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Airport The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = AirportPeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Airport The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = AirportPeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    }
// hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->description = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->type = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->latitude = ($row[$startcol + 4] !== null) ? (double) $row[$startcol + 4] : null;
            $this->longitude = ($row[$startcol + 5] !== null) ? (double) $row[$startcol + 5] : null;
            $this->elevation = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->state = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->city = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->municipality = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->country = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->continent = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->scheduled_service = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->gps_code = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->region = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->faa = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->iata = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->icao = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->role = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->enplanements = ($row[$startcol + 19] !== null) ? (int) $row[$startcol + 19] : null;
            $this->created_at = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->updated_at = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 22; // 22 = AirportPeer::NUM_HYDRATE_COLUMNS.
        } catch (Exception $e) {
            throw new PropelException("Error populating Airport object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        
    }
// ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AirportPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AirportPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?
            $this->collFlightRoutesRelatedByIdDepartureLocation = null;

            $this->collFlightRoutesRelatedByIdDestinationLocation = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AirportPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AirportQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AirportPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                if (!$this->isColumnModified(AirportPeer::CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(AirportPeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(AirportPeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AirportPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion !== null) {
                if (!$this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion->isEmpty()) {
                    foreach ($this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion as $flightRouteRelatedByIdDepartureLocation) {
                        // need to save related object because we set the relation to null
                        $flightRouteRelatedByIdDepartureLocation->save($con);
                    }
                    $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion = null;
                }
            }

            if ($this->collFlightRoutesRelatedByIdDepartureLocation !== null) {
                foreach ($this->collFlightRoutesRelatedByIdDepartureLocation as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion !== null) {
                if (!$this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion->isEmpty()) {
                    foreach ($this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion as $flightRouteRelatedByIdDestinationLocation) {
                        // need to save related object because we set the relation to null
                        $flightRouteRelatedByIdDestinationLocation->save($con);
                    }
                    $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion = null;
                }
            }

            if ($this->collFlightRoutesRelatedByIdDestinationLocation !== null) {
                foreach ($this->collFlightRoutesRelatedByIdDestinationLocation as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;
        }

        return $affectedRows;
    }
// doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = AirportPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . AirportPeer::ID . ')');
        }

        // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AirportPeer::ID)) {
            $modifiedColumns[':p' . $index++] = '`id`';
        }
        if ($this->isColumnModified(AirportPeer::NAME)) {
            $modifiedColumns[':p' . $index++] = '`name`';
        }
        if ($this->isColumnModified(AirportPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++] = '`description`';
        }
        if ($this->isColumnModified(AirportPeer::TYPE)) {
            $modifiedColumns[':p' . $index++] = '`type`';
        }
        if ($this->isColumnModified(AirportPeer::LATITUDE)) {
            $modifiedColumns[':p' . $index++] = '`latitude`';
        }
        if ($this->isColumnModified(AirportPeer::LONGITUDE)) {
            $modifiedColumns[':p' . $index++] = '`longitude`';
        }
        if ($this->isColumnModified(AirportPeer::ELEVATION)) {
            $modifiedColumns[':p' . $index++] = '`elevation`';
        }
        if ($this->isColumnModified(AirportPeer::STATE)) {
            $modifiedColumns[':p' . $index++] = '`state`';
        }
        if ($this->isColumnModified(AirportPeer::CITY)) {
            $modifiedColumns[':p' . $index++] = '`city`';
        }
        if ($this->isColumnModified(AirportPeer::MUNICIPALITY)) {
            $modifiedColumns[':p' . $index++] = '`municipality`';
        }
        if ($this->isColumnModified(AirportPeer::COUNTRY)) {
            $modifiedColumns[':p' . $index++] = '`country`';
        }
        if ($this->isColumnModified(AirportPeer::CONTINENT)) {
            $modifiedColumns[':p' . $index++] = '`continent`';
        }
        if ($this->isColumnModified(AirportPeer::SCHEDULED_SERVICE)) {
            $modifiedColumns[':p' . $index++] = '`scheduled_service`';
        }
        if ($this->isColumnModified(AirportPeer::GPS_CODE)) {
            $modifiedColumns[':p' . $index++] = '`gps_code`';
        }
        if ($this->isColumnModified(AirportPeer::REGION)) {
            $modifiedColumns[':p' . $index++] = '`region`';
        }
        if ($this->isColumnModified(AirportPeer::FAA)) {
            $modifiedColumns[':p' . $index++] = '`faa`';
        }
        if ($this->isColumnModified(AirportPeer::IATA)) {
            $modifiedColumns[':p' . $index++] = '`iata`';
        }
        if ($this->isColumnModified(AirportPeer::ICAO)) {
            $modifiedColumns[':p' . $index++] = '`icao`';
        }
        if ($this->isColumnModified(AirportPeer::ROLE)) {
            $modifiedColumns[':p' . $index++] = '`role`';
        }
        if ($this->isColumnModified(AirportPeer::ENPLANEMENTS)) {
            $modifiedColumns[':p' . $index++] = '`enplanements`';
        }
        if ($this->isColumnModified(AirportPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`created_at`';
        }
        if ($this->isColumnModified(AirportPeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `airport` (%s) VALUES (%s)', implode(', ', $modifiedColumns), implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`type`':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case '`latitude`':
                        $stmt->bindValue($identifier, $this->latitude, PDO::PARAM_STR);
                        break;
                    case '`longitude`':
                        $stmt->bindValue($identifier, $this->longitude, PDO::PARAM_STR);
                        break;
                    case '`elevation`':
                        $stmt->bindValue($identifier, $this->elevation, PDO::PARAM_INT);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`city`':
                        $stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
                        break;
                    case '`municipality`':
                        $stmt->bindValue($identifier, $this->municipality, PDO::PARAM_STR);
                        break;
                    case '`country`':
                        $stmt->bindValue($identifier, $this->country, PDO::PARAM_STR);
                        break;
                    case '`continent`':
                        $stmt->bindValue($identifier, $this->continent, PDO::PARAM_STR);
                        break;
                    case '`scheduled_service`':
                        $stmt->bindValue($identifier, $this->scheduled_service, PDO::PARAM_STR);
                        break;
                    case '`gps_code`':
                        $stmt->bindValue($identifier, $this->gps_code, PDO::PARAM_STR);
                        break;
                    case '`region`':
                        $stmt->bindValue($identifier, $this->region, PDO::PARAM_STR);
                        break;
                    case '`faa`':
                        $stmt->bindValue($identifier, $this->faa, PDO::PARAM_STR);
                        break;
                    case '`iata`':
                        $stmt->bindValue($identifier, $this->iata, PDO::PARAM_STR);
                        break;
                    case '`icao`':
                        $stmt->bindValue($identifier, $this->icao, PDO::PARAM_STR);
                        break;
                    case '`role`':
                        $stmt->bindValue($identifier, $this->role, PDO::PARAM_STR);
                        break;
                    case '`enplanements`':
                        $stmt->bindValue($identifier, $this->enplanements, PDO::PARAM_INT);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }
    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = AirportPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


            if ($this->collFlightRoutesRelatedByIdDepartureLocation !== null) {
                foreach ($this->collFlightRoutesRelatedByIdDepartureLocation as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }

            if ($this->collFlightRoutesRelatedByIdDestinationLocation !== null) {
                foreach ($this->collFlightRoutesRelatedByIdDestinationLocation as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AirportPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getDescription();
                break;
            case 3:
                return $this->getType();
                break;
            case 4:
                return $this->getLatitude();
                break;
            case 5:
                return $this->getLongitude();
                break;
            case 6:
                return $this->getElevation();
                break;
            case 7:
                return $this->getState();
                break;
            case 8:
                return $this->getCity();
                break;
            case 9:
                return $this->getMunicipality();
                break;
            case 10:
                return $this->getCountry();
                break;
            case 11:
                return $this->getContinent();
                break;
            case 12:
                return $this->getScheduledService();
                break;
            case 13:
                return $this->getGpsCode();
                break;
            case 14:
                return $this->getRegion();
                break;
            case 15:
                return $this->getFaa();
                break;
            case 16:
                return $this->getIata();
                break;
            case 17:
                return $this->getIcao();
                break;
            case 18:
                return $this->getRole();
                break;
            case 19:
                return $this->getEnplanements();
                break;
            case 20:
                return $this->getCreatedAt();
                break;
            case 21:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Airport'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Airport'][$this->getPrimaryKey()] = true;
        $keys = AirportPeer::getFieldNames($keyType);
        $result = array(
          $keys[0]  => $this->getId(),
          $keys[1]  => $this->getName(),
          $keys[2]  => $this->getDescription(),
          $keys[3]  => $this->getType(),
          $keys[4]  => $this->getLatitude(),
          $keys[5]  => $this->getLongitude(),
          $keys[6]  => $this->getElevation(),
          $keys[7]  => $this->getState(),
          $keys[8]  => $this->getCity(),
          $keys[9]  => $this->getMunicipality(),
          $keys[10] => $this->getCountry(),
          $keys[11] => $this->getContinent(),
          $keys[12] => $this->getScheduledService(),
          $keys[13] => $this->getGpsCode(),
          $keys[14] => $this->getRegion(),
          $keys[15] => $this->getFaa(),
          $keys[16] => $this->getIata(),
          $keys[17] => $this->getIcao(),
          $keys[18] => $this->getRole(),
          $keys[19] => $this->getEnplanements(),
          $keys[20] => $this->getCreatedAt(),
          $keys[21] => $this->getUpdatedAt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collFlightRoutesRelatedByIdDepartureLocation) {
                $result['FlightRoutesRelatedByIdDepartureLocation'] = $this->collFlightRoutesRelatedByIdDepartureLocation->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFlightRoutesRelatedByIdDestinationLocation) {
                $result['FlightRoutesRelatedByIdDestinationLocation'] = $this->collFlightRoutesRelatedByIdDestinationLocation->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AirportPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setDescription($value);
                break;
            case 3:
                $this->setType($value);
                break;
            case 4:
                $this->setLatitude($value);
                break;
            case 5:
                $this->setLongitude($value);
                break;
            case 6:
                $this->setElevation($value);
                break;
            case 7:
                $this->setState($value);
                break;
            case 8:
                $this->setCity($value);
                break;
            case 9:
                $this->setMunicipality($value);
                break;
            case 10:
                $this->setCountry($value);
                break;
            case 11:
                $this->setContinent($value);
                break;
            case 12:
                $this->setScheduledService($value);
                break;
            case 13:
                $this->setGpsCode($value);
                break;
            case 14:
                $this->setRegion($value);
                break;
            case 15:
                $this->setFaa($value);
                break;
            case 16:
                $this->setIata($value);
                break;
            case 17:
                $this->setIcao($value);
                break;
            case 18:
                $this->setRole($value);
                break;
            case 19:
                $this->setEnplanements($value);
                break;
            case 20:
                $this->setCreatedAt($value);
                break;
            case 21:
                $this->setUpdatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AirportPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr))
            $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr))
            $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr))
            $this->setDescription($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr))
            $this->setType($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr))
            $this->setLatitude($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr))
            $this->setLongitude($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr))
            $this->setElevation($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr))
            $this->setState($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr))
            $this->setCity($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr))
            $this->setMunicipality($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr))
            $this->setCountry($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr))
            $this->setContinent($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr))
            $this->setScheduledService($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr))
            $this->setGpsCode($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr))
            $this->setRegion($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr))
            $this->setFaa($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr))
            $this->setIata($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr))
            $this->setIcao($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr))
            $this->setRole($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr))
            $this->setEnplanements($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr))
            $this->setCreatedAt($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr))
            $this->setUpdatedAt($arr[$keys[21]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AirportPeer::DATABASE_NAME);

        if ($this->isColumnModified(AirportPeer::ID))
            $criteria->add(AirportPeer::ID, $this->id);
        if ($this->isColumnModified(AirportPeer::NAME))
            $criteria->add(AirportPeer::NAME, $this->name);
        if ($this->isColumnModified(AirportPeer::DESCRIPTION))
            $criteria->add(AirportPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(AirportPeer::TYPE))
            $criteria->add(AirportPeer::TYPE, $this->type);
        if ($this->isColumnModified(AirportPeer::LATITUDE))
            $criteria->add(AirportPeer::LATITUDE, $this->latitude);
        if ($this->isColumnModified(AirportPeer::LONGITUDE))
            $criteria->add(AirportPeer::LONGITUDE, $this->longitude);
        if ($this->isColumnModified(AirportPeer::ELEVATION))
            $criteria->add(AirportPeer::ELEVATION, $this->elevation);
        if ($this->isColumnModified(AirportPeer::STATE))
            $criteria->add(AirportPeer::STATE, $this->state);
        if ($this->isColumnModified(AirportPeer::CITY))
            $criteria->add(AirportPeer::CITY, $this->city);
        if ($this->isColumnModified(AirportPeer::MUNICIPALITY))
            $criteria->add(AirportPeer::MUNICIPALITY, $this->municipality);
        if ($this->isColumnModified(AirportPeer::COUNTRY))
            $criteria->add(AirportPeer::COUNTRY, $this->country);
        if ($this->isColumnModified(AirportPeer::CONTINENT))
            $criteria->add(AirportPeer::CONTINENT, $this->continent);
        if ($this->isColumnModified(AirportPeer::SCHEDULED_SERVICE))
            $criteria->add(AirportPeer::SCHEDULED_SERVICE, $this->scheduled_service);
        if ($this->isColumnModified(AirportPeer::GPS_CODE))
            $criteria->add(AirportPeer::GPS_CODE, $this->gps_code);
        if ($this->isColumnModified(AirportPeer::REGION))
            $criteria->add(AirportPeer::REGION, $this->region);
        if ($this->isColumnModified(AirportPeer::FAA))
            $criteria->add(AirportPeer::FAA, $this->faa);
        if ($this->isColumnModified(AirportPeer::IATA))
            $criteria->add(AirportPeer::IATA, $this->iata);
        if ($this->isColumnModified(AirportPeer::ICAO))
            $criteria->add(AirportPeer::ICAO, $this->icao);
        if ($this->isColumnModified(AirportPeer::ROLE))
            $criteria->add(AirportPeer::ROLE, $this->role);
        if ($this->isColumnModified(AirportPeer::ENPLANEMENTS))
            $criteria->add(AirportPeer::ENPLANEMENTS, $this->enplanements);
        if ($this->isColumnModified(AirportPeer::CREATED_AT))
            $criteria->add(AirportPeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(AirportPeer::UPDATED_AT))
            $criteria->add(AirportPeer::UPDATED_AT, $this->updated_at);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AirportPeer::DATABASE_NAME);
        $criteria->add(AirportPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Airport (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setType($this->getType());
        $copyObj->setLatitude($this->getLatitude());
        $copyObj->setLongitude($this->getLongitude());
        $copyObj->setElevation($this->getElevation());
        $copyObj->setState($this->getState());
        $copyObj->setCity($this->getCity());
        $copyObj->setMunicipality($this->getMunicipality());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setContinent($this->getContinent());
        $copyObj->setScheduledService($this->getScheduledService());
        $copyObj->setGpsCode($this->getGpsCode());
        $copyObj->setRegion($this->getRegion());
        $copyObj->setFaa($this->getFaa());
        $copyObj->setIata($this->getIata());
        $copyObj->setIcao($this->getIcao());
        $copyObj->setRole($this->getRole());
        $copyObj->setEnplanements($this->getEnplanements());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getFlightRoutesRelatedByIdDepartureLocation() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlightRouteRelatedByIdDepartureLocation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFlightRoutesRelatedByIdDestinationLocation() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlightRouteRelatedByIdDestinationLocation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Airport Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AirportPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AirportPeer();
        }

        return self::$peer;
    }

    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('FlightRouteRelatedByIdDepartureLocation' == $relationName) {
            $this->initFlightRoutesRelatedByIdDepartureLocation();
        }
        if ('FlightRouteRelatedByIdDestinationLocation' == $relationName) {
            $this->initFlightRoutesRelatedByIdDestinationLocation();
        }
    }

    /**
     * Clears out the collFlightRoutesRelatedByIdDepartureLocation collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Airport The current object (for fluent API support)
     * @see        addFlightRoutesRelatedByIdDepartureLocation()
     */
    public function clearFlightRoutesRelatedByIdDepartureLocation()
    {
        $this->collFlightRoutesRelatedByIdDepartureLocation = null; // important to set this to null since that means it is uninitialized
        $this->collFlightRoutesRelatedByIdDepartureLocationPartial = null;

        return $this;
    }

    /**
     * reset is the collFlightRoutesRelatedByIdDepartureLocation collection loaded partially
     *
     * @return void
     */
    public function resetPartialFlightRoutesRelatedByIdDepartureLocation($v = true)
    {
        $this->collFlightRoutesRelatedByIdDepartureLocationPartial = $v;
    }

    /**
     * Initializes the collFlightRoutesRelatedByIdDepartureLocation collection.
     *
     * By default this just sets the collFlightRoutesRelatedByIdDepartureLocation collection to an empty array (like clearcollFlightRoutesRelatedByIdDepartureLocation());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlightRoutesRelatedByIdDepartureLocation($overrideExisting = true)
    {
        if (null !== $this->collFlightRoutesRelatedByIdDepartureLocation && !$overrideExisting) {
            return;
        }
        $this->collFlightRoutesRelatedByIdDepartureLocation = new PropelObjectCollection();
        $this->collFlightRoutesRelatedByIdDepartureLocation->setModel('FlightRoute');
    }

    /**
     * Gets an array of FlightRoute objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Airport is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     * @throws PropelException
     */
    public function getFlightRoutesRelatedByIdDepartureLocation($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesRelatedByIdDepartureLocationPartial && !$this->isNew();
        if (null === $this->collFlightRoutesRelatedByIdDepartureLocation || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutesRelatedByIdDepartureLocation) {
                // return empty collection
                $this->initFlightRoutesRelatedByIdDepartureLocation();
            } else {
                $collFlightRoutesRelatedByIdDepartureLocation = FlightRouteQuery::create(null, $criteria)
                    ->filterByAirportRelatedByIdDepartureLocation($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collFlightRoutesRelatedByIdDepartureLocationPartial && count($collFlightRoutesRelatedByIdDepartureLocation)) {
                        $this->initFlightRoutesRelatedByIdDepartureLocation(false);

                        foreach ($collFlightRoutesRelatedByIdDepartureLocation as $obj) {
                            if (false == $this->collFlightRoutesRelatedByIdDepartureLocation->contains($obj)) {
                                $this->collFlightRoutesRelatedByIdDepartureLocation->append($obj);
                            }
                        }

                        $this->collFlightRoutesRelatedByIdDepartureLocationPartial = true;
                    }

                    $collFlightRoutesRelatedByIdDepartureLocation->getInternalIterator()->rewind();

                    return $collFlightRoutesRelatedByIdDepartureLocation;
                }

                if ($partial && $this->collFlightRoutesRelatedByIdDepartureLocation) {
                    foreach ($this->collFlightRoutesRelatedByIdDepartureLocation as $obj) {
                        if ($obj->isNew()) {
                            $collFlightRoutesRelatedByIdDepartureLocation[] = $obj;
                        }
                    }
                }

                $this->collFlightRoutesRelatedByIdDepartureLocation = $collFlightRoutesRelatedByIdDepartureLocation;
                $this->collFlightRoutesRelatedByIdDepartureLocationPartial = false;
            }
        }

        return $this->collFlightRoutesRelatedByIdDepartureLocation;
    }

    /**
     * Sets a collection of FlightRouteRelatedByIdDepartureLocation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $flightRoutesRelatedByIdDepartureLocation A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Airport The current object (for fluent API support)
     */
    public function setFlightRoutesRelatedByIdDepartureLocation(PropelCollection $flightRoutesRelatedByIdDepartureLocation, PropelPDO $con = null)
    {
        $flightRoutesRelatedByIdDepartureLocationToDelete = $this->getFlightRoutesRelatedByIdDepartureLocation(new Criteria(), $con)->diff($flightRoutesRelatedByIdDepartureLocation);


        $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion = $flightRoutesRelatedByIdDepartureLocationToDelete;

        foreach ($flightRoutesRelatedByIdDepartureLocationToDelete as $flightRouteRelatedByIdDepartureLocationRemoved) {
            $flightRouteRelatedByIdDepartureLocationRemoved->setAirportRelatedByIdDepartureLocation(null);
        }

        $this->collFlightRoutesRelatedByIdDepartureLocation = null;
        foreach ($flightRoutesRelatedByIdDepartureLocation as $flightRouteRelatedByIdDepartureLocation) {
            $this->addFlightRouteRelatedByIdDepartureLocation($flightRouteRelatedByIdDepartureLocation);
        }

        $this->collFlightRoutesRelatedByIdDepartureLocation = $flightRoutesRelatedByIdDepartureLocation;
        $this->collFlightRoutesRelatedByIdDepartureLocationPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlightRoute objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related FlightRoute objects.
     * @throws PropelException
     */
    public function countFlightRoutesRelatedByIdDepartureLocation(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesRelatedByIdDepartureLocationPartial && !$this->isNew();
        if (null === $this->collFlightRoutesRelatedByIdDepartureLocation || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutesRelatedByIdDepartureLocation) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlightRoutesRelatedByIdDepartureLocation());
            }
            $query = FlightRouteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByAirportRelatedByIdDepartureLocation($this)
                    ->count($con);
        }

        return count($this->collFlightRoutesRelatedByIdDepartureLocation);
    }

    /**
     * Method called to associate a FlightRoute object to this object
     * through the FlightRoute foreign key attribute.
     *
     * @param    FlightRoute $l FlightRoute
     * @return Airport The current object (for fluent API support)
     */
    public function addFlightRouteRelatedByIdDepartureLocation(FlightRoute $l)
    {
        if ($this->collFlightRoutesRelatedByIdDepartureLocation === null) {
            $this->initFlightRoutesRelatedByIdDepartureLocation();
            $this->collFlightRoutesRelatedByIdDepartureLocationPartial = true;
        }

        if (!in_array($l, $this->collFlightRoutesRelatedByIdDepartureLocation->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddFlightRouteRelatedByIdDepartureLocation($l);

            if ($this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion and $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion->contains($l)) {
                $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion->remove($this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	FlightRouteRelatedByIdDepartureLocation $flightRouteRelatedByIdDepartureLocation The flightRouteRelatedByIdDepartureLocation object to add.
     */
    protected function doAddFlightRouteRelatedByIdDepartureLocation($flightRouteRelatedByIdDepartureLocation)
    {
        $this->collFlightRoutesRelatedByIdDepartureLocation[] = $flightRouteRelatedByIdDepartureLocation;
        $flightRouteRelatedByIdDepartureLocation->setAirportRelatedByIdDepartureLocation($this);
    }

    /**
     * @param	FlightRouteRelatedByIdDepartureLocation $flightRouteRelatedByIdDepartureLocation The flightRouteRelatedByIdDepartureLocation object to remove.
     * @return Airport The current object (for fluent API support)
     */
    public function removeFlightRouteRelatedByIdDepartureLocation($flightRouteRelatedByIdDepartureLocation)
    {
        if ($this->getFlightRoutesRelatedByIdDepartureLocation()->contains($flightRouteRelatedByIdDepartureLocation)) {
            $this->collFlightRoutesRelatedByIdDepartureLocation->remove($this->collFlightRoutesRelatedByIdDepartureLocation->search($flightRouteRelatedByIdDepartureLocation));
            if (null === $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion) {
                $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion = clone $this->collFlightRoutesRelatedByIdDepartureLocation;
                $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion->clear();
            }
            $this->flightRoutesRelatedByIdDepartureLocationScheduledForDeletion[] = $flightRouteRelatedByIdDepartureLocation;
            $flightRouteRelatedByIdDepartureLocation->setAirportRelatedByIdDepartureLocation(null);
        }

        return $this;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airport is new, it will return
     * an empty collection; or if this Airport has previously
     * been saved, it will retrieve related FlightRoutesRelatedByIdDepartureLocation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airport.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesRelatedByIdDepartureLocationJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getFlightRoutesRelatedByIdDepartureLocation($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airport is new, it will return
     * an empty collection; or if this Airport has previously
     * been saved, it will retrieve related FlightRoutesRelatedByIdDepartureLocation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airport.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesRelatedByIdDepartureLocationJoinAirplane($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('Airplane', $join_behavior);

        return $this->getFlightRoutesRelatedByIdDepartureLocation($query, $con);
    }

    /**
     * Clears out the collFlightRoutesRelatedByIdDestinationLocation collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Airport The current object (for fluent API support)
     * @see        addFlightRoutesRelatedByIdDestinationLocation()
     */
    public function clearFlightRoutesRelatedByIdDestinationLocation()
    {
        $this->collFlightRoutesRelatedByIdDestinationLocation = null; // important to set this to null since that means it is uninitialized
        $this->collFlightRoutesRelatedByIdDestinationLocationPartial = null;

        return $this;
    }

    /**
     * reset is the collFlightRoutesRelatedByIdDestinationLocation collection loaded partially
     *
     * @return void
     */
    public function resetPartialFlightRoutesRelatedByIdDestinationLocation($v = true)
    {
        $this->collFlightRoutesRelatedByIdDestinationLocationPartial = $v;
    }

    /**
     * Initializes the collFlightRoutesRelatedByIdDestinationLocation collection.
     *
     * By default this just sets the collFlightRoutesRelatedByIdDestinationLocation collection to an empty array (like clearcollFlightRoutesRelatedByIdDestinationLocation());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlightRoutesRelatedByIdDestinationLocation($overrideExisting = true)
    {
        if (null !== $this->collFlightRoutesRelatedByIdDestinationLocation && !$overrideExisting) {
            return;
        }
        $this->collFlightRoutesRelatedByIdDestinationLocation = new PropelObjectCollection();
        $this->collFlightRoutesRelatedByIdDestinationLocation->setModel('FlightRoute');
    }

    /**
     * Gets an array of FlightRoute objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Airport is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     * @throws PropelException
     */
    public function getFlightRoutesRelatedByIdDestinationLocation($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesRelatedByIdDestinationLocationPartial && !$this->isNew();
        if (null === $this->collFlightRoutesRelatedByIdDestinationLocation || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutesRelatedByIdDestinationLocation) {
                // return empty collection
                $this->initFlightRoutesRelatedByIdDestinationLocation();
            } else {
                $collFlightRoutesRelatedByIdDestinationLocation = FlightRouteQuery::create(null, $criteria)
                    ->filterByAirportRelatedByIdDestinationLocation($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collFlightRoutesRelatedByIdDestinationLocationPartial && count($collFlightRoutesRelatedByIdDestinationLocation)) {
                        $this->initFlightRoutesRelatedByIdDestinationLocation(false);

                        foreach ($collFlightRoutesRelatedByIdDestinationLocation as $obj) {
                            if (false == $this->collFlightRoutesRelatedByIdDestinationLocation->contains($obj)) {
                                $this->collFlightRoutesRelatedByIdDestinationLocation->append($obj);
                            }
                        }

                        $this->collFlightRoutesRelatedByIdDestinationLocationPartial = true;
                    }

                    $collFlightRoutesRelatedByIdDestinationLocation->getInternalIterator()->rewind();

                    return $collFlightRoutesRelatedByIdDestinationLocation;
                }

                if ($partial && $this->collFlightRoutesRelatedByIdDestinationLocation) {
                    foreach ($this->collFlightRoutesRelatedByIdDestinationLocation as $obj) {
                        if ($obj->isNew()) {
                            $collFlightRoutesRelatedByIdDestinationLocation[] = $obj;
                        }
                    }
                }

                $this->collFlightRoutesRelatedByIdDestinationLocation = $collFlightRoutesRelatedByIdDestinationLocation;
                $this->collFlightRoutesRelatedByIdDestinationLocationPartial = false;
            }
        }

        return $this->collFlightRoutesRelatedByIdDestinationLocation;
    }

    /**
     * Sets a collection of FlightRouteRelatedByIdDestinationLocation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $flightRoutesRelatedByIdDestinationLocation A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Airport The current object (for fluent API support)
     */
    public function setFlightRoutesRelatedByIdDestinationLocation(PropelCollection $flightRoutesRelatedByIdDestinationLocation, PropelPDO $con = null)
    {
        $flightRoutesRelatedByIdDestinationLocationToDelete = $this->getFlightRoutesRelatedByIdDestinationLocation(new Criteria(), $con)->diff($flightRoutesRelatedByIdDestinationLocation);


        $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion = $flightRoutesRelatedByIdDestinationLocationToDelete;

        foreach ($flightRoutesRelatedByIdDestinationLocationToDelete as $flightRouteRelatedByIdDestinationLocationRemoved) {
            $flightRouteRelatedByIdDestinationLocationRemoved->setAirportRelatedByIdDestinationLocation(null);
        }

        $this->collFlightRoutesRelatedByIdDestinationLocation = null;
        foreach ($flightRoutesRelatedByIdDestinationLocation as $flightRouteRelatedByIdDestinationLocation) {
            $this->addFlightRouteRelatedByIdDestinationLocation($flightRouteRelatedByIdDestinationLocation);
        }

        $this->collFlightRoutesRelatedByIdDestinationLocation = $flightRoutesRelatedByIdDestinationLocation;
        $this->collFlightRoutesRelatedByIdDestinationLocationPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlightRoute objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related FlightRoute objects.
     * @throws PropelException
     */
    public function countFlightRoutesRelatedByIdDestinationLocation(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collFlightRoutesRelatedByIdDestinationLocationPartial && !$this->isNew();
        if (null === $this->collFlightRoutesRelatedByIdDestinationLocation || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlightRoutesRelatedByIdDestinationLocation) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlightRoutesRelatedByIdDestinationLocation());
            }
            $query = FlightRouteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByAirportRelatedByIdDestinationLocation($this)
                    ->count($con);
        }

        return count($this->collFlightRoutesRelatedByIdDestinationLocation);
    }

    /**
     * Method called to associate a FlightRoute object to this object
     * through the FlightRoute foreign key attribute.
     *
     * @param    FlightRoute $l FlightRoute
     * @return Airport The current object (for fluent API support)
     */
    public function addFlightRouteRelatedByIdDestinationLocation(FlightRoute $l)
    {
        if ($this->collFlightRoutesRelatedByIdDestinationLocation === null) {
            $this->initFlightRoutesRelatedByIdDestinationLocation();
            $this->collFlightRoutesRelatedByIdDestinationLocationPartial = true;
        }

        if (!in_array($l, $this->collFlightRoutesRelatedByIdDestinationLocation->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddFlightRouteRelatedByIdDestinationLocation($l);

            if ($this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion and $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion->contains($l)) {
                $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion->remove($this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	FlightRouteRelatedByIdDestinationLocation $flightRouteRelatedByIdDestinationLocation The flightRouteRelatedByIdDestinationLocation object to add.
     */
    protected function doAddFlightRouteRelatedByIdDestinationLocation($flightRouteRelatedByIdDestinationLocation)
    {
        $this->collFlightRoutesRelatedByIdDestinationLocation[] = $flightRouteRelatedByIdDestinationLocation;
        $flightRouteRelatedByIdDestinationLocation->setAirportRelatedByIdDestinationLocation($this);
    }

    /**
     * @param	FlightRouteRelatedByIdDestinationLocation $flightRouteRelatedByIdDestinationLocation The flightRouteRelatedByIdDestinationLocation object to remove.
     * @return Airport The current object (for fluent API support)
     */
    public function removeFlightRouteRelatedByIdDestinationLocation($flightRouteRelatedByIdDestinationLocation)
    {
        if ($this->getFlightRoutesRelatedByIdDestinationLocation()->contains($flightRouteRelatedByIdDestinationLocation)) {
            $this->collFlightRoutesRelatedByIdDestinationLocation->remove($this->collFlightRoutesRelatedByIdDestinationLocation->search($flightRouteRelatedByIdDestinationLocation));
            if (null === $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion) {
                $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion = clone $this->collFlightRoutesRelatedByIdDestinationLocation;
                $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion->clear();
            }
            $this->flightRoutesRelatedByIdDestinationLocationScheduledForDeletion[] = $flightRouteRelatedByIdDestinationLocation;
            $flightRouteRelatedByIdDestinationLocation->setAirportRelatedByIdDestinationLocation(null);
        }

        return $this;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airport is new, it will return
     * an empty collection; or if this Airport has previously
     * been saved, it will retrieve related FlightRoutesRelatedByIdDestinationLocation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airport.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesRelatedByIdDestinationLocationJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getFlightRoutesRelatedByIdDestinationLocation($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Airport is new, it will return
     * an empty collection; or if this Airport has previously
     * been saved, it will retrieve related FlightRoutesRelatedByIdDestinationLocation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Airport.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|FlightRoute[] List of FlightRoute objects
     */
    public function getFlightRoutesRelatedByIdDestinationLocationJoinAirplane($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = FlightRouteQuery::create(null, $criteria);
        $query->joinWith('Airplane', $join_behavior);

        return $this->getFlightRoutesRelatedByIdDestinationLocation($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->description = null;
        $this->type = null;
        $this->latitude = null;
        $this->longitude = null;
        $this->elevation = null;
        $this->state = null;
        $this->city = null;
        $this->municipality = null;
        $this->country = null;
        $this->continent = null;
        $this->scheduled_service = null;
        $this->gps_code = null;
        $this->region = null;
        $this->faa = null;
        $this->iata = null;
        $this->icao = null;
        $this->role = null;
        $this->enplanements = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collFlightRoutesRelatedByIdDepartureLocation) {
                foreach ($this->collFlightRoutesRelatedByIdDepartureLocation as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFlightRoutesRelatedByIdDestinationLocation) {
                foreach ($this->collFlightRoutesRelatedByIdDestinationLocation as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collFlightRoutesRelatedByIdDepartureLocation instanceof PropelCollection) {
            $this->collFlightRoutesRelatedByIdDepartureLocation->clearIterator();
        }
        $this->collFlightRoutesRelatedByIdDepartureLocation = null;
        if ($this->collFlightRoutesRelatedByIdDestinationLocation instanceof PropelCollection) {
            $this->collFlightRoutesRelatedByIdDestinationLocation->clearIterator();
        }
        $this->collFlightRoutesRelatedByIdDestinationLocation = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AirportPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }
    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     Airport The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[] = AirportPeer::UPDATED_AT;

        return $this;
    }
}
