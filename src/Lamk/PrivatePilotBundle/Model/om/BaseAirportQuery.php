<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airport;
use Lamk\PrivatePilotBundle\Model\AirportPeer;
use Lamk\PrivatePilotBundle\Model\AirportQuery;
use Lamk\PrivatePilotBundle\Model\FlightRoute;

/**
 * @method AirportQuery orderById($order = Criteria::ASC) Order by the id column
 * @method AirportQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method AirportQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method AirportQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method AirportQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method AirportQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 * @method AirportQuery orderByElevation($order = Criteria::ASC) Order by the elevation column
 * @method AirportQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method AirportQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method AirportQuery orderByMunicipality($order = Criteria::ASC) Order by the municipality column
 * @method AirportQuery orderByCountry($order = Criteria::ASC) Order by the country column
 * @method AirportQuery orderByContinent($order = Criteria::ASC) Order by the continent column
 * @method AirportQuery orderByScheduledService($order = Criteria::ASC) Order by the scheduled_service column
 * @method AirportQuery orderByGpsCode($order = Criteria::ASC) Order by the gps_code column
 * @method AirportQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method AirportQuery orderByFaa($order = Criteria::ASC) Order by the faa column
 * @method AirportQuery orderByIata($order = Criteria::ASC) Order by the iata column
 * @method AirportQuery orderByIcao($order = Criteria::ASC) Order by the icao column
 * @method AirportQuery orderByRole($order = Criteria::ASC) Order by the role column
 * @method AirportQuery orderByEnplanements($order = Criteria::ASC) Order by the enplanements column
 * @method AirportQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method AirportQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method AirportQuery groupById() Group by the id column
 * @method AirportQuery groupByName() Group by the name column
 * @method AirportQuery groupByDescription() Group by the description column
 * @method AirportQuery groupByType() Group by the type column
 * @method AirportQuery groupByLatitude() Group by the latitude column
 * @method AirportQuery groupByLongitude() Group by the longitude column
 * @method AirportQuery groupByElevation() Group by the elevation column
 * @method AirportQuery groupByState() Group by the state column
 * @method AirportQuery groupByCity() Group by the city column
 * @method AirportQuery groupByMunicipality() Group by the municipality column
 * @method AirportQuery groupByCountry() Group by the country column
 * @method AirportQuery groupByContinent() Group by the continent column
 * @method AirportQuery groupByScheduledService() Group by the scheduled_service column
 * @method AirportQuery groupByGpsCode() Group by the gps_code column
 * @method AirportQuery groupByRegion() Group by the region column
 * @method AirportQuery groupByFaa() Group by the faa column
 * @method AirportQuery groupByIata() Group by the iata column
 * @method AirportQuery groupByIcao() Group by the icao column
 * @method AirportQuery groupByRole() Group by the role column
 * @method AirportQuery groupByEnplanements() Group by the enplanements column
 * @method AirportQuery groupByCreatedAt() Group by the created_at column
 * @method AirportQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method AirportQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method AirportQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method AirportQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method AirportQuery leftJoinFlightRouteRelatedByIdDepartureLocation($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlightRouteRelatedByIdDepartureLocation relation
 * @method AirportQuery rightJoinFlightRouteRelatedByIdDepartureLocation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlightRouteRelatedByIdDepartureLocation relation
 * @method AirportQuery innerJoinFlightRouteRelatedByIdDepartureLocation($relationAlias = null) Adds a INNER JOIN clause to the query using the FlightRouteRelatedByIdDepartureLocation relation
 *
 * @method AirportQuery leftJoinFlightRouteRelatedByIdDestinationLocation($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlightRouteRelatedByIdDestinationLocation relation
 * @method AirportQuery rightJoinFlightRouteRelatedByIdDestinationLocation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlightRouteRelatedByIdDestinationLocation relation
 * @method AirportQuery innerJoinFlightRouteRelatedByIdDestinationLocation($relationAlias = null) Adds a INNER JOIN clause to the query using the FlightRouteRelatedByIdDestinationLocation relation
 *
 * @method Airport findOne(PropelPDO $con = null) Return the first Airport matching the query
 * @method Airport findOneOrCreate(PropelPDO $con = null) Return the first Airport matching the query, or a new Airport object populated from the query conditions when no match is found
 *
 * @method Airport findOneByName(string $name) Return the first Airport filtered by the name column
 * @method Airport findOneByDescription(string $description) Return the first Airport filtered by the description column
 * @method Airport findOneByType(string $type) Return the first Airport filtered by the type column
 * @method Airport findOneByLatitude(double $latitude) Return the first Airport filtered by the latitude column
 * @method Airport findOneByLongitude(double $longitude) Return the first Airport filtered by the longitude column
 * @method Airport findOneByElevation(int $elevation) Return the first Airport filtered by the elevation column
 * @method Airport findOneByState(string $state) Return the first Airport filtered by the state column
 * @method Airport findOneByCity(string $city) Return the first Airport filtered by the city column
 * @method Airport findOneByMunicipality(string $municipality) Return the first Airport filtered by the municipality column
 * @method Airport findOneByCountry(string $country) Return the first Airport filtered by the country column
 * @method Airport findOneByContinent(string $continent) Return the first Airport filtered by the continent column
 * @method Airport findOneByScheduledService(string $scheduled_service) Return the first Airport filtered by the scheduled_service column
 * @method Airport findOneByGpsCode(string $gps_code) Return the first Airport filtered by the gps_code column
 * @method Airport findOneByRegion(string $region) Return the first Airport filtered by the region column
 * @method Airport findOneByFaa(string $faa) Return the first Airport filtered by the faa column
 * @method Airport findOneByIata(string $iata) Return the first Airport filtered by the iata column
 * @method Airport findOneByIcao(string $icao) Return the first Airport filtered by the icao column
 * @method Airport findOneByRole(string $role) Return the first Airport filtered by the role column
 * @method Airport findOneByEnplanements(int $enplanements) Return the first Airport filtered by the enplanements column
 * @method Airport findOneByCreatedAt(string $created_at) Return the first Airport filtered by the created_at column
 * @method Airport findOneByUpdatedAt(string $updated_at) Return the first Airport filtered by the updated_at column
 *
 * @method array findById(int $id) Return Airport objects filtered by the id column
 * @method array findByName(string $name) Return Airport objects filtered by the name column
 * @method array findByDescription(string $description) Return Airport objects filtered by the description column
 * @method array findByType(string $type) Return Airport objects filtered by the type column
 * @method array findByLatitude(double $latitude) Return Airport objects filtered by the latitude column
 * @method array findByLongitude(double $longitude) Return Airport objects filtered by the longitude column
 * @method array findByElevation(int $elevation) Return Airport objects filtered by the elevation column
 * @method array findByState(string $state) Return Airport objects filtered by the state column
 * @method array findByCity(string $city) Return Airport objects filtered by the city column
 * @method array findByMunicipality(string $municipality) Return Airport objects filtered by the municipality column
 * @method array findByCountry(string $country) Return Airport objects filtered by the country column
 * @method array findByContinent(string $continent) Return Airport objects filtered by the continent column
 * @method array findByScheduledService(string $scheduled_service) Return Airport objects filtered by the scheduled_service column
 * @method array findByGpsCode(string $gps_code) Return Airport objects filtered by the gps_code column
 * @method array findByRegion(string $region) Return Airport objects filtered by the region column
 * @method array findByFaa(string $faa) Return Airport objects filtered by the faa column
 * @method array findByIata(string $iata) Return Airport objects filtered by the iata column
 * @method array findByIcao(string $icao) Return Airport objects filtered by the icao column
 * @method array findByRole(string $role) Return Airport objects filtered by the role column
 * @method array findByEnplanements(int $enplanements) Return Airport objects filtered by the enplanements column
 * @method array findByCreatedAt(string $created_at) Return Airport objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return Airport objects filtered by the updated_at column
 */
abstract class BaseAirportQuery extends ModelCriteria
{

    /**
     * Initializes internal state of BaseAirportQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'private_pilot';
        }
        if (null === $modelName) {
            $modelName = 'Lamk\\PrivatePilotBundle\\Model\\Airport';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new AirportQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   AirportQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return AirportQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof AirportQuery) {
            return $criteria;
        }
        $query = new AirportQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Airport|Airport[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AirportPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(AirportPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select || $this->selectColumns || $this->asColumns || $this->selectModifiers || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Airport A model object, or null if the key is not found
     * @throws PropelException
     */
    public function findOneById($key, $con = null)
    {
        return $this->findPk($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Airport A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `name`, `description`, `type`, `latitude`, `longitude`, `elevation`, `state`, `city`, `municipality`, `country`, `continent`, `scheduled_service`, `gps_code`, `region`, `faa`, `iata`, `icao`, `role`, `enplanements`, `created_at`, `updated_at` FROM `airport` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Airport();
            $obj->hydrate($row);
            AirportPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Airport|Airport[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Airport[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AirportPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AirportPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AirportPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AirportPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude(1234); // WHERE latitude = 1234
     * $query->filterByLatitude(array(12, 34)); // WHERE latitude IN (12, 34)
     * $query->filterByLatitude(array('min' => 12)); // WHERE latitude >= 12
     * $query->filterByLatitude(array('max' => 12)); // WHERE latitude <= 12
     * </code>
     *
     * @param     mixed $latitude The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (is_array($latitude)) {
            $useMinMax = false;
            if (isset($latitude['min'])) {
                $this->addUsingAlias(AirportPeer::LATITUDE, $latitude['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($latitude['max'])) {
                $this->addUsingAlias(AirportPeer::LATITUDE, $latitude['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude(1234); // WHERE longitude = 1234
     * $query->filterByLongitude(array(12, 34)); // WHERE longitude IN (12, 34)
     * $query->filterByLongitude(array('min' => 12)); // WHERE longitude >= 12
     * $query->filterByLongitude(array('max' => 12)); // WHERE longitude <= 12
     * </code>
     *
     * @param     mixed $longitude The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (is_array($longitude)) {
            $useMinMax = false;
            if (isset($longitude['min'])) {
                $this->addUsingAlias(AirportPeer::LONGITUDE, $longitude['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($longitude['max'])) {
                $this->addUsingAlias(AirportPeer::LONGITUDE, $longitude['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the elevation column
     *
     * Example usage:
     * <code>
     * $query->filterByElevation(1234); // WHERE elevation = 1234
     * $query->filterByElevation(array(12, 34)); // WHERE elevation IN (12, 34)
     * $query->filterByElevation(array('min' => 12)); // WHERE elevation >= 12
     * $query->filterByElevation(array('max' => 12)); // WHERE elevation <= 12
     * </code>
     *
     * @param     mixed $elevation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByElevation($elevation = null, $comparison = null)
    {
        if (is_array($elevation)) {
            $useMinMax = false;
            if (isset($elevation['min'])) {
                $this->addUsingAlias(AirportPeer::ELEVATION, $elevation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($elevation['max'])) {
                $this->addUsingAlias(AirportPeer::ELEVATION, $elevation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::ELEVATION, $elevation, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $city)) {
                $city = str_replace('*', '%', $city);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::CITY, $city, $comparison);
    }

    /**
     * Filter the query on the municipality column
     *
     * Example usage:
     * <code>
     * $query->filterByMunicipality('fooValue');   // WHERE municipality = 'fooValue'
     * $query->filterByMunicipality('%fooValue%'); // WHERE municipality LIKE '%fooValue%'
     * </code>
     *
     * @param     string $municipality The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByMunicipality($municipality = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($municipality)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $municipality)) {
                $municipality = str_replace('*', '%', $municipality);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::MUNICIPALITY, $municipality, $comparison);
    }

    /**
     * Filter the query on the country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE country = 'fooValue'
     * $query->filterByCountry('%fooValue%'); // WHERE country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $country)) {
                $country = str_replace('*', '%', $country);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the continent column
     *
     * Example usage:
     * <code>
     * $query->filterByContinent('fooValue');   // WHERE continent = 'fooValue'
     * $query->filterByContinent('%fooValue%'); // WHERE continent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $continent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByContinent($continent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($continent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $continent)) {
                $continent = str_replace('*', '%', $continent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::CONTINENT, $continent, $comparison);
    }

    /**
     * Filter the query on the scheduled_service column
     *
     * Example usage:
     * <code>
     * $query->filterByScheduledService('fooValue');   // WHERE scheduled_service = 'fooValue'
     * $query->filterByScheduledService('%fooValue%'); // WHERE scheduled_service LIKE '%fooValue%'
     * </code>
     *
     * @param     string $scheduledService The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByScheduledService($scheduledService = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($scheduledService)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $scheduledService)) {
                $scheduledService = str_replace('*', '%', $scheduledService);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::SCHEDULED_SERVICE, $scheduledService, $comparison);
    }

    /**
     * Filter the query on the gps_code column
     *
     * Example usage:
     * <code>
     * $query->filterByGpsCode('fooValue');   // WHERE gps_code = 'fooValue'
     * $query->filterByGpsCode('%fooValue%'); // WHERE gps_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gpsCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByGpsCode($gpsCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gpsCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gpsCode)) {
                $gpsCode = str_replace('*', '%', $gpsCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::GPS_CODE, $gpsCode, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE region = 'fooValue'
     * $query->filterByRegion('%fooValue%'); // WHERE region LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $region)) {
                $region = str_replace('*', '%', $region);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::REGION, $region, $comparison);
    }

    /**
     * Filter the query on the faa column
     *
     * Example usage:
     * <code>
     * $query->filterByFaa('fooValue');   // WHERE faa = 'fooValue'
     * $query->filterByFaa('%fooValue%'); // WHERE faa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $faa The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByFaa($faa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($faa)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $faa)) {
                $faa = str_replace('*', '%', $faa);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::FAA, $faa, $comparison);
    }

    /**
     * Filter the query on the iata column
     *
     * Example usage:
     * <code>
     * $query->filterByIata('fooValue');   // WHERE iata = 'fooValue'
     * $query->filterByIata('%fooValue%'); // WHERE iata LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iata The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByIata($iata = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iata)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $iata)) {
                $iata = str_replace('*', '%', $iata);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::IATA, $iata, $comparison);
    }

    /**
     * Filter the query on the icao column
     *
     * Example usage:
     * <code>
     * $query->filterByIcao('fooValue');   // WHERE icao = 'fooValue'
     * $query->filterByIcao('%fooValue%'); // WHERE icao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icao The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByIcao($icao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icao)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $icao)) {
                $icao = str_replace('*', '%', $icao);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::ICAO, $icao, $comparison);
    }

    /**
     * Filter the query on the role column
     *
     * Example usage:
     * <code>
     * $query->filterByRole('fooValue');   // WHERE role = 'fooValue'
     * $query->filterByRole('%fooValue%'); // WHERE role LIKE '%fooValue%'
     * </code>
     *
     * @param     string $role The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByRole($role = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($role)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $role)) {
                $role = str_replace('*', '%', $role);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AirportPeer::ROLE, $role, $comparison);
    }

    /**
     * Filter the query on the enplanements column
     *
     * Example usage:
     * <code>
     * $query->filterByEnplanements(1234); // WHERE enplanements = 1234
     * $query->filterByEnplanements(array(12, 34)); // WHERE enplanements IN (12, 34)
     * $query->filterByEnplanements(array('min' => 12)); // WHERE enplanements >= 12
     * $query->filterByEnplanements(array('max' => 12)); // WHERE enplanements <= 12
     * </code>
     *
     * @param     mixed $enplanements The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByEnplanements($enplanements = null, $comparison = null)
    {
        if (is_array($enplanements)) {
            $useMinMax = false;
            if (isset($enplanements['min'])) {
                $this->addUsingAlias(AirportPeer::ENPLANEMENTS, $enplanements['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enplanements['max'])) {
                $this->addUsingAlias(AirportPeer::ENPLANEMENTS, $enplanements['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::ENPLANEMENTS, $enplanements, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AirportPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AirportPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AirportPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AirportPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AirportPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related FlightRoute object
     *
     * @param   FlightRoute|PropelObjectCollection $flightRoute  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AirportQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFlightRouteRelatedByIdDepartureLocation($flightRoute, $comparison = null)
    {
        if ($flightRoute instanceof FlightRoute) {
            return $this
                    ->addUsingAlias(AirportPeer::ID, $flightRoute->getIdDepartureLocation(), $comparison);
        } elseif ($flightRoute instanceof PropelObjectCollection) {
            return $this
                    ->useFlightRouteRelatedByIdDepartureLocationQuery()
                    ->filterByPrimaryKeys($flightRoute->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByFlightRouteRelatedByIdDepartureLocation() only accepts arguments of type FlightRoute or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlightRouteRelatedByIdDepartureLocation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function joinFlightRouteRelatedByIdDepartureLocation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlightRouteRelatedByIdDepartureLocation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlightRouteRelatedByIdDepartureLocation');
        }

        return $this;
    }

    /**
     * Use the FlightRouteRelatedByIdDepartureLocation relation FlightRoute object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\FlightRouteQuery A secondary query class using the current class as primary query
     */
    public function useFlightRouteRelatedByIdDepartureLocationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinFlightRouteRelatedByIdDepartureLocation($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'FlightRouteRelatedByIdDepartureLocation', '\Lamk\PrivatePilotBundle\Model\FlightRouteQuery');
    }

    /**
     * Filter the query by a related FlightRoute object
     *
     * @param   FlightRoute|PropelObjectCollection $flightRoute  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AirportQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFlightRouteRelatedByIdDestinationLocation($flightRoute, $comparison = null)
    {
        if ($flightRoute instanceof FlightRoute) {
            return $this
                    ->addUsingAlias(AirportPeer::ID, $flightRoute->getIdDestinationLocation(), $comparison);
        } elseif ($flightRoute instanceof PropelObjectCollection) {
            return $this
                    ->useFlightRouteRelatedByIdDestinationLocationQuery()
                    ->filterByPrimaryKeys($flightRoute->getPrimaryKeys())
                    ->endUse();
        } else {
            throw new PropelException('filterByFlightRouteRelatedByIdDestinationLocation() only accepts arguments of type FlightRoute or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlightRouteRelatedByIdDestinationLocation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function joinFlightRouteRelatedByIdDestinationLocation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlightRouteRelatedByIdDestinationLocation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlightRouteRelatedByIdDestinationLocation');
        }

        return $this;
    }

    /**
     * Use the FlightRouteRelatedByIdDestinationLocation relation FlightRoute object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Lamk\PrivatePilotBundle\Model\FlightRouteQuery A secondary query class using the current class as primary query
     */
    public function useFlightRouteRelatedByIdDestinationLocationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
                ->joinFlightRouteRelatedByIdDestinationLocation($relationAlias, $joinType)
                ->useQuery($relationAlias ? $relationAlias : 'FlightRouteRelatedByIdDestinationLocation', '\Lamk\PrivatePilotBundle\Model\FlightRouteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Airport $airport Object to remove from the list of results
     *
     * @return AirportQuery The current query, for fluid interface
     */
    public function prune($airport = null)
    {
        if ($airport) {
            $this->addUsingAlias(AirportPeer::ID, $airport->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }
    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     AirportQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(AirportPeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     AirportQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(AirportPeer::UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     AirportQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(AirportPeer::UPDATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     AirportQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(AirportPeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date desc
     *
     * @return     AirportQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(AirportPeer::CREATED_AT);
    }

    /**
     * Order by create date asc
     *
     * @return     AirportQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(AirportPeer::CREATED_AT);
    }
}
