<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoPeer;
use Lamk\PrivatePilotBundle\Model\map\UserPersonalInfoTableMap;

abstract class BaseUserPersonalInfoPeer
{
    /** the default database name for this class */
    const DATABASE_NAME = 'private_pilot';
    /** the table name for this class */
    const TABLE_NAME = 'user_personal_info';
    /** the related Propel class for this table */
    const OM_CLASS = 'Lamk\\PrivatePilotBundle\\Model\\UserPersonalInfo';
    /** the related TableMap class for this table */
    const TM_CLASS = 'Lamk\\PrivatePilotBundle\\Model\\map\\UserPersonalInfoTableMap';
    /** The total number of columns. */
    const NUM_COLUMNS = 11;
    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;
    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 11;
    /** the column name for the id field */
    const ID = 'user_personal_info.id';
    /** the column name for the first_name field */
    const FIRST_NAME = 'user_personal_info.first_name';
    /** the column name for the last_name field */
    const LAST_NAME = 'user_personal_info.last_name';
    /** the column name for the line1 field */
    const LINE1 = 'user_personal_info.line1';
    /** the column name for the line2 field */
    const LINE2 = 'user_personal_info.line2';
    /** the column name for the city field */
    const CITY = 'user_personal_info.city';
    /** the column name for the state field */
    const STATE = 'user_personal_info.state';
    /** the column name for the postal_code field */
    const POSTAL_CODE = 'user_personal_info.postal_code';
    /** the column name for the country_code field */
    const COUNTRY_CODE = 'user_personal_info.country_code';
    /** the column name for the created_at field */
    const CREATED_AT = 'user_personal_info.created_at';
    /** the column name for the updated_at field */
    const UPDATED_AT = 'user_personal_info.updated_at';
    /** The default string format for model objects of the related table * */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of UserPersonalInfo objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array UserPersonalInfo[]
     */
    public static $instances = array();
    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. UserPersonalInfoPeer::$fieldNames[UserPersonalInfoPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array(
      BasePeer::TYPE_PHPNAME       => array('Id', 'FirstName', 'LastName', 'Line1', 'Line2', 'City', 'State', 'PostalCode', 'CountryCode', 'CreatedAt', 'UpdatedAt',),
      BasePeer::TYPE_STUDLYPHPNAME => array('id', 'firstName', 'lastName', 'line1', 'line2', 'city', 'state', 'postalCode', 'countryCode', 'createdAt', 'updatedAt',),
      BasePeer::TYPE_COLNAME       => array(UserPersonalInfoPeer::ID, UserPersonalInfoPeer::FIRST_NAME, UserPersonalInfoPeer::LAST_NAME, UserPersonalInfoPeer::LINE1, UserPersonalInfoPeer::LINE2, UserPersonalInfoPeer::CITY, UserPersonalInfoPeer::STATE, UserPersonalInfoPeer::POSTAL_CODE, UserPersonalInfoPeer::COUNTRY_CODE, UserPersonalInfoPeer::CREATED_AT, UserPersonalInfoPeer::UPDATED_AT,),
      BasePeer::TYPE_RAW_COLNAME   => array('ID', 'FIRST_NAME', 'LAST_NAME', 'LINE1', 'LINE2', 'CITY', 'STATE', 'POSTAL_CODE', 'COUNTRY_CODE', 'CREATED_AT', 'UPDATED_AT',),
      BasePeer::TYPE_FIELDNAME     => array('id', 'first_name', 'last_name', 'line1', 'line2', 'city', 'state', 'postal_code', 'country_code', 'created_at', 'updated_at',),
      BasePeer::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,)
    );
    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. UserPersonalInfoPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array(
      BasePeer::TYPE_PHPNAME       => array('Id' => 0, 'FirstName' => 1, 'LastName' => 2, 'Line1' => 3, 'Line2' => 4, 'City' => 5, 'State' => 6, 'PostalCode' => 7, 'CountryCode' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10,),
      BasePeer::TYPE_STUDLYPHPNAME => array('id' => 0, 'firstName' => 1, 'lastName' => 2, 'line1' => 3, 'line2' => 4, 'city' => 5, 'state' => 6, 'postalCode' => 7, 'countryCode' => 8, 'createdAt' => 9, 'updatedAt' => 10,),
      BasePeer::TYPE_COLNAME       => array(UserPersonalInfoPeer::ID => 0, UserPersonalInfoPeer::FIRST_NAME => 1, UserPersonalInfoPeer::LAST_NAME => 2, UserPersonalInfoPeer::LINE1 => 3, UserPersonalInfoPeer::LINE2 => 4, UserPersonalInfoPeer::CITY => 5, UserPersonalInfoPeer::STATE => 6, UserPersonalInfoPeer::POSTAL_CODE => 7, UserPersonalInfoPeer::COUNTRY_CODE => 8, UserPersonalInfoPeer::CREATED_AT => 9, UserPersonalInfoPeer::UPDATED_AT => 10,),
      BasePeer::TYPE_RAW_COLNAME   => array('ID' => 0, 'FIRST_NAME' => 1, 'LAST_NAME' => 2, 'LINE1' => 3, 'LINE2' => 4, 'CITY' => 5, 'STATE' => 6, 'POSTAL_CODE' => 7, 'COUNTRY_CODE' => 8, 'CREATED_AT' => 9, 'UPDATED_AT' => 10,),
      BasePeer::TYPE_FIELDNAME     => array('id' => 0, 'first_name' => 1, 'last_name' => 2, 'line1' => 3, 'line2' => 4, 'city' => 5, 'state' => 6, 'postal_code' => 7, 'country_code' => 8, 'created_at' => 9, 'updated_at' => 10,),
      BasePeer::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,)
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = UserPersonalInfoPeer::getFieldNames($toType);
        $key = isset(UserPersonalInfoPeer::$fieldKeys[$fromType][$name]) ? UserPersonalInfoPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(UserPersonalInfoPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, UserPersonalInfoPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return UserPersonalInfoPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     * 		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     * 		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. UserPersonalInfoPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(UserPersonalInfoPeer::TABLE_NAME . '.', $alias . '.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserPersonalInfoPeer::ID);
            $criteria->addSelectColumn(UserPersonalInfoPeer::FIRST_NAME);
            $criteria->addSelectColumn(UserPersonalInfoPeer::LAST_NAME);
            $criteria->addSelectColumn(UserPersonalInfoPeer::LINE1);
            $criteria->addSelectColumn(UserPersonalInfoPeer::LINE2);
            $criteria->addSelectColumn(UserPersonalInfoPeer::CITY);
            $criteria->addSelectColumn(UserPersonalInfoPeer::STATE);
            $criteria->addSelectColumn(UserPersonalInfoPeer::POSTAL_CODE);
            $criteria->addSelectColumn(UserPersonalInfoPeer::COUNTRY_CODE);
            $criteria->addSelectColumn(UserPersonalInfoPeer::CREATED_AT);
            $criteria->addSelectColumn(UserPersonalInfoPeer::UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.line1');
            $criteria->addSelectColumn($alias . '.line2');
            $criteria->addSelectColumn($alias . '.city');
            $criteria->addSelectColumn($alias . '.state');
            $criteria->addSelectColumn($alias . '.postal_code');
            $criteria->addSelectColumn($alias . '.country_code');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(UserPersonalInfoPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            UserPersonalInfoPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(UserPersonalInfoPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return UserPersonalInfo
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = UserPersonalInfoPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }

    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return UserPersonalInfoPeer::populateObjects(UserPersonalInfoPeer::doSelectStmt($criteria, $con));
    }

    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            UserPersonalInfoPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(UserPersonalInfoPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param UserPersonalInfo $obj A UserPersonalInfo object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            UserPersonalInfoPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A UserPersonalInfo object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof UserPersonalInfo) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or UserPersonalInfo object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(UserPersonalInfoPeer::$instances[$key]);
        }
    }
// removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return UserPersonalInfo Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(UserPersonalInfoPeer::$instances[$key])) {
                return UserPersonalInfoPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
        if ($and_clear_all_references) {
            foreach (UserPersonalInfoPeer::$instances as $instance) {
                $instance->clearAllReferences(true);
            }
        }
        UserPersonalInfoPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to user_personal_info
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = UserPersonalInfoPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = UserPersonalInfoPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = UserPersonalInfoPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserPersonalInfoPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     * @return array (UserPersonalInfo object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = UserPersonalInfoPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = UserPersonalInfoPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + UserPersonalInfoPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserPersonalInfoPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            UserPersonalInfoPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(UserPersonalInfoPeer::DATABASE_NAME)->getTable(UserPersonalInfoPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getDatabaseMap(BaseUserPersonalInfoPeer::DATABASE_NAME);
        if (!$dbMap->hasTable(BaseUserPersonalInfoPeer::TABLE_NAME)) {
            $dbMap->addTableObject(new \Lamk\PrivatePilotBundle\Model\map\UserPersonalInfoTableMap());
        }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return UserPersonalInfoPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a UserPersonalInfo or Criteria object.
     *
     * @param      mixed $values Criteria or UserPersonalInfo object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from UserPersonalInfo object
        }

        if ($criteria->containsKey(UserPersonalInfoPeer::ID) && $criteria->keyContainsValue(UserPersonalInfoPeer::ID)) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserPersonalInfoPeer::ID . ')');
        }


        // Set the correct dbName
        $criteria->setDbName(UserPersonalInfoPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a UserPersonalInfo or Criteria object.
     *
     * @param      mixed $values Criteria or UserPersonalInfo object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(UserPersonalInfoPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(UserPersonalInfoPeer::ID);
            $value = $criteria->remove(UserPersonalInfoPeer::ID);
            if ($value) {
                $selectCriteria->add(UserPersonalInfoPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(UserPersonalInfoPeer::TABLE_NAME);
            }
        } else { // $values is UserPersonalInfo object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(UserPersonalInfoPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the user_personal_info table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(UserPersonalInfoPeer::TABLE_NAME, $con, UserPersonalInfoPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserPersonalInfoPeer::clearInstancePool();
            UserPersonalInfoPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a UserPersonalInfo or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or UserPersonalInfo object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     * 				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function doDelete($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            UserPersonalInfoPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof UserPersonalInfo) { // it's a model object
            // invalidate the cache for this single object
            UserPersonalInfoPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserPersonalInfoPeer::DATABASE_NAME);
            $criteria->add(UserPersonalInfoPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                UserPersonalInfoPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(UserPersonalInfoPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            UserPersonalInfoPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given UserPersonalInfo object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param UserPersonalInfo $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(UserPersonalInfoPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(UserPersonalInfoPeer::TABLE_NAME);

            if (!is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {
            
        }

        return BasePeer::doValidate(UserPersonalInfoPeer::DATABASE_NAME, UserPersonalInfoPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return UserPersonalInfo
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = UserPersonalInfoPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(UserPersonalInfoPeer::DATABASE_NAME);
        $criteria->add(UserPersonalInfoPeer::ID, $pk);

        $v = UserPersonalInfoPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return UserPersonalInfo[]
     * @throws PropelException Any exceptions caught during processing will be
     * 		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(UserPersonalInfoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(UserPersonalInfoPeer::DATABASE_NAME);
            $criteria->add(UserPersonalInfoPeer::ID, $pks, Criteria::IN);
            $objs = UserPersonalInfoPeer::doSelect($criteria, $con);
        }

        return $objs;
    }
}

// BaseUserPersonalInfoPeer
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseUserPersonalInfoPeer::buildTableMap();

