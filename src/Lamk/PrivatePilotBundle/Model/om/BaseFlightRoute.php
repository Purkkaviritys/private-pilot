<?php

namespace Lamk\PrivatePilotBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\AirplaneQuery;
use Lamk\PrivatePilotBundle\Model\Airport;
use Lamk\PrivatePilotBundle\Model\AirportQuery;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\FlightRoutePeer;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\ReceivedOrder;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserQuery;

abstract class BaseFlightRoute extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Lamk\\PrivatePilotBundle\\Model\\FlightRoutePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        FlightRoutePeer
     */
    protected static $peer;
    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;
    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;
    /**
     * The value for the active field.
     * @var        boolean
     */
    protected $active;
    /**
     * The value for the number_of_seats field.
     * @var        int
     */
    protected $number_of_seats;
    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;
    /**
     * The value for the id_pilot field.
     * @var        int
     */
    protected $id_pilot;
    /**
     * The value for the id_plane field.
     * @var        int
     */
    protected $id_plane;
    /**
     * The value for the expires_at field.
     * @var        string
     */
    protected $expires_at;
    /**
     * The value for the price field.
     * @var        string
     */
    protected $price;
    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;
    /**
     * The value for the departure_location field.
     * @var        string
     */
    protected $departure_location;
    /**
     * The value for the destination_location field.
     * @var        string
     */
    protected $destination_location;
    /**
     * The value for the id_departure_location field.
     * @var        int
     */
    protected $id_departure_location;
    /**
     * The value for the id_destination_location field.
     * @var        int
     */
    protected $id_destination_location;
    /**
     * The value for the estimated_flight_time field.
     * @var        string
     */
    protected $estimated_flight_time;
    /**
     * The value for the rating field.
     * @var        double
     */
    protected $rating;
    /**
     * The value for the departure_date field.
     * @var        string
     */
    protected $departure_date;
    /**
     * The value for the destination_date field.
     * @var        string
     */
    protected $destination_date;
    /**
     * The value for the departure_time field.
     * @var        string
     */
    protected $departure_time;
    /**
     * The value for the destination_time field.
     * @var        string
     */
    protected $destination_time;
    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;
    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;
    /**
     * @var        User
     */
    protected $aUser;
    /**
     * @var        Airplane
     */
    protected $aAirplane;
    /**
     * @var        Airport
     */
    protected $aAirportRelatedByIdDepartureLocation;
    /**
     * @var        Airport
     */
    protected $aAirportRelatedByIdDestinationLocation;
    /**
     * @var        PropelObjectCollection|ReceivedOrder[] Collection to store aggregation of ReceivedOrder objects.
     */
    protected $collReceivedOrders;
    protected $collReceivedOrdersPartial;
    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;
    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;
    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;
    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $receivedOrdersScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [active] column value.
     *
     * @return boolean
     */
    public function getActive()
    {

        return $this->active;
    }

    /**
     * Get the [number_of_seats] column value.
     *
     * @return int
     */
    public function getNumberOfSeats()
    {

        return $this->number_of_seats;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [id_pilot] column value.
     *
     * @return int
     */
    public function getIdPilot()
    {

        return $this->id_pilot;
    }

    /**
     * Get the [id_plane] column value.
     *
     * @return int
     */
    public function getIdPlane()
    {

        return $this->id_plane;
    }

    /**
     * Get the [optionally formatted] temporal [expires_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiresAt($format = null)
    {
        if ($this->expires_at === null) {
            return null;
        }

        if ($this->expires_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->expires_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expires_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [price] column value.
     *
     * @return string
     */
    public function getPrice()
    {

        return $this->price;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [departure_location] column value.
     *
     * @return string
     */
    public function getDepartureLocation()
    {

        return $this->departure_location;
    }

    /**
     * Get the [destination_location] column value.
     *
     * @return string
     */
    public function getDestinationLocation()
    {

        return $this->destination_location;
    }

    /**
     * Get the [id_departure_location] column value.
     *
     * @return int
     */
    public function getIdDepartureLocation()
    {

        return $this->id_departure_location;
    }

    /**
     * Get the [id_destination_location] column value.
     *
     * @return int
     */
    public function getIdDestinationLocation()
    {

        return $this->id_destination_location;
    }

    /**
     * Get the [optionally formatted] temporal [estimated_flight_time] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEstimatedFlightTime($format = null)
    {
        if ($this->estimated_flight_time === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->estimated_flight_time);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->estimated_flight_time, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [rating] column value.
     *
     * @return double
     */
    public function getRating()
    {

        return $this->rating;
    }

    /**
     * Get the [optionally formatted] temporal [departure_date] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDepartureDate($format = null)
    {
        if ($this->departure_date === null) {
            return null;
        }

        if ($this->departure_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->departure_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->departure_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [destination_date] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDestinationDate($format = null)
    {
        if ($this->destination_date === null) {
            return null;
        }

        if ($this->destination_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->destination_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->destination_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [departure_time] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDepartureTime($format = null)
    {
        if ($this->departure_time === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->departure_time);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->departure_time, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [destination_time] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDestinationTime($format = null)
    {
        if ($this->destination_time === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->destination_time);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->destination_time, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     * 				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($this->updated_at === null) {
            return null;
        }

        if ($this->updated_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = FlightRoutePeer::ID;
        }


        return $this;
    }
// setId()

    /**
     * Sets the value of the [active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[] = FlightRoutePeer::ACTIVE;
        }


        return $this;
    }
// setActive()

    /**
     * Set the value of [number_of_seats] column.
     *
     * @param  int $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setNumberOfSeats($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->number_of_seats !== $v) {
            $this->number_of_seats = $v;
            $this->modifiedColumns[] = FlightRoutePeer::NUMBER_OF_SEATS;
        }


        return $this;
    }
// setNumberOfSeats()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = FlightRoutePeer::NAME;
        }


        return $this;
    }
// setName()

    /**
     * Set the value of [id_pilot] column.
     *
     * @param  int $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setIdPilot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_pilot !== $v) {
            $this->id_pilot = $v;
            $this->modifiedColumns[] = FlightRoutePeer::ID_PILOT;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }


        return $this;
    }
// setIdPilot()

    /**
     * Set the value of [id_plane] column.
     *
     * @param  int $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setIdPlane($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_plane !== $v) {
            $this->id_plane = $v;
            $this->modifiedColumns[] = FlightRoutePeer::ID_PLANE;
        }

        if ($this->aAirplane !== null && $this->aAirplane->getId() !== $v) {
            $this->aAirplane = null;
        }


        return $this;
    }
// setIdPlane()

    /**
     * Sets the value of [expires_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setExpiresAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expires_at !== null || $dt !== null) {
            $currentDateAsString = ($this->expires_at !== null && $tmpDt = new DateTime($this->expires_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expires_at = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::EXPIRES_AT;
            }
        } // if either are not null


        return $this;
    }
// setExpiresAt()

    /**
     * Set the value of [price] column.
     *
     * @param  string $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->price !== $v) {
            $this->price = $v;
            $this->modifiedColumns[] = FlightRoutePeer::PRICE;
        }


        return $this;
    }
// setPrice()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = FlightRoutePeer::DESCRIPTION;
        }


        return $this;
    }
// setDescription()

    /**
     * Set the value of [departure_location] column.
     *
     * @param  string $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDepartureLocation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->departure_location !== $v) {
            $this->departure_location = $v;
            $this->modifiedColumns[] = FlightRoutePeer::DEPARTURE_LOCATION;
        }


        return $this;
    }
// setDepartureLocation()

    /**
     * Set the value of [destination_location] column.
     *
     * @param  string $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDestinationLocation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->destination_location !== $v) {
            $this->destination_location = $v;
            $this->modifiedColumns[] = FlightRoutePeer::DESTINATION_LOCATION;
        }


        return $this;
    }
// setDestinationLocation()

    /**
     * Set the value of [id_departure_location] column.
     *
     * @param  int $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setIdDepartureLocation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_departure_location !== $v) {
            $this->id_departure_location = $v;
            $this->modifiedColumns[] = FlightRoutePeer::ID_DEPARTURE_LOCATION;
        }

        if ($this->aAirportRelatedByIdDepartureLocation !== null && $this->aAirportRelatedByIdDepartureLocation->getId() !== $v) {
            $this->aAirportRelatedByIdDepartureLocation = null;
        }


        return $this;
    }
// setIdDepartureLocation()

    /**
     * Set the value of [id_destination_location] column.
     *
     * @param  int $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setIdDestinationLocation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_destination_location !== $v) {
            $this->id_destination_location = $v;
            $this->modifiedColumns[] = FlightRoutePeer::ID_DESTINATION_LOCATION;
        }

        if ($this->aAirportRelatedByIdDestinationLocation !== null && $this->aAirportRelatedByIdDestinationLocation->getId() !== $v) {
            $this->aAirportRelatedByIdDestinationLocation = null;
        }


        return $this;
    }
// setIdDestinationLocation()

    /**
     * Sets the value of [estimated_flight_time] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setEstimatedFlightTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->estimated_flight_time !== null || $dt !== null) {
            $currentDateAsString = ($this->estimated_flight_time !== null && $tmpDt = new DateTime($this->estimated_flight_time)) ? $tmpDt->format('H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->estimated_flight_time = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::ESTIMATED_FLIGHT_TIME;
            }
        } // if either are not null


        return $this;
    }
// setEstimatedFlightTime()

    /**
     * Set the value of [rating] column.
     *
     * @param  double $v new value
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setRating($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->rating !== $v) {
            $this->rating = $v;
            $this->modifiedColumns[] = FlightRoutePeer::RATING;
        }


        return $this;
    }
// setRating()

    /**
     * Sets the value of [departure_date] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDepartureDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->departure_date !== null || $dt !== null) {
            $currentDateAsString = ($this->departure_date !== null && $tmpDt = new DateTime($this->departure_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->departure_date = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::DEPARTURE_DATE;
            }
        } // if either are not null


        return $this;
    }
// setDepartureDate()

    /**
     * Sets the value of [destination_date] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDestinationDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->destination_date !== null || $dt !== null) {
            $currentDateAsString = ($this->destination_date !== null && $tmpDt = new DateTime($this->destination_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->destination_date = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::DESTINATION_DATE;
            }
        } // if either are not null


        return $this;
    }
// setDestinationDate()

    /**
     * Sets the value of [departure_time] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDepartureTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->departure_time !== null || $dt !== null) {
            $currentDateAsString = ($this->departure_time !== null && $tmpDt = new DateTime($this->departure_time)) ? $tmpDt->format('H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->departure_time = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::DEPARTURE_TIME;
            }
        } // if either are not null


        return $this;
    }
// setDepartureTime()

    /**
     * Sets the value of [destination_time] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setDestinationTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->destination_time !== null || $dt !== null) {
            $currentDateAsString = ($this->destination_time !== null && $tmpDt = new DateTime($this->destination_time)) ? $tmpDt->format('H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->destination_time = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::DESTINATION_TIME;
            }
        } // if either are not null


        return $this;
    }
// setDestinationTime()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = FlightRoutePeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    }
// setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    }
// hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->active = ($row[$startcol + 1] !== null) ? (boolean) $row[$startcol + 1] : null;
            $this->number_of_seats = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->name = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->id_pilot = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_plane = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->expires_at = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->price = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->description = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->departure_location = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->destination_location = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->id_departure_location = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->id_destination_location = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->estimated_flight_time = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->rating = ($row[$startcol + 14] !== null) ? (double) $row[$startcol + 14] : null;
            $this->departure_date = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->destination_date = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->departure_time = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->destination_time = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->created_at = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->updated_at = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 21; // 21 = FlightRoutePeer::NUM_HYDRATE_COLUMNS.
        } catch (Exception $e) {
            throw new PropelException("Error populating FlightRoute object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aUser !== null && $this->id_pilot !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aAirplane !== null && $this->id_plane !== $this->aAirplane->getId()) {
            $this->aAirplane = null;
        }
        if ($this->aAirportRelatedByIdDepartureLocation !== null && $this->id_departure_location !== $this->aAirportRelatedByIdDepartureLocation->getId()) {
            $this->aAirportRelatedByIdDepartureLocation = null;
        }
        if ($this->aAirportRelatedByIdDestinationLocation !== null && $this->id_destination_location !== $this->aAirportRelatedByIdDestinationLocation->getId()) {
            $this->aAirportRelatedByIdDestinationLocation = null;
        }
    }
// ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(FlightRoutePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = FlightRoutePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?
            $this->aUser = null;
            $this->aAirplane = null;
            $this->aAirportRelatedByIdDepartureLocation = null;
            $this->aAirportRelatedByIdDestinationLocation = null;
            $this->collReceivedOrders = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(FlightRoutePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = FlightRouteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(FlightRoutePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                if (!$this->isColumnModified(FlightRoutePeer::CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(FlightRoutePeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(FlightRoutePeer::UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FlightRoutePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aAirplane !== null) {
                if ($this->aAirplane->isModified() || $this->aAirplane->isNew()) {
                    $affectedRows += $this->aAirplane->save($con);
                }
                $this->setAirplane($this->aAirplane);
            }

            if ($this->aAirportRelatedByIdDepartureLocation !== null) {
                if ($this->aAirportRelatedByIdDepartureLocation->isModified() || $this->aAirportRelatedByIdDepartureLocation->isNew()) {
                    $affectedRows += $this->aAirportRelatedByIdDepartureLocation->save($con);
                }
                $this->setAirportRelatedByIdDepartureLocation($this->aAirportRelatedByIdDepartureLocation);
            }

            if ($this->aAirportRelatedByIdDestinationLocation !== null) {
                if ($this->aAirportRelatedByIdDestinationLocation->isModified() || $this->aAirportRelatedByIdDestinationLocation->isNew()) {
                    $affectedRows += $this->aAirportRelatedByIdDestinationLocation->save($con);
                }
                $this->setAirportRelatedByIdDestinationLocation($this->aAirportRelatedByIdDestinationLocation);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->receivedOrdersScheduledForDeletion !== null) {
                if (!$this->receivedOrdersScheduledForDeletion->isEmpty()) {
                    foreach ($this->receivedOrdersScheduledForDeletion as $receivedOrder) {
                        // need to save related object because we set the relation to null
                        $receivedOrder->save($con);
                    }
                    $this->receivedOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collReceivedOrders !== null) {
                foreach ($this->collReceivedOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;
        }

        return $affectedRows;
    }
// doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = FlightRoutePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FlightRoutePeer::ID . ')');
        }

        // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FlightRoutePeer::ID)) {
            $modifiedColumns[':p' . $index++] = '`id`';
        }
        if ($this->isColumnModified(FlightRoutePeer::ACTIVE)) {
            $modifiedColumns[':p' . $index++] = '`active`';
        }
        if ($this->isColumnModified(FlightRoutePeer::NUMBER_OF_SEATS)) {
            $modifiedColumns[':p' . $index++] = '`number_of_seats`';
        }
        if ($this->isColumnModified(FlightRoutePeer::NAME)) {
            $modifiedColumns[':p' . $index++] = '`name`';
        }
        if ($this->isColumnModified(FlightRoutePeer::ID_PILOT)) {
            $modifiedColumns[':p' . $index++] = '`id_pilot`';
        }
        if ($this->isColumnModified(FlightRoutePeer::ID_PLANE)) {
            $modifiedColumns[':p' . $index++] = '`id_plane`';
        }
        if ($this->isColumnModified(FlightRoutePeer::EXPIRES_AT)) {
            $modifiedColumns[':p' . $index++] = '`expires_at`';
        }
        if ($this->isColumnModified(FlightRoutePeer::PRICE)) {
            $modifiedColumns[':p' . $index++] = '`price`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++] = '`description`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DEPARTURE_LOCATION)) {
            $modifiedColumns[':p' . $index++] = '`departure_location`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DESTINATION_LOCATION)) {
            $modifiedColumns[':p' . $index++] = '`destination_location`';
        }
        if ($this->isColumnModified(FlightRoutePeer::ID_DEPARTURE_LOCATION)) {
            $modifiedColumns[':p' . $index++] = '`id_departure_location`';
        }
        if ($this->isColumnModified(FlightRoutePeer::ID_DESTINATION_LOCATION)) {
            $modifiedColumns[':p' . $index++] = '`id_destination_location`';
        }
        if ($this->isColumnModified(FlightRoutePeer::ESTIMATED_FLIGHT_TIME)) {
            $modifiedColumns[':p' . $index++] = '`estimated_flight_time`';
        }
        if ($this->isColumnModified(FlightRoutePeer::RATING)) {
            $modifiedColumns[':p' . $index++] = '`rating`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DEPARTURE_DATE)) {
            $modifiedColumns[':p' . $index++] = '`departure_date`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DESTINATION_DATE)) {
            $modifiedColumns[':p' . $index++] = '`destination_date`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DEPARTURE_TIME)) {
            $modifiedColumns[':p' . $index++] = '`departure_time`';
        }
        if ($this->isColumnModified(FlightRoutePeer::DESTINATION_TIME)) {
            $modifiedColumns[':p' . $index++] = '`destination_time`';
        }
        if ($this->isColumnModified(FlightRoutePeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`created_at`';
        }
        if ($this->isColumnModified(FlightRoutePeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++] = '`updated_at`';
        }

        $sql = sprintf(
            'INSERT INTO `flight_route` (%s) VALUES (%s)', implode(', ', $modifiedColumns), implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`active`':
                        $stmt->bindValue($identifier, (int) $this->active, PDO::PARAM_INT);
                        break;
                    case '`number_of_seats`':
                        $stmt->bindValue($identifier, $this->number_of_seats, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`id_pilot`':
                        $stmt->bindValue($identifier, $this->id_pilot, PDO::PARAM_INT);
                        break;
                    case '`id_plane`':
                        $stmt->bindValue($identifier, $this->id_plane, PDO::PARAM_INT);
                        break;
                    case '`expires_at`':
                        $stmt->bindValue($identifier, $this->expires_at, PDO::PARAM_STR);
                        break;
                    case '`price`':
                        $stmt->bindValue($identifier, $this->price, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`departure_location`':
                        $stmt->bindValue($identifier, $this->departure_location, PDO::PARAM_STR);
                        break;
                    case '`destination_location`':
                        $stmt->bindValue($identifier, $this->destination_location, PDO::PARAM_STR);
                        break;
                    case '`id_departure_location`':
                        $stmt->bindValue($identifier, $this->id_departure_location, PDO::PARAM_INT);
                        break;
                    case '`id_destination_location`':
                        $stmt->bindValue($identifier, $this->id_destination_location, PDO::PARAM_INT);
                        break;
                    case '`estimated_flight_time`':
                        $stmt->bindValue($identifier, $this->estimated_flight_time, PDO::PARAM_STR);
                        break;
                    case '`rating`':
                        $stmt->bindValue($identifier, $this->rating, PDO::PARAM_STR);
                        break;
                    case '`departure_date`':
                        $stmt->bindValue($identifier, $this->departure_date, PDO::PARAM_STR);
                        break;
                    case '`destination_date`':
                        $stmt->bindValue($identifier, $this->destination_date, PDO::PARAM_STR);
                        break;
                    case '`departure_time`':
                        $stmt->bindValue($identifier, $this->departure_time, PDO::PARAM_STR);
                        break;
                    case '`destination_time`':
                        $stmt->bindValue($identifier, $this->destination_time, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }
    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUser !== null) {
                if (!$this->aUser->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aUser->getValidationFailures());
                }
            }

            if ($this->aAirplane !== null) {
                if (!$this->aAirplane->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAirplane->getValidationFailures());
                }
            }

            if ($this->aAirportRelatedByIdDepartureLocation !== null) {
                if (!$this->aAirportRelatedByIdDepartureLocation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAirportRelatedByIdDepartureLocation->getValidationFailures());
                }
            }

            if ($this->aAirportRelatedByIdDestinationLocation !== null) {
                if (!$this->aAirportRelatedByIdDestinationLocation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAirportRelatedByIdDestinationLocation->getValidationFailures());
                }
            }


            if (($retval = FlightRoutePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


            if ($this->collReceivedOrders !== null) {
                foreach ($this->collReceivedOrders as $referrerFK) {
                    if (!$referrerFK->validate($columns)) {
                        $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                    }
                }
            }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = FlightRoutePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getActive();
                break;
            case 2:
                return $this->getNumberOfSeats();
                break;
            case 3:
                return $this->getName();
                break;
            case 4:
                return $this->getIdPilot();
                break;
            case 5:
                return $this->getIdPlane();
                break;
            case 6:
                return $this->getExpiresAt();
                break;
            case 7:
                return $this->getPrice();
                break;
            case 8:
                return $this->getDescription();
                break;
            case 9:
                return $this->getDepartureLocation();
                break;
            case 10:
                return $this->getDestinationLocation();
                break;
            case 11:
                return $this->getIdDepartureLocation();
                break;
            case 12:
                return $this->getIdDestinationLocation();
                break;
            case 13:
                return $this->getEstimatedFlightTime();
                break;
            case 14:
                return $this->getRating();
                break;
            case 15:
                return $this->getDepartureDate();
                break;
            case 16:
                return $this->getDestinationDate();
                break;
            case 17:
                return $this->getDepartureTime();
                break;
            case 18:
                return $this->getDestinationTime();
                break;
            case 19:
                return $this->getCreatedAt();
                break;
            case 20:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['FlightRoute'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['FlightRoute'][$this->getPrimaryKey()] = true;
        $keys = FlightRoutePeer::getFieldNames($keyType);
        $result = array(
          $keys[0]  => $this->getId(),
          $keys[1]  => $this->getActive(),
          $keys[2]  => $this->getNumberOfSeats(),
          $keys[3]  => $this->getName(),
          $keys[4]  => $this->getIdPilot(),
          $keys[5]  => $this->getIdPlane(),
          $keys[6]  => $this->getExpiresAt(),
          $keys[7]  => $this->getPrice(),
          $keys[8]  => $this->getDescription(),
          $keys[9]  => $this->getDepartureLocation(),
          $keys[10] => $this->getDestinationLocation(),
          $keys[11] => $this->getIdDepartureLocation(),
          $keys[12] => $this->getIdDestinationLocation(),
          $keys[13] => $this->getEstimatedFlightTime(),
          $keys[14] => $this->getRating(),
          $keys[15] => $this->getDepartureDate(),
          $keys[16] => $this->getDestinationDate(),
          $keys[17] => $this->getDepartureTime(),
          $keys[18] => $this->getDestinationTime(),
          $keys[19] => $this->getCreatedAt(),
          $keys[20] => $this->getUpdatedAt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUser) {
                $result['User'] = $this->aUser->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAirplane) {
                $result['Airplane'] = $this->aAirplane->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAirportRelatedByIdDepartureLocation) {
                $result['AirportRelatedByIdDepartureLocation'] = $this->aAirportRelatedByIdDepartureLocation->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAirportRelatedByIdDestinationLocation) {
                $result['AirportRelatedByIdDestinationLocation'] = $this->aAirportRelatedByIdDestinationLocation->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collReceivedOrders) {
                $result['ReceivedOrders'] = $this->collReceivedOrders->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = FlightRoutePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setActive($value);
                break;
            case 2:
                $this->setNumberOfSeats($value);
                break;
            case 3:
                $this->setName($value);
                break;
            case 4:
                $this->setIdPilot($value);
                break;
            case 5:
                $this->setIdPlane($value);
                break;
            case 6:
                $this->setExpiresAt($value);
                break;
            case 7:
                $this->setPrice($value);
                break;
            case 8:
                $this->setDescription($value);
                break;
            case 9:
                $this->setDepartureLocation($value);
                break;
            case 10:
                $this->setDestinationLocation($value);
                break;
            case 11:
                $this->setIdDepartureLocation($value);
                break;
            case 12:
                $this->setIdDestinationLocation($value);
                break;
            case 13:
                $this->setEstimatedFlightTime($value);
                break;
            case 14:
                $this->setRating($value);
                break;
            case 15:
                $this->setDepartureDate($value);
                break;
            case 16:
                $this->setDestinationDate($value);
                break;
            case 17:
                $this->setDepartureTime($value);
                break;
            case 18:
                $this->setDestinationTime($value);
                break;
            case 19:
                $this->setCreatedAt($value);
                break;
            case 20:
                $this->setUpdatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = FlightRoutePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr))
            $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr))
            $this->setActive($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr))
            $this->setNumberOfSeats($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr))
            $this->setName($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr))
            $this->setIdPilot($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr))
            $this->setIdPlane($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr))
            $this->setExpiresAt($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr))
            $this->setPrice($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr))
            $this->setDescription($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr))
            $this->setDepartureLocation($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr))
            $this->setDestinationLocation($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr))
            $this->setIdDepartureLocation($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr))
            $this->setIdDestinationLocation($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr))
            $this->setEstimatedFlightTime($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr))
            $this->setRating($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr))
            $this->setDepartureDate($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr))
            $this->setDestinationDate($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr))
            $this->setDepartureTime($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr))
            $this->setDestinationTime($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr))
            $this->setCreatedAt($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr))
            $this->setUpdatedAt($arr[$keys[20]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FlightRoutePeer::DATABASE_NAME);

        if ($this->isColumnModified(FlightRoutePeer::ID))
            $criteria->add(FlightRoutePeer::ID, $this->id);
        if ($this->isColumnModified(FlightRoutePeer::ACTIVE))
            $criteria->add(FlightRoutePeer::ACTIVE, $this->active);
        if ($this->isColumnModified(FlightRoutePeer::NUMBER_OF_SEATS))
            $criteria->add(FlightRoutePeer::NUMBER_OF_SEATS, $this->number_of_seats);
        if ($this->isColumnModified(FlightRoutePeer::NAME))
            $criteria->add(FlightRoutePeer::NAME, $this->name);
        if ($this->isColumnModified(FlightRoutePeer::ID_PILOT))
            $criteria->add(FlightRoutePeer::ID_PILOT, $this->id_pilot);
        if ($this->isColumnModified(FlightRoutePeer::ID_PLANE))
            $criteria->add(FlightRoutePeer::ID_PLANE, $this->id_plane);
        if ($this->isColumnModified(FlightRoutePeer::EXPIRES_AT))
            $criteria->add(FlightRoutePeer::EXPIRES_AT, $this->expires_at);
        if ($this->isColumnModified(FlightRoutePeer::PRICE))
            $criteria->add(FlightRoutePeer::PRICE, $this->price);
        if ($this->isColumnModified(FlightRoutePeer::DESCRIPTION))
            $criteria->add(FlightRoutePeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(FlightRoutePeer::DEPARTURE_LOCATION))
            $criteria->add(FlightRoutePeer::DEPARTURE_LOCATION, $this->departure_location);
        if ($this->isColumnModified(FlightRoutePeer::DESTINATION_LOCATION))
            $criteria->add(FlightRoutePeer::DESTINATION_LOCATION, $this->destination_location);
        if ($this->isColumnModified(FlightRoutePeer::ID_DEPARTURE_LOCATION))
            $criteria->add(FlightRoutePeer::ID_DEPARTURE_LOCATION, $this->id_departure_location);
        if ($this->isColumnModified(FlightRoutePeer::ID_DESTINATION_LOCATION))
            $criteria->add(FlightRoutePeer::ID_DESTINATION_LOCATION, $this->id_destination_location);
        if ($this->isColumnModified(FlightRoutePeer::ESTIMATED_FLIGHT_TIME))
            $criteria->add(FlightRoutePeer::ESTIMATED_FLIGHT_TIME, $this->estimated_flight_time);
        if ($this->isColumnModified(FlightRoutePeer::RATING))
            $criteria->add(FlightRoutePeer::RATING, $this->rating);
        if ($this->isColumnModified(FlightRoutePeer::DEPARTURE_DATE))
            $criteria->add(FlightRoutePeer::DEPARTURE_DATE, $this->departure_date);
        if ($this->isColumnModified(FlightRoutePeer::DESTINATION_DATE))
            $criteria->add(FlightRoutePeer::DESTINATION_DATE, $this->destination_date);
        if ($this->isColumnModified(FlightRoutePeer::DEPARTURE_TIME))
            $criteria->add(FlightRoutePeer::DEPARTURE_TIME, $this->departure_time);
        if ($this->isColumnModified(FlightRoutePeer::DESTINATION_TIME))
            $criteria->add(FlightRoutePeer::DESTINATION_TIME, $this->destination_time);
        if ($this->isColumnModified(FlightRoutePeer::CREATED_AT))
            $criteria->add(FlightRoutePeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(FlightRoutePeer::UPDATED_AT))
            $criteria->add(FlightRoutePeer::UPDATED_AT, $this->updated_at);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(FlightRoutePeer::DATABASE_NAME);
        $criteria->add(FlightRoutePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of FlightRoute (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setActive($this->getActive());
        $copyObj->setNumberOfSeats($this->getNumberOfSeats());
        $copyObj->setName($this->getName());
        $copyObj->setIdPilot($this->getIdPilot());
        $copyObj->setIdPlane($this->getIdPlane());
        $copyObj->setExpiresAt($this->getExpiresAt());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setDepartureLocation($this->getDepartureLocation());
        $copyObj->setDestinationLocation($this->getDestinationLocation());
        $copyObj->setIdDepartureLocation($this->getIdDepartureLocation());
        $copyObj->setIdDestinationLocation($this->getIdDestinationLocation());
        $copyObj->setEstimatedFlightTime($this->getEstimatedFlightTime());
        $copyObj->setRating($this->getRating());
        $copyObj->setDepartureDate($this->getDepartureDate());
        $copyObj->setDestinationDate($this->getDestinationDate());
        $copyObj->setDepartureTime($this->getDepartureTime());
        $copyObj->setDestinationTime($this->getDestinationTime());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getReceivedOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addReceivedOrder($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return FlightRoute Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return FlightRoutePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new FlightRoutePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param                  User $v
     * @return FlightRoute The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(User $v = null)
    {
        if ($v === null) {
            $this->setIdPilot(NULL);
        } else {
            $this->setIdPilot($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addFlightRoute($this);
        }


        return $this;
    }

    /**
     * Get the associated User object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUser === null && ($this->id_pilot !== null) && $doQuery) {
            $this->aUser = UserQuery::create()->findPk($this->id_pilot, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aUser->addFlightRoutes($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a Airplane object.
     *
     * @param                  Airplane $v
     * @return FlightRoute The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAirplane(Airplane $v = null)
    {
        if ($v === null) {
            $this->setIdPlane(NULL);
        } else {
            $this->setIdPlane($v->getId());
        }

        $this->aAirplane = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Airplane object, it will not be re-added.
        if ($v !== null) {
            $v->addFlightRoute($this);
        }


        return $this;
    }

    /**
     * Get the associated Airplane object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Airplane The associated Airplane object.
     * @throws PropelException
     */
    public function getAirplane(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAirplane === null && ($this->id_plane !== null) && $doQuery) {
            $this->aAirplane = AirplaneQuery::create()->findPk($this->id_plane, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aAirplane->addFlightRoutes($this);
             */
        }

        return $this->aAirplane;
    }

    /**
     * Declares an association between this object and a Airport object.
     *
     * @param                  Airport $v
     * @return FlightRoute The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAirportRelatedByIdDepartureLocation(Airport $v = null)
    {
        if ($v === null) {
            $this->setIdDepartureLocation(NULL);
        } else {
            $this->setIdDepartureLocation($v->getId());
        }

        $this->aAirportRelatedByIdDepartureLocation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Airport object, it will not be re-added.
        if ($v !== null) {
            $v->addFlightRouteRelatedByIdDepartureLocation($this);
        }


        return $this;
    }

    /**
     * Get the associated Airport object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Airport The associated Airport object.
     * @throws PropelException
     */
    public function getAirportRelatedByIdDepartureLocation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAirportRelatedByIdDepartureLocation === null && ($this->id_departure_location !== null) && $doQuery) {
            $this->aAirportRelatedByIdDepartureLocation = AirportQuery::create()->findPk($this->id_departure_location, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aAirportRelatedByIdDepartureLocation->addFlightRoutesRelatedByIdDepartureLocation($this);
             */
        }

        return $this->aAirportRelatedByIdDepartureLocation;
    }

    /**
     * Declares an association between this object and a Airport object.
     *
     * @param                  Airport $v
     * @return FlightRoute The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAirportRelatedByIdDestinationLocation(Airport $v = null)
    {
        if ($v === null) {
            $this->setIdDestinationLocation(NULL);
        } else {
            $this->setIdDestinationLocation($v->getId());
        }

        $this->aAirportRelatedByIdDestinationLocation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Airport object, it will not be re-added.
        if ($v !== null) {
            $v->addFlightRouteRelatedByIdDestinationLocation($this);
        }


        return $this;
    }

    /**
     * Get the associated Airport object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Airport The associated Airport object.
     * @throws PropelException
     */
    public function getAirportRelatedByIdDestinationLocation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAirportRelatedByIdDestinationLocation === null && ($this->id_destination_location !== null) && $doQuery) {
            $this->aAirportRelatedByIdDestinationLocation = AirportQuery::create()->findPk($this->id_destination_location, $con);
            /* The following can be used additionally to
              guarantee the related object contains a reference
              to this object.  This level of coupling may, however, be
              undesirable since it could result in an only partially populated collection
              in the referenced object.
              $this->aAirportRelatedByIdDestinationLocation->addFlightRoutesRelatedByIdDestinationLocation($this);
             */
        }

        return $this->aAirportRelatedByIdDestinationLocation;
    }

    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ReceivedOrder' == $relationName) {
            $this->initReceivedOrders();
        }
    }

    /**
     * Clears out the collReceivedOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return FlightRoute The current object (for fluent API support)
     * @see        addReceivedOrders()
     */
    public function clearReceivedOrders()
    {
        $this->collReceivedOrders = null; // important to set this to null since that means it is uninitialized
        $this->collReceivedOrdersPartial = null;

        return $this;
    }

    /**
     * reset is the collReceivedOrders collection loaded partially
     *
     * @return void
     */
    public function resetPartialReceivedOrders($v = true)
    {
        $this->collReceivedOrdersPartial = $v;
    }

    /**
     * Initializes the collReceivedOrders collection.
     *
     * By default this just sets the collReceivedOrders collection to an empty array (like clearcollReceivedOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initReceivedOrders($overrideExisting = true)
    {
        if (null !== $this->collReceivedOrders && !$overrideExisting) {
            return;
        }
        $this->collReceivedOrders = new PropelObjectCollection();
        $this->collReceivedOrders->setModel('ReceivedOrder');
    }

    /**
     * Gets an array of ReceivedOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this FlightRoute is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|ReceivedOrder[] List of ReceivedOrder objects
     * @throws PropelException
     */
    public function getReceivedOrders($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collReceivedOrdersPartial && !$this->isNew();
        if (null === $this->collReceivedOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collReceivedOrders) {
                // return empty collection
                $this->initReceivedOrders();
            } else {
                $collReceivedOrders = ReceivedOrderQuery::create(null, $criteria)
                    ->filterByFlightRoute($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collReceivedOrdersPartial && count($collReceivedOrders)) {
                        $this->initReceivedOrders(false);

                        foreach ($collReceivedOrders as $obj) {
                            if (false == $this->collReceivedOrders->contains($obj)) {
                                $this->collReceivedOrders->append($obj);
                            }
                        }

                        $this->collReceivedOrdersPartial = true;
                    }

                    $collReceivedOrders->getInternalIterator()->rewind();

                    return $collReceivedOrders;
                }

                if ($partial && $this->collReceivedOrders) {
                    foreach ($this->collReceivedOrders as $obj) {
                        if ($obj->isNew()) {
                            $collReceivedOrders[] = $obj;
                        }
                    }
                }

                $this->collReceivedOrders = $collReceivedOrders;
                $this->collReceivedOrdersPartial = false;
            }
        }

        return $this->collReceivedOrders;
    }

    /**
     * Sets a collection of ReceivedOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $receivedOrders A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return FlightRoute The current object (for fluent API support)
     */
    public function setReceivedOrders(PropelCollection $receivedOrders, PropelPDO $con = null)
    {
        $receivedOrdersToDelete = $this->getReceivedOrders(new Criteria(), $con)->diff($receivedOrders);


        $this->receivedOrdersScheduledForDeletion = $receivedOrdersToDelete;

        foreach ($receivedOrdersToDelete as $receivedOrderRemoved) {
            $receivedOrderRemoved->setFlightRoute(null);
        }

        $this->collReceivedOrders = null;
        foreach ($receivedOrders as $receivedOrder) {
            $this->addReceivedOrder($receivedOrder);
        }

        $this->collReceivedOrders = $receivedOrders;
        $this->collReceivedOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ReceivedOrder objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related ReceivedOrder objects.
     * @throws PropelException
     */
    public function countReceivedOrders(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collReceivedOrdersPartial && !$this->isNew();
        if (null === $this->collReceivedOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collReceivedOrders) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getReceivedOrders());
            }
            $query = ReceivedOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                    ->filterByFlightRoute($this)
                    ->count($con);
        }

        return count($this->collReceivedOrders);
    }

    /**
     * Method called to associate a ReceivedOrder object to this object
     * through the ReceivedOrder foreign key attribute.
     *
     * @param    ReceivedOrder $l ReceivedOrder
     * @return FlightRoute The current object (for fluent API support)
     */
    public function addReceivedOrder(ReceivedOrder $l)
    {
        if ($this->collReceivedOrders === null) {
            $this->initReceivedOrders();
            $this->collReceivedOrdersPartial = true;
        }

        if (!in_array($l, $this->collReceivedOrders->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddReceivedOrder($l);

            if ($this->receivedOrdersScheduledForDeletion and $this->receivedOrdersScheduledForDeletion->contains($l)) {
                $this->receivedOrdersScheduledForDeletion->remove($this->receivedOrdersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	ReceivedOrder $receivedOrder The receivedOrder object to add.
     */
    protected function doAddReceivedOrder($receivedOrder)
    {
        $this->collReceivedOrders[] = $receivedOrder;
        $receivedOrder->setFlightRoute($this);
    }

    /**
     * @param	ReceivedOrder $receivedOrder The receivedOrder object to remove.
     * @return FlightRoute The current object (for fluent API support)
     */
    public function removeReceivedOrder($receivedOrder)
    {
        if ($this->getReceivedOrders()->contains($receivedOrder)) {
            $this->collReceivedOrders->remove($this->collReceivedOrders->search($receivedOrder));
            if (null === $this->receivedOrdersScheduledForDeletion) {
                $this->receivedOrdersScheduledForDeletion = clone $this->collReceivedOrders;
                $this->receivedOrdersScheduledForDeletion->clear();
            }
            $this->receivedOrdersScheduledForDeletion[] = $receivedOrder;
            $receivedOrder->setFlightRoute(null);
        }

        return $this;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FlightRoute is new, it will return
     * an empty collection; or if this FlightRoute has previously
     * been saved, it will retrieve related ReceivedOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FlightRoute.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|ReceivedOrder[] List of ReceivedOrder objects
     */
    public function getReceivedOrdersJoinUserPersonalInfo($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ReceivedOrderQuery::create(null, $criteria);
        $query->joinWith('UserPersonalInfo', $join_behavior);

        return $this->getReceivedOrders($query, $con);
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FlightRoute is new, it will return
     * an empty collection; or if this FlightRoute has previously
     * been saved, it will retrieve related ReceivedOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FlightRoute.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|ReceivedOrder[] List of ReceivedOrder objects
     */
    public function getReceivedOrdersJoinPaymentMethod($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ReceivedOrderQuery::create(null, $criteria);
        $query->joinWith('PaymentMethod', $join_behavior);

        return $this->getReceivedOrders($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->active = null;
        $this->number_of_seats = null;
        $this->name = null;
        $this->id_pilot = null;
        $this->id_plane = null;
        $this->expires_at = null;
        $this->price = null;
        $this->description = null;
        $this->departure_location = null;
        $this->destination_location = null;
        $this->id_departure_location = null;
        $this->id_destination_location = null;
        $this->estimated_flight_time = null;
        $this->rating = null;
        $this->departure_date = null;
        $this->destination_date = null;
        $this->departure_time = null;
        $this->destination_time = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collReceivedOrders) {
                foreach ($this->collReceivedOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aUser instanceof Persistent) {
                $this->aUser->clearAllReferences($deep);
            }
            if ($this->aAirplane instanceof Persistent) {
                $this->aAirplane->clearAllReferences($deep);
            }
            if ($this->aAirportRelatedByIdDepartureLocation instanceof Persistent) {
                $this->aAirportRelatedByIdDepartureLocation->clearAllReferences($deep);
            }
            if ($this->aAirportRelatedByIdDestinationLocation instanceof Persistent) {
                $this->aAirportRelatedByIdDestinationLocation->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collReceivedOrders instanceof PropelCollection) {
            $this->collReceivedOrders->clearIterator();
        }
        $this->collReceivedOrders = null;
        $this->aUser = null;
        $this->aAirplane = null;
        $this->aAirportRelatedByIdDepartureLocation = null;
        $this->aAirportRelatedByIdDestinationLocation = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string The value of the 'name' column
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }
    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     FlightRoute The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[] = FlightRoutePeer::UPDATED_AT;

        return $this;
    }
}
