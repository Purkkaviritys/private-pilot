<?php

namespace Lamk\PrivatePilotBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use Lamk\PrivatePilotBundle\Model\om\BaseUser;

class User extends BaseUser implements UserInterface
{

    public function eraseCredentials()
    {
        /**
         * Does user have any sensitive information that has to be
         * removed before persisting a token?
         */
        return null;
    }

    public function getRoles()
    {
        /**
         * Get user's role object from Propel,
         * turn that object into an array,
         * cut the element 'Id' from the array,
         * and offer that array as user's role to the Symfony.
         */
        $userRole = $this->getUserRole()->toArray();
        unset($userRole['Id']);

        return $userRole;
    }
}
