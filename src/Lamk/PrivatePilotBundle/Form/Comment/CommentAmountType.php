<?php

namespace Lamk\PrivatePilotBundle\Form\Comment;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommentAmountType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('commentamount', 'choice', array(
          'choices' => array(
            '10' => '10',
            '15' => '15',
            '20' => '20',
            '25' => '25',
        )));
    }

    public function getName()
    {
        return 'commentamount';
    }
}
