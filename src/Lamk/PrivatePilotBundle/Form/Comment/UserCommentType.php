<?php

namespace Lamk\PrivatePilotBundle\Form\Comment;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserCommentType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\UserComment',
      'name'       => 'usercomment',
    );

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('idUser');
        $builder->add('idThread');
        $builder->add('idParent');
        $builder->add('commentDate');
        $builder->add('commentTime');
        $builder->add('comment');
    }
}
