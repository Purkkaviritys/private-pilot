<?php

namespace Lamk\PrivatePilotBundle\Form\Comment;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommentType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\UserComment',
      'name'       => 'usercomment',
    );

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id_user', 'hidden')
            ->add('id_thread', 'hidden')
            ->add('id_parent', 'hidden')
            ->add('comment', 'textarea', array(
              'max_length' => 1500,
              'label'      => 'Write a comment'))
            ->add('submit', 'submit');
    }

    public function getName()
    {
        return 'usercomment';
    }
}
