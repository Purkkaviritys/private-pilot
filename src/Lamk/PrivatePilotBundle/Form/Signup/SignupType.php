<?php

namespace Lamk\PrivatePilotBundle\Form\Signup;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of SignupType
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version 0.2
 * @since 0.2
 */
class SignupType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\User',
      'name'       => 'user',
    );

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('username');

        $builder->add('emailAddress', 'email', array(
          'label'    => 'Email Address',
          'required' => true,
        ));

        $builder->add('password', 'repeated', array(
          'first_name'  => 'password',
          'second_name' => 'confirm',
          'type'        => 'password',
          // 'label' => 'Password',
          'required'    => true,
        ));

        $builder->add('submit', 'submit');
    }
}
