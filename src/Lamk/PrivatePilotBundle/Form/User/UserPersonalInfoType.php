<?php

namespace Lamk\PrivatePilotBundle\Form\User;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserPersonalInfoType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', array(
          'required' => true,
        ));
        $builder->add('lastName', 'text', array(
          'required' => true,
        ));
        $builder->add('line1', 'text', array(
          'required' => true,
        ));
        $builder->add('line2', 'text', array(
          'required' => false,
        ));
        $builder->add('city', 'text', array(
          'required' => true,
        ));
        $builder->add('state', 'text', array(
          'required' => true,
        ));
        $builder->add('postalCode', 'integer', array(
          'required' => true,
        ));
        $builder->add('countryCode', 'country', array(
          'required' => true,
        ));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\UserPersonalInfo',
        ));
    }

    public function getName()
    {
        return 'userpersonalinfo';
    }
}
