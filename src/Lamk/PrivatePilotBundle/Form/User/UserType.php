<?php

namespace Lamk\PrivatePilotBundle\Form\User;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('emailAddress', 'email', array(
          'label'    => 'Email Address',
          'required' => true
        ));
        $builder->add('description', 'textarea', array(
          'label'    => 'Description',
          'required' => false
        ));
        $builder->add('timezone', 'timezone', array(
          'label'    => 'Timezone',
          'required' => false
        ));
        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\User',
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
