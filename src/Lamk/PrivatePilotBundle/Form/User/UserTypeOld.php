<?php

namespace Lamk\PrivatePilotBundle\Form\User;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('active', 'checkbox', array(
          'required' => true,
        ));
        $builder->add('username', 'text', array(
          'required' => true,
        ));
        $builder->add('firstname', 'text', array(
          'required' => false,
        ));
        $builder->add('lastname', 'text', array(
          'required' => false,
        ));
        $builder->add('emailaddress', 'email', array(
          'required' => true,
        ));
        $builder->add('idrole', 'choice', array(
          'choices'  => array(
            '1' => 'Admin',
            '2' => 'Pilot',
            '3' => 'User',
            '4' => 'Customer',
            '5' => 'Anonymous',
            '6' => 'Banned',
          ),
          'multiple' => false,
          'required' => true,
        ));
        // $builder->add('idaddress', 'hidden');
        $builder->add('description', 'textarea', array(
          'required' => false,
        ));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\User',
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
