<?php

namespace Lamk\PrivatePilotBundle\Form\User;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AvatarType extends BaseAbstractType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => null,
          'name'       => 'avatar',
        ));
    }

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('avatar', 'file');
        $builder->add('submit', 'submit');
    }

    public function getName()
    {
        return 'avatar';
    }
}
