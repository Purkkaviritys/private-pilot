<?php

namespace Lamk\PrivatePilotBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangePasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->add('oldPassword', 'password');
        $builder->add('password', 'repeated', array(
          'type'            => 'password',
          'invalid_message' => 'The password fields must match.',
          'required'        => true,
          'first_options'   => array('label' => 'Password'),
          'second_options'  => array('label' => 'Repeat Password'),
        ));
        $builder->add('change', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\User',
        ));
    }

    public function getName()
    {
        return 'changepassword';
    }
}
