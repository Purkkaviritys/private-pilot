<?php

namespace Lamk\PrivatePilotBundle\Form\Airport;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AirportType extends BaseAbstractType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\Airport',
            // 'validation_groups' => array('airport'),
        ));
    }

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('description');
        $builder->add('type');
        $builder->add('latitude');
        $builder->add('longitude');
        $builder->add('elevation');
        $builder->add('state');
        $builder->add('city');
        $builder->add('municipality');
        $builder->add('country');
        $builder->add('continent');
        $builder->add('scheduledService');
        $builder->add('gpsCode');
        $builder->add('region');
        $builder->add('faa');
        $builder->add('iata');
        $builder->add('icao');
        $builder->add('role');
        $builder->add('enplanements');
        $builder->add('save', 'submit');
    }

    public function getName()
    {
        return 'airport';
    }
}
