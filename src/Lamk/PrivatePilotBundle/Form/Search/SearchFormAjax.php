<?php

namespace Lamk\PrivatePilotBundle\Form\Search;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

//use Symfony\Component\Form\Extension\Core\DataTransformer\IntegerToLocalizedStringTransformer;

/**
 * Description of SearchForm
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version 0.2
 * @since 0.1
 */
class SearchForm extends AbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('departure', 'text');
        $builder->add('destination', 'text');
        /* $builder->add('passengers', 'integer', array(
          'precision'     => '0',
          'empty_data'    => '1',
          'disabled'      => false,
          'rounding_mode' => IntegerToLocalizedStringTransformer::ROUND_CEILING,
          'attr'          => array(
          'min' => '0',
          'max' => '5',))); */
        $builder->add('passengers', 'choice', array(
          'empty_value' => 'Passengers',
          'choices'     => array(
            '1' => 'One',
            '2' => 'Two',
            '3' => 'Three',
            '4' => 'Four',
          )
        ));
        $builder->add('date', 'date', array(
          'widget'   => 'single_text',
          //'format'   => 'MM-dd-yy',
          'format'   => 'yyyy-MM-dd',
          'disabled' => false,
            //'empty_value' => date('MM-dd-yyyy'),
        ));
        $builder->add('search', 'submit');
    }

    public function getName()
    {
        return 'search';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'validation_groups' => array('search'),
        ));
    }
}
