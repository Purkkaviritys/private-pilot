<?php

namespace Lamk\PrivatePilotBundle\Form\Sort;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SortType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sortresults', 'choice', array(
          'choices'  => array(
            '1' => 'Name',
            '2' => 'Pilot',
            '3' => 'Price',
            '4' => 'Date',
            '5' => 'Accuracy',
          ),
          'multiple' => false,
        ));
    }

    public function getName()
    {
        return 'sort';
    }
}
