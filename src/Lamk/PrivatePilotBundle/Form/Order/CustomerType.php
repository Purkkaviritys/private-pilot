<?php

namespace Lamk\PrivatePilotBundle\Form\Order;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of DetailType
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 */
class CustomerType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname', 'text', array(
          'required' => true,
        ));
        $builder->add('lastname', 'text', array(
          'required' => true,
        ));
        $builder->add('line1', 'text', array(
          'required' => true,
        ));
        $builder->add('line2', 'text', array(
          'required' => false,
        ));
        $builder->add('city', 'text', array(
          'required' => true,
        ));
        $builder->add('state', 'text', array(
          'required' => true,
        ));
        $builder->add('postcode', 'text', array(
          'required' => true,
        ));
        $builder->add('countrycode', 'text', array(
          'required' => true,
        ));
        $builder->add('Submit Customer Info', 'submit');
    }

    public function getName()
    {
        return 'customer';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\ReceivedOrder',
          'name'       => 'customer',
        ));
    }
}
