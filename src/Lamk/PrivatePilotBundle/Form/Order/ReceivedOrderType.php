<?php

namespace Lamk\PrivatePilotBundle\Form\Order;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ReceivedOrderType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\ReceivedOrder',
      'name'       => 'receivedorder',
    );

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('idUser', 'integer');
        $builder->add('idFlightRoute', 'integer');
        $builder->add('numberOfReservedSeats', 'integer');
        $builder->add('orderDate', 'date');
        $builder->add('orderTime', 'time');
        $builder->add('idOrderPaymentMethod', 'integer');
    }

    public function getName()
    {
        return 'receivedorder';
    }
}
