<?php

namespace Lamk\PrivatePilotBundle\Form\Order;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of DetailType
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 */
class OrderDetailType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numberofreservedseats', 'choice', array(
          'label'       => 'How many seats do you want to book?',
          'empty_value' => 'Choose amount of seats',
          'required'    => true,
          'choices'     => array(
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
          ),
          'multiple'    => false,));
        $builder->add('idorderpaymentmethod', 'model', array(
          'label'       => 'What payment method do you wish to use?',
          'empty_value' => 'Choose a payment method',
          'required'    => true,
          'class'       => 'Lamk\PrivatePilotBundle\Model\PaymentMethod',
          'property'    => 'name',
          //'index_property' => 'name',
          'multiple'    => false,
        ));
        $builder->add('Submit order', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\ReceivedOrder',
        ));
    }

    public function getName()
    {
        return 'orderdetails';
    }
}
