<?php

namespace Lamk\PrivatePilotBundle\Form\Order;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of DetailType
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 */
class DetailType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\ReceivedOrder',
      'name'       => 'order',
    );

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* $builder->add('iduser', 'hidden'); */
        $builder->add('idflightroute', 'hidden');
        $builder->add('numberofreservedseats', 'choice', array(
          'empty_value' => 'Choose amount of seats',
          'label'       => 'How many seats do you want to book?',
          'required'    => true,
          'choices'     => array(
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
          ),
          'multiple'    => false,));
        $builder->add('idorderpaymentmethod', 'choice', array(
          'empty_value' => 'Choose a payment method',
          'label'       => 'What payment method do you wish to use?',
          'required'    => true,
          'choices'     => array(
            '1' => 'Paypal',
            '2' => 'Credit Card',
            '3' => 'Bill',
          ),
          'multiple'    => false,));
        $builder->add('Submit order', 'submit');
    }

    public function getName()
    {
        return 'Details';
    }
}
