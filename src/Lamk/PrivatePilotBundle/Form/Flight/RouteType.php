<?php

namespace Lamk\PrivatePilotBundle\Form\Flight;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RouteType extends BaseAbstractType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\FlightRoute',
        ));
    }

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', 'text', array(
          'required' => true
        ));
        $builder->add('description', 'textarea', array(
          'required' => false
        ));
        $builder->add('price', 'integer', array(
          'required' => true
        ));
        $builder->add('numberOfSeats', 'choice', array(
          'empty_value' => 'Passenger seats available',
          'choices'     => array(
            '1' => 'One',
            '2' => 'Two',
            '3' => 'Three',
            '4' => 'Four',
            '5' => 'Five',
            '6' => 'Six',
          ),
          'required'    => true,
        ));
        $builder->add('departureLocation', 'text', array(
          'required' => true
        ));
        $builder->add('destinationLocation', 'text', array(
          'required' => true
        ));
        $builder->add('departureDate', 'date', array(
          'required' => true
        ));
        $builder->add('destinationDate', 'date', array(
          'required' => true
        ));
        $builder->add('departureTime', 'time', array(
          'required' => true
        ));
        $builder->add('destinationTime', 'time', array(
          'required' => true
        ));
        $builder->add('expiresat', 'datetime', array(
          'required' => true
        ));
        /*$builder->add('active', 'checkbox', array(
          'label'    => 'Show this Flight Route publicly?',
          'required' => false,
        ));*/

        $builder->add('save', 'submit');
    }

    public function getName()
    {
        return 'flightroute';
    }
}
