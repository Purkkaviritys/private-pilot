<?php

namespace Lamk\PrivatePilotBundle\Form\Airplane;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AirplaneType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\Airplane',
      'name'       => 'airplane',
    );

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('airplaneModel', 'text');
        $builder->add('description', 'textarea');
        $builder->add('airplaneManufacturingDate', 'date');
        $builder->add('airplaneCountry', 'country');
        $builder->add('airplaneSerialNumber', 'text');
        $builder->add('airplaneTtaf', 'text');
        $builder->add('numberOfSeatsAvailable', 'integer');
        $builder->add('save', 'submit');
    }

    public function getName()
    {
        return 'airplane';
    }
}
