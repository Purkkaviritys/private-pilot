<?php

namespace Lamk\PrivatePilotBundle\Form\Type;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserRoleType extends BaseAbstractType
{
    protected $options = array(
      'data_class' => 'Lamk\PrivatePilotBundle\Model\UserRole',
      'name'       => 'userrole',);

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('role');
        $builder->add('add', 'submit');
    }

    public function getName()
    {
        return 'userrole';
    }
}
