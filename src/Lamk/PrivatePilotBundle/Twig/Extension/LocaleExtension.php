<?php

namespace Lamk\PrivatePilotBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Matt Drollette <matt@drollette.com>
 */
class LocaleExtension extends \Twig_Extension
{
    protected $container;
    protected $timezone;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->timezone = $this->container->get('session')->get('timezone', 'UTC');
    }

    public function getFunctions()
    {
        return array();
    }

    public function getFilters()
    {
        return array(
          'datetime' => new \Twig_Filter_Method($this, 'formatDatetime', array('is_safe' => array('html'))),
        );
    }

    public function formatDatetime($date, $timezone = null)
    {
        if (null === $timezone) {
            $timezone = $this->timezone;
        }
        if (!$date instanceof \DateTime) {
            if (ctype_digit((string) $date)) {
                $date = new \DateTime('@' . $date);
            } else {
                $date = new \DateTime($date);
            }
        }
        if (!$timezone instanceof \DateTimeZone) {
            $timezone = new \DateTimeZone($timezone);
        }
        $date->setTimezone($timezone);
        return $date;
    }

    public function getName()
    {
        return 'locale';
    }
}
