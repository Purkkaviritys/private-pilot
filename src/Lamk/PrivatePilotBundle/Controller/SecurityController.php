<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Security Controller handles service security related functions
 *
 * Security Controller handles service security related operations such as
 * loggin in and loggin out of the system. It is also used to various other
 * security related tasks.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class SecurityController extends Controller
{

    /**
     * @todo We could build the login form here.
     * And then print it on login page separately. It's supposedly more secure.
     */
    public function loginForm()
    {
        
    }

    /**
     * loginAction
     *
     * @todo Write actual login manager for this thing.
     */
    public function loginAction(Request $request)
    {

        $session = $request->getSession();

        /**
         * Catch the login error if there is one.
         */
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        }

        return $this->render('::loginTemplate.html.twig', array(
              /**
               * Previous username entered by the user.
               */
              'last_username' => $session->get(SecurityContextInterface::LAST_USERNAME),
              'error'         => $error
        ));
    }
}
