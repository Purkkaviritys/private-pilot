<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Lamk\PrivatePilotBundle\Model\File;
use Lamk\PrivatePilotBundle\Model\FileQuery;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotBundle\Model\Airplane;

/**
 * Description of PilotController
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class PilotController extends Controller
{

    /**
     * @Template()
     */
    public function licenseAction()
    {
        /* We should start session here. */
        /* $session = new Session();

          $session->set('name', 'Antti');

          echo $session->get('name');

          var_dump($session); */

        return array();
    }

    /**
     * Common user is asked information:
     */
    public function addPlaneAction(Request $request)
    {
        
    }

    /**
     * When user has been elevated do:
     */
    public function addAction(Request $request)
    {
        
    }

    /**
     * Update user to become a pilot with an airplane.
     */
    public function upgradeAction()
    {

        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->findOneById($id);

        $airplane = new Airplane();

        $user->setIdRole($pilotRole = 2)
            ->setAirplane($airplane)
            ->save();

        return $this->redirect($this->generateUrl('lamk_private_pilot_logout'));
    }
}
