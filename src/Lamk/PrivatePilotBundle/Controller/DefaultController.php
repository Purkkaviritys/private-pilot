<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Criteria;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Lamk\PrivatePilotBundle\Form\Search\SearchForm;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;

/**
 * DefaultController for managing front page of the website
 *
 * <p>DefaultController manages the front page of the private pilot website. It
 * doesn't have any particular function but it acts as a bridge for all
 * the different parts the site is made of.</p>
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class DefaultController extends Controller
{

    /**
     * indexAction
     */
    public function indexAction(Request $request)
    {
        $logger = $this->get('logger');

        /**
         * If there are no data in the database skip this and input empty string
         * to prevent running into problems.
         */
        if (FlightRouteQuery::create()->count() >= 1) {
            $openDepartures = FlightRouteQuery::create()
                ->select(array('departure_location'))
                ->find();
            $openDestinations = FlightRouteQuery::create()
                ->select(array('destination_location'))
                ->find();
        } else {
            $logger->info('FlightRouteQuery returned less than one result.');
            $openDepartures = [''];
            $openDestinations = [''];
        }

        $form = $this->createForm(new SearchForm(), array(
          'action' => $this->generateUrl('lamk_private_pilot_search'),
          'method' => 'POST'));

        if ($request->isXmlHttpRequest()) {
            //json response object.
        }


        return $this->render('LamkPrivatePilotBundle:Default:index.html.twig', array(
              'form'                   => $form->createView(),
              'available_destinations' => $openDestinations,
              'available_departures'   => $openDepartures
        ));
    }

    /**
     * availableDeparturesAction
     */
    public function availableDeparturesAction(Request $request)
    {
        $c = new \Criteria();
        /*$value = $request->get('term');
        $departures = FlightRouteQuery::create()
            ->filterByActive(TRUE)
            ->findByDepartureLocation($value . '%');*/

        $openDepartures = FlightRouteQuery::create(null, $c->setDistinct())
            ->select(array('departure_location'))
            ->find();

        $json = array();

        /*foreach ($departures as $departure) {
            $json[] = array(
              'label' => $departure->getDepartureLocation(),
              'value' => $departure->getDepartureLocation(),
            );
        }*/

        foreach ($openDepartures as $openDeparture) {
            $json[] = array(
              'label' => $openDeparture,
              'value' => $openDeparture,
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($json));

        return $response;
    }

    /**
     * availableDestinationsAction
     */
    public function availableDestinationsAction(Request $request)
    {

        $c = new \Criteria();
        /*$value = $request->get('term');
        $destinations = FlightRouteQuery::create(null, $c->setDistinct())
            ->filterByActive(TRUE)
            ->findByDestinationLocation($value . '%');*/

        $openDestinations = FlightRouteQuery::create(null, $c->setDistinct())
            ->select(array('destination_location'))
            ->find();

        //print_r($openDestinations);
        //die();

        $json = array();

        foreach ($openDestinations as $openDestination) {
            $json[] = array(
              'label' => $openDestination,
              'value' => $openDestination,
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($json));

        return $response;
    }

    /**
     * searchAction
     */
    public function searchAction(Request $request)
    {
        $logger = $this->get('logger');
        $form = $this->createForm(new SearchForm(), null);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            /**
             * Validation is broken because we don't have a model for
             * the search form Propel would require. There are alternative ways
             * to validate the data but they not on the priority list
             * at least not right now.
             */
            if ($form->isValid()) {
                /**
                 * We don't have a model for the form so we need to
                 * hand pick all the data from the form that comes from
                 * the response object Symfony2 arranges for us.
                 */
                $getDepartureData = $form->get('departure')->getData();
                $getDestinationData = $form->get('destination')->getData();
                $getPassengerData = $form->get('passengers')->getData();
                $getDateFromData = $form->get('date')->getData();
                $searchResults = FlightRouteQuery::create()
                    ->filterByDepartureLocation($getDepartureData)
                    ->filterByDestinationLocation($getDestinationData)
                    ->filterByNumberOfSeats($getPassengerData, Criteria::GREATER_EQUAL)
                    ->filterByDepartureDate($getDateFromData, Criteria::GREATER_EQUAL)
                    ->find();
                $searchCount = $searchResults->count();

                return $this->render("LamkPrivatePilotBundle:Default:result.html.twig", array(
                      'searchresults' => $searchResults,
                      'form'          => $form->createView(),
                      'count'         => $searchCount));
            } else {
                $logger->error('Sent search form data was invalid!');
                return $this->redirect($this->generateUrl('lamk_private_pilot_homepage'));
            }
        }
        $logger->error('Sent form data was invalid!');
        return $this->redirect($this->generateUrl('lamk_private_pilot_homepage'));
    }

    /**
     * contactAction
     */
    public function contactAction()
    {
        return $this->render('LamkPrivatePilotBundle:Help:contact.html.twig');
    }
}
