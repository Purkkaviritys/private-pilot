<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\ReceivedOrder;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery;
use Lamk\PrivatePilotBundle\Form\Order\DetailType;
use Lamk\PrivatePilotBundle\Form\Order\OrderDetailType;
use Lamk\PrivatePilotBundle\Form\User\UserPersonalInfoType;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoQuery;
use Lamk\PrivatePilotBundle\Model\PaymentMethod;

/**
 * Order Controller is for processing orders the web service receives.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 * 
 */
class OrderController extends Controller
{

    /**
     * Inspect the order details.
     * 
     * @todo Split into two different functions.
     */
    public function inspectAction($id)
    {
        $getUserId = $this->getUser()->getId();
        $user = UserQuery::create()
            ->findPk($getUserId);
        $getUserIdPersonalInfo = $user->getIdPersonalInfo();
        $userPersonalInfo = UserPersonalInfoQuery::create()
            ->findPk($getUserIdPersonalInfo);

        /**
         * @TODO Do the Symfony thing and get Request object to
         * place it actually belongs to. This function is too large.
         * It should be split into two functions that do different things.
         */
        //$flight = FlightRouteQuery::create()->findPk($id);
        $flight = FlightRouteQuery::create()
            ->joinUser()
            ->joinAirplane()
            ->findPk($id);
        $pilotId = $flight->getIdPilot();
        $flightroutepilot = UserPersonalInfoQuery::create()
            ->findPk($pilotId);
        $order = new ReceivedOrder;
        $form = $this->createForm(new DetailType(), $order);
        $request = $this->getRequest();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            /**
             * @todo Form Validation doesn't work as intended due broken code.
             * It's about broken time/date object handling. I need to choose a
             * standard way to express time in this system or things will get
             * strange and these issues become a commonplace.
             * 
             * RFC 3339/ISO 8601
             */
            if ($form->isValid()) {
                $getFlightRouterIdentifier = $flight->getId();
                $getUserId = $this->getUser()->getId();
                $user = UserQuery::create()
                    ->findPk($getUserId);
                $getUserIdPersonalInfo = $user->getIdPersonalInfo();
                $getOpenSeats = $flight->getNumberOfSeats();
                $getReservedSeats = $form->get('numberofreservedseats')->getData();
                $getOrderPaymentMethod = $form->get('idorderpaymentmethod')->getData();

                // Does the plane even have the amount of seats available?
                if ($getOpenSeats < $getReservedSeats) {
                    $this->get('session')->getFlashBag()->add('notice', 'Not enough seats available in this plane.');
                    return $this->redirect($this->generateUrl('lamk_private_pilot_order_inspect', array('id' => $id)));
                }
                // Decrease the amount of open seats.
                $openSeatsInPlane = $getOpenSeats - $getReservedSeats;

                // Save the amount to database.
                $flight->setNumberOfSeats($openSeatsInPlane)
                    ->save();

                // Add new reservation to database.
                $order->setNumberOfReservedSeats($getReservedSeats)
                    ->setIdUserPersonalInfo($getUserIdPersonalInfo)
                    ->setIdFlightRoute($getFlightRouterIdentifier)
                    ->setIdOrderPaymentMethod($getOrderPaymentMethod)
                    ->save();

                $this->sendEmailSuccessfulOrder($emailAddress = $user->getEmailAddress());

                return $this->render("LamkPrivatePilotBundle:Order:success.html.twig");
            }
        }

        /**
         * @todo <p>Redirect user to some meaningful place and tell that things
         * went wrong, and the the user needs to try again later.</p>
         */
        return $this->render('LamkPrivatePilotBundle:Order:inspect.html.twig', array(
              'form'             => $form->createView(),
              'id'               => $id,
              'flightroutepilot' => $flightroutepilot,
              'flight'           => $flight,
              'customer'         => $userPersonalInfo,
        ));
    }

    /**
     * Send Email to User that the flight was reserved properly.
     */
    private function sendEmailSuccessfulOrder($emailAddress)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Private Pilot: Your order has been received')
            ->setFrom('orders@privatepilot.com')
            ->setTo($emailAddress)
            ->setBody("<p style='color: #000000;'>Your order has been successfully received by our website. Here's the information about your flight.</p>", 'text/html');

        $this->get('mailer')->send($message);
    }

    /**
     * Handle the order payment.
     * 
     * @TODO This has too many responsibilities. It should be split to two or three
     * different controllers. One for drawing the page, one for processing the
     * application, one for making the seat change, and one for sending
     * the email to the customer.
     */
    public function paymentAction(Request $request)
    {
        /**
         * Why I am not using session for these things? D:
         */
        $customerId = $request->get('customerId');
        $id = $request->get('flightId');
        $flight = FlightRouteQuery::create()
            ->joinUser()
            ->joinAirplane()
            ->findPk($id);

        $customer = UserPersonalInfoQuery::create()
            ->findOneById($customerId);
        $pilotId = $flight->getIdPilot();
        $pilot = UserPersonalInfoQuery::create()
            ->findOneById($pilotId);
        $order = new ReceivedOrder;
        $form = $this->createForm(new OrderDetailType(), $order);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $getFlightRouterIdentifier = $request->get('flightId');
                $getUserIdentifier = $customerId;
                $getOpenSeats = $flight->getNumberOfSeats();
                $getReservedSeats = $form->get('numberofreservedseats')->getData();
                $getOrderPaymentMethod = $form->get('idorderpaymentmethod')->getViewData();

                // Does the plane even have the amount of seats available?
                if ($getOpenSeats < $getReservedSeats) {
                    $this->get('session')->getFlashBag()->add('notice', 'Not enough seats available in this plane.');
                    return $this->redirect($this->generateUrl('lamk_private_pilot_order_inspect', array('id' => $id)));
                }
                // Decrease the amount of open seats.
                $openSeatsInPlane = $getOpenSeats - $getReservedSeats;

                // Save the amount to database.
                $flight->setNumberOfSeats($openSeatsInPlane)
                    ->save();

                // Add new reservation to database.
                $order->setNumberOfReservedSeats($getReservedSeats)
                    ->setIdUserPersonalInfo($getUserIdentifier)
                    ->setIdFlightRoute($getFlightRouterIdentifier)
                    ->setIdOrderPaymentMethod($getOrderPaymentMethod)
                    ->save();

                // @todo Why I am not asking for user's email in my form?
                $this->sendEmailSuccessfulOrder($emailAddress = 'antti@sakakino');

                return $this->render("LamkPrivatePilotBundle:Order:success.html.twig");
            }
        }

        return $this->render('LamkPrivatePilotBundle:Order:payment.html.twig', array(
              'flight'   => $flight,
              'pilot'    => $pilot,
              'customer' => $customer,
              'form'     => $form->createView(),
        ));
    }

    /**
     * Basically this is intended for anonymous customers who don't want
     * to register an account for themselves.
     * 
     * 1. collect the needed information.
     * 2. send email to their mail address.
     * 
     */
    public function customerAction($id)
    {

        $customer = new UserPersonalInfo();
        $form = $this->createForm(new UserPersonalInfoType(), $customer);
        $flight = FlightRouteQuery::create()
            ->joinUser()
            ->joinAirplane()
            ->findPk($id);
        $pilotId = $flight->getIdPilot();
        $pilot = UserPersonalInfoQuery::create()
            ->findOneById($pilotId);
        $request = $this->getRequest();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $customer->save();
                $customerId = $customer->getId();

                return $this->redirect($this->generateUrl('lamk_private_pilot_order_payment', array(
                          'customerId' => $customerId,
                          'flightId'   => $id,
                )));
            }
        }

        return $this->render('LamkPrivatePilotBundle:Order:customer.html.twig', array(
              'flight' => $flight,
              'pilot'  => $pilot,
              'form'   => $form->createView(),
        ));
    }

    /**
     * cancelOrderAction
     */
    public function cancelAction()
    {
        
    }

    /**
     * listOrderAction
     */
    public function listAction()
    {

        $id = $this->getUser()->getId();
        $orders = ReceivedOrderQuery::create()
            ->joinFlightRoute()
            ->joinUser()
            ->orderBy('id')
            ->findByIdUser($id);

        if (!$orders) {
            throw $this->createNotFoundException('No orders found for id ' . $id);
        }

        return $this->render('LamkPrivatePilotBundle:Order:list.html.twig', array(
              'orders' => $orders,
        ));
    }

    /**
     * updateAction
     */
    public function updateAction()
    {
        
    }
}
