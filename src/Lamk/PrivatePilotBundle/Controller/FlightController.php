<?php

namespace Lamk\PrivatePilotBundle\Controller;

use \Criteria; // Propel Criteria.
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Lamk\PrivatePilotBundle\Model\FlightRoute;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\UserComment;
use Lamk\PrivatePilotBundle\Model\UserCommentQuery;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Form\Flight\RouteType;
use Lamk\PrivatePilotBundle\Form\Flight\RouteEditType;
use Lamk\PrivatePilotBundle\Form\Comment\CommentType;
use Lamk\PrivatePilotBundle\Form\Search\SearchForm;

/**
 * FlightController is for managing Flight Routes the service provides.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class FlightController extends Controller
{

    /**
     * indexAction lists flight routes currently available in the system.
     */
    public function indexAction($page)
    {
        if (FlightRouteQuery::create()->count() >= 1) {

            $openDepartures = FlightRouteQuery::create()
                ->select(array('departure_location'))
                ->find();
            $openDestinations = FlightRouteQuery::create()
                ->select(array('destination_location'))
                ->find();

            /*
              $Airportnames = AirportQuery::create()
              ->select('city')
              ->find();

              $openIdDepartures = FlightRouteQuery::create()
              ->joinAirportRelatedByIdDepartureLocation()
              ->find();
              // ->select(array('city'))
              //->findPks();

              $openDepartures = AirportQuery::create()
              //->filterById($openIdDepartures)
              ->findByCity($openIdDepartures);

              var_dump($Airportnames);
              die();

              $openDestinations = FlightRouteQuery::create()
              ->joinAirportRelatedByIdDestinationLocation()
              // ->select(array('city'))
              ->find();
             */
        } else {
            /**
             * If there are no data in the database cop out and
             * input null data to prevent running into problems.
             */
            $openDepartures = [''];
            $openDestinations = [''];
        }

        try {
            $flightroutes = FlightRouteQuery::create()
                ->filterByActive(TRUE)
                ->filterByNumberOfSeats(0, Criteria::GREATER_THAN)
                ->filterByExpiresAt(Criteria::CURRENT_DATE, Criteria::GREATER_THAN)
                ->orderById()
                ->paginate($page, $rowsPerPage = 10);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $nextPage = $flightroutes->getNextPage();
        $previousPage = $flightroutes->getPreviousPage();
        $links = $flightroutes->getLinks(10);
        $form = $this->createForm(new SearchForm(), array(
          'action' => $this->generateUrl('lamk_private_pilot_search'),
          'method' => 'POST',
        ));

        return $this->render("LamkPrivatePilotBundle:Flight:index.html.twig", array(
              'form'                   => $form->createView(),
              'flightroutes'           => $flightroutes,
              'page'                   => $page,
              'links'                  => $links,
              'rowsPerPage'            => $rowsPerPage,
              'nextpage'               => $nextPage,
              'previouspage'           => $previousPage,
              'available_destinations' => $openDestinations,
              'available_departures'   => $openDepartures,
        ));
    }

    /**
     * searchAction
     */
    public function searchAction(Request $request)
    {
        $form = $this->createForm(new SearchForm(), null);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $getDepartureData = $form->get('departure')->getData();
                $getDestinationData = $form->get('destination')->getData();
                $getPassengerData = $form->get('passengers')->getData();
                $getDateFromData = $form->get('date')->getData();
                $searchResults = FlightRouteQuery::create()
                    ->filterByDepartureLocation($getDepartureData)
                    ->filterByDestinationLocation($getDestinationData)
                    ->filterByNumberOfSeats($getPassengerData, Criteria::GREATER_THAN)
                    ->filterByDepartureDate($getDateFromData, Criteria::GREATER_THAN)
                    ->find();

                $searchCount = $searchResults->count();

                return $this->render("LamkPrivatePilotBundle:Default:result.html.twig", array(
                      'searchresults' => $searchResults,
                      'count'         => $searchCount,
                ));
            }

            $this->get('session')->getFlashBag()->add('warning', 'Sent form data was invalid!');

            return $this->redirect($this->generateUrl('lamk_private_pilot_homepage'));
        }
    }

    /**
     * createAction
     */
    public function createAction(Request $request)
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->joinUserRole()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }
        $flightroute = new FlightRoute();
        $form = $this->createForm(new RouteType(), $flightroute);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $flightroute->setIdPilot($user->getId());
                $flightroute->setIdPlane($user->getIdPlane());
                $flightroute->save();
                $this->get('session')->getFlashBag()->add('info', 'New flight route was successfully created.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_flight_show', array(
                          'id' => $flightroute->getId()
                )));
            }

            $this->get('session')->getFlashBag()->add('warning', 'Problem encountered during creation of a new flight route');

            return $this->redirect($this->generateUrl('lamk_private_pilot_flight_create'));
        }

        return $this->render('LamkPrivatePilotBundle:Flight:add.html.twig', array(
              'form' => $form->createView(),
        ));
    }

    /**
     * showAction
     */
    public function showAction($id)
    {
        $request = $this->getRequest();
        $page = $request->query->get('page');
        $flightroute = FlightRouteQuery::create()
            ->joinUser()
            ->findPk($id);

        if (!$flightroute) {
            throw $this->createNotFoundException('No flightroute was found for id ' . $id);
        }
        $flightroutePilotId = $flightroute->getIdPilot();
        $flightroutePilot = UserQuery::create()
            ->joinUserPersonalInfo()
            ->joinFile()
            ->findOneById($flightroutePilotId);

        if (!$flightroutePilot) {
            throw $this->createNotFoundException('No pilot was found for flightroute ' . $id);
        }
        $form = $this->createForm(new CommentType(), new UserComment());

        if (true === $this->get('security.context')->isGranted('ROLE_ADMIN')) {

            $comments = UserCommentQuery::create()
                ->filterByIdThread($id)
                ->lastCreatedFirst()
                ->paginate($page, $rowsPerPage = 8);
        } else {

            $comments = UserCommentQuery::create()
                ->filterByVisible(TRUE)
                ->filterByIdThread($id)
                ->lastCreatedFirst()
                ->paginate($page, $rowsPerPage = 8);
        }

        $nextPage = $comments->getNextPage();
        $previousPage = $comments->getPreviousPage();
        $links = $comments->getLinks(5);

        return $this->render("LamkPrivatePilotBundle:Flight:result.html.twig", array(
              'flightroute'      => $flightroute,
              'form'             => $form->createView(),
              'comments'         => $comments,
              'flightroutepilot' => $flightroutePilot,
              'page'             => $page,
              'links'            => $links,
              'rowsPerPage'      => $rowsPerPage,
              'nextpage'         => $nextPage,
              'previouspage'     => $previousPage,
        ));
    }

    /**
     * updateAction
     */
    public function updateAction($id)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_PILOT')) {
            throw new AccessDeniedException();
        }
        $flightroute = FlightRouteQuery::create()
            ->findPk($id);

        if (!$flightroute) {
            throw $this->createNotFoundException(
                'No flight route found for id ' . $id
            );
        }

        // Prevent pilots from editing eachothers flights.
        if ($flightroute->getIdPilot() !== $this->getUser()->getId()) {
            throw new AccessDeniedException();
        }
        $request = $this->getRequest();
        $form = $this->createForm(new RouteEditType(), $flightroute);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $flightroute->save();
                $this->get('session')->getFlashBag()->add('info', 'Flight route info was successfully updated.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_flight_show', array(
                          'id' => $id
                )));
            }
        }

        return $this->render('LamkPrivatePilotBundle:Flight:update.html.twig', array(
              'form' => $form->createView(),
        ));
    }

    /**
     * deleteAction for deactivating a flight route.
     */
    public function deleteAction($id)
    {

        if (false === $this->get('security.context')->isGranted('ROLE_PILOT')) {
            throw new AccessDeniedException();
        }
        $flightroute = FlightRouteQuery::create()
            ->findPk($id);

        if (!$flightroute) {
            throw $this->createNotFoundException(
                'No flight route found for id ' . $id
            );
        }

        /**
         * <p>Instead of actually deleting the data from the database we will
         * instead mark it inactive. It's up to administrators to clean the data
         * if they want to do so.</p>
         * 
         * @todo <p>There could be some kind of clean up process that runs once
         * a day and purges old, at least six months old, data out of the system.</p>
         */
        $flightroute->setActive(FALSE)
            ->save();
        $this->get('session')->getFlashBag()->add('info', 'Flight route was successfully disabled.');

        return $this->redirect($this->generateUrl('lamk_private_pilot_flight', array(
                  /* User is redirected to the first page of the flight routes. */
                  'page' => '1',
        )));
    }

    /**
     * commentAction
     */
    public function commentAction(Request $request)
    {
        $comment = new UserComment();
        $form = $this->createForm(new CommentType(), $comment);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {

                /**
                 * Get data from the form. Do I have to do this?
                 */
                $getComment = $form->get('comment')->getData();
                $getUserId = $form->get('id_user')->getData();
                $getThreadId = $form->get('id_thread')->getData();
                $getParentId = $form->get('id_parent')->getData();

                /**
                 * Save comment to the database.
                 */
                $comment->setComment($getComment)
                    ->setVisible(TRUE)
                    ->setIdParent($getParentId)
                    ->setIdThread($getThreadId)
                    ->setIdUser($getUserId)
                    ->save();

                /**
                 * Return back to the page we came from.
                 */
                return $this->redirect($this->generateUrl('lamk_private_pilot_flight_show', array(
                          'id' => $getThreadId,
                )));
            }
        }
    }

    /**
     * listAction
     */
    public function listAction()
    {
        
    }
}
