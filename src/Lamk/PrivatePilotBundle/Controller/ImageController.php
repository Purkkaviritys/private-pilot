<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lamk\PrivatePilotBundle\Model\FileQuery;

/**
 * Returns url to image requested.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class ImageController extends Controller
{

    public function getImageAction($id, $type)
    {

        $image = FileQuery::create()
            ->findById($id);

        return $image;
    }
}
