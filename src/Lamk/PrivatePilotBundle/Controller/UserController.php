<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Lamk\PrivatePilotBundle\Model\File;
use Lamk\PrivatePilotBundle\Model\FileQuery;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Form\Signup\SignupType;
use Lamk\PrivatePilotBundle\Form\User\AvatarType;
use Lamk\PrivatePilotBundle\Form\User\ChangePasswordType;

/**
 * User Controller is intended for managing properties of the users.
 *
 * <p>UserController manages creation, showing, updating, and deleting data of
 * users whom are active in system.</p>
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class UserController extends Controller
{
    const ROLE_ADMIN = 1;
    const ROLE_PILOT = 2;
    const ROLE_USER = 3;

    /**
     * indexAction
     */
    public function indexAction()
    {
        return $this->render('LamkPrivatePilotBundle:Default:index.html.twig');
    }

    /**
     * changePassword
     */
    public function changePasswordAction(Request $request)
    {
        $logger = $this->get('logger');
        $userId = $this->getUser()->getId();
        $user = UserQuery::create()
            ->findOneById($userId);
        //$password = $user->getPassword();

        $form = $this->createForm(new ChangePasswordType(), null);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {

                /**
                 * @todo SALT THE FRIES!
                 */
                $passwordHash = password_hash($form->get('password')->getData(), PASSWORD_BCRYPT);
                $user->setPassword($passwordHash);
                $user->save();

                $logger->info('Password of a user was changed.');
                $this->get('session')->getFlashBag()->add('info', 'Password was successfully changed.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_profile', array(
                          'id' => $user->getId())));
            }

            $logger->error('Sent password change form data was invalid.');
        }

        return $this->render('LamkPrivatePilotBundle:User:password.html.twig', array(
              'form' => $form->createView(),
        ));
    }

    /**
     * createUserAction
     */
    public function createAction(Request $request)
    {
        $logger = $this->get('logger');
        $address = new UserPersonalInfo();
        $avatar = new File();
        $user = new User();
        $form = $this->createForm(new SignupType(), $user);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user->setActive(true);
                $user->setIdRole(self::ROLE_USER);

                /**
                 * New address and avatar. This is little bit against
                 * the Propel's intended way to do things but I'm doing this
                 * because sometimes Propel fails to automatically save a new
                 * address/avatar and I don't know why. Saving here ensures
                 * they will be created.
                 */
                $address->save();
                $avatar->save();

                /**
                 * @todo SALT THE FRIES!
                 */
                $passwordHash = password_hash($form->get('password')->getData(), PASSWORD_BCRYPT);
                $user->setPassword($passwordHash);
                $user->setUserPersonalInfo($address);
                $user->setIdImage($avatar->getId());
                $user->save();

                $this->userAccountCreatedEmail($emailAddress = $user->getEmailAddress());

                $logger->info('New user was added to the system.');
                $this->get('session')->getFlashBag()->add('info', 'User account was successfully created.');

                /**
                 * Redirect user to profile page after creation of the credentials.
                 * It will also ask user to login.
                 *
                 * @todo <p>Implement something to check that the user's email was real by
                 * confirming it, and only after _that_ make user _active_ in the system.</p>
                 */
                return $this->redirect($this->generateUrl('lamk_private_pilot_profile', array(
                          'id' => $user->getId())));
            }
        }

        return $this->render('LamkPrivatePilotBundle:User:signup.html.twig', array(
              'form' => $form->createView(),
        ));
    }

    /**
     * Send email that a new user account was created.
     */
    private function userAccountCreatedEmail($emailAddress)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Private Pilot: New user registered')
            ->setFrom('customerservice@privatepilot.com')
            ->setTo($emailAddress)
            ->setBody("Welcome to Private Pilot. New user account has been created for you.");

        $this->get('mailer')->send($message);
    }

    /**
     * showUserAction
     */
    public function showAction($id)
    {

        $user = UserQuery::create()
            //->joinUserRole()
            //->joinUserPersonalInfo()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user was found for id ' . $id
            );
        }

        if ($user->getUserRole() == 'ROLE_PILOT' || $user->getUserRole() == 'ROLE_ADMIN') {
            $routes = FlightRouteQuery::create()
                ->limit(3)
                ->findByIdPilot($id);
        } else {
            $routes = null;
        }

        $avatar = $this->fetchAvatar($id);

        return $this->render("LamkPrivatePilotBundle:User:result.html.twig", array(
              'user'         => $user,
              'avatar'       => $avatar,
              'flightroutes' => $routes,
        ));
    }

    /**
     * fetchAvatar
     */
    private function fetchAvatar($id)
    {
        $idImage = UserQuery::create()
            ->findOneById($id)
            ->getIdImage();

        if (!$idImage) {
            // throw $this->createNotFoundException('No image found for id ' . $id);

            return $avatar = null;
        } else {
            $image = FileQuery::create()
                ->findOneById($idImage)
                ->getUrl();

            // Doesn't involve unicorns but feels like magic.
            $avatar = '/media/avatars/' . $image;

            return $avatar;
        }

        return null;
    }

    /**
     * @todo
     */
    public function isAccountNonExpired()
    {

        return true;
    }

    /**
     * @todo
     */
    public function isAccountNonLocked()
    {

        return true;
    }

    /**
     * @todo
     */
    public function isCredentialsNonExpired()
    {

        return true;
    }

    /**
     * Check if the user is active before doing anything.
     *
     * @todo Write logic for user active check first since we need that NOW!
     */
    public function isEnabled($id)
    {
        $user = UserQuery::create()
            ->findPk($id);

        if ($user->getActive() == true) {

            return true;
        } else {

            return false;
        }
    }

    public function uploadAction(Request $request)
    {
        $dir = $this->get('kernel')->getRootDir() . '/../web/media/avatars';
        $form = $this->createForm(new AvatarType());
        $userId = $this->getUser()->getId();
        $file = new File();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $uploadedfile = $form['avatar']->getData();
                $fileExtension = $uploadedfile->guessExtension();

                if (!$fileExtension) {
                    $fileExtension = 'bin';
                }

                $fileName = uniqid() . '.' . $fileExtension;
                $uploadedfile->move($dir, $fileName);
                $user = UserQuery::create()
                    ->findOneById($userId);
                /**
                 * @Todo Mime-type filtering is essential here and we should do
                 * it sooner or later. I need to check the Symfony2's documentation
                 * for only accepting certain type files.
                 */
                $file->setName('Avatar')
                    //->setMimeType(mime_content_type($uploadedfile))
                    ->setUrl($fileName)
                    ->save();

                $user->setIdImage($file->getId())
                    ->save();

                $this->get('session')->getFlashBag()->add('info', 'Avatar was successfully updated.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_profile', array(
                          'id' => $userId)));
            }
        }

        return $this->render("LamkPrivatePilotBundle:User:upload.html.twig", array(
              'form' => $form->createView(),
        ));
    }
}
