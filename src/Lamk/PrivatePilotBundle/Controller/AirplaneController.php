<?php

namespace Lamk\PrivatePilotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Model\AirplaneQuery;

/**
 * Airplane Controller is used to manage airplanes in the system.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class AirplaneController extends Controller
{

    /**
     * @Template()
     */
    public function showAction()
    {
        $planes = AirplaneQuery::create()
            ->limit(100)
            ->find();

        return array('planes' => $planes);
    }

    /**
     * @Template()
     */
    public function addAction()
    {
        $logger = $this->get('logger');
        $airplane = new Airplane();

        $airplane->save();

        $logger->info('New airplane was added to the system.');
        $this->get('session')->getFlashBag()->add('info', 'Airplane was added to the system.');

        return $this->render('LamkPrivatePilotBundle:Airplane:add.html.twig');
    }

    /**
     * @Template()
     */
    public function updateAction($id)
    {
        $logger = $this->get('logger');

        $airplane = AirplaneQuery::create()
            ->findPk($id);

        $airplane->setDescription('This is an airplane.');
        $airplane->save();

        $logger->info('Airplane was successfully updated.');
        $this->get('session')->getFlashBag()->add('info', 'Airplane was successfully updated.');

        return $this->render('LamkPrivatePilotBundle:Airplane:update.html.twig');
    }

    /**
     * @Template()
     */
    public function removeAction($AirplaneId)
    {
        $logger = $this->get('logger');

        $airplane = AirplaneQuery::create()
            ->findPk($AirplaneId);
        $airplane->delete();

        $logger->info('Airplane was deleted from the system.');
        $this->get('session')->getFlashBag()->add('info', 'Airplane was deleted.');

        return $this->render('LamkPrivatePilotBundle:Airplane:remove.html.twig');
    }
}
