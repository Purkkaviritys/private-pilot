<?php

namespace Lamk\PrivatePilotBundle\Controller;

use \Criteria;
// use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\FileQuery;
use Lamk\PrivatePilotBundle\Model\UserCommentQuery;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfoQuery;
use Lamk\PrivatePilotBundle\Form\User\UserType;
use Lamk\PrivatePilotBundle\Form\Airplane\AirplaneType;
use Lamk\PrivatePilotBundle\Form\User\UserPersonalInfoType;

/**
 * Purpose of ProfileController is to display information about the users
 * for the users so that they can manage their bookings, messages, etc.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class ProfileController extends Controller
{

    /**
     * profileAction
     */
    public function profileAction()
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->joinUserRole()
            ->joinUserPersonalInfo()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        $avatar = $this->fetchAvatar($id);
        
        return $this->render("LamkPrivatePilotBundle:Profile:profile.html.twig", array(
              'user'   => $user,
              'avatar' => $avatar,
        ));
    }

    /**
     * fetchAvatar
     */
    private function fetchAvatar($id)
    {
        $idImage = UserQuery::create()
            ->findOneById($id)
            ->getIdImage();

        if (!$idImage) {

            return null;
        } else {
            $image = FileQuery::create()
                ->findOneById($idImage)
                ->getUrl();

            // Doesn't involve unicorns but feels like magic.
            $avatar = '/media/avatars/' . $image;

            return $avatar;
        }

        return null;
    }

    /**
     * historyAction
     */
    public function historyAction()
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->joinUserRole()
            ->joinUserPersonalInfo()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        $orders = ReceivedOrderQuery::create()
            ->joinFlightRoute()
            ->joinUserPersonalInfo()
            ->orderById() // This might cause a bug to occur.
            ->findByIdUserPersonalInfo($id);

        return $this->render('LamkPrivatePilotBundle:Profile:history.html.twig', array(
              'user'   => $user,
              'orders' => $orders,
        ));
    }

    /**
     * messagesAction
     */
    public function messagesAction($page)
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->joinUserRole()
            ->joinUserPersonalInfo()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        $userComments = UserCommentQuery::create()
            ->filterByIdUser($id)
            ->lastCreatedFirst()
            ->paginate($page, $rowsPerPage = 10);

        $nextPage = $userComments->getNextPage();
        $previousPage = $userComments->getPreviousPage();
        $links = $userComments->getLinks(10);

        return $this->render('LamkPrivatePilotBundle:Profile:messages.html.twig', array(
              'user'         => $user,
              'comments'     => $userComments,
              'page'         => $page,
              'links'        => $links,
              'rowsPerPage'  => $rowsPerPage,
              'nextpage'     => $nextPage,
              'previouspage' => $previousPage,
        ));
    }

    /**
     * routesAction
     */
    public function routesAction()
    {
        /**
         * Make sure that ROLE_USER isn't allowed to do this stuff.
         */
        if (false === $this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->joinUserRole()
            ->joinUserPersonalInfo()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        if ($user->getUserRole() == 'ROLE_PILOT' || $user->getUserRole() == 'ROLE_ADMIN') {
            $routes = FlightRouteQuery::create()
                ->findByIdPilot($id);
        }

        return $this->render('LamkPrivatePilotBundle:Profile:routes.html.twig', array(
              'user'         => $user,
              'flightroutes' => $routes,
        ));
    }

    /**
     * settingsAction
     */
    public function settingsAction()
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->joinUserRole()
            ->joinUserPersonalInfo()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        return $this->render('LamkPrivatePilotBundle:Profile:settings.html.twig', array(
              'user' => $user,
        ));
    }

    /**
     * editProfileAction()
     * 
     * This is intended for one user to edit their own personal profile.
     */
    public function updateProfileAction(Request $request)
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->findPk($id);

        if (!$user) {

            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        $form = $this->createForm(new UserType(), $user);
        $form->setData($user);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user->save();
                $this->get('session')->getFlashBag()->add('info', 'Profile was successfully updated.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_profile'));
            }
        }

        return $this->render('LamkPrivatePilotBundle:Profile:edit.html.twig', array(
              'form' => $form->createView(),
              'user' => $user,
        ));
    }

    /**
     * editAddressAction()
     * 
     * This is intended for one user to edit their own personal address.
     */
    public function updateAddressAction(Request $request)
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->filterById($id)
            ->findOne();
        $userPersonalInfoId = $user->getIdPersonalInfo();
        $userPersonalInfo = UserPersonalInfoQuery::create()
            ->findPk($userPersonalInfoId);

        if (!$user) {

            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        $form = $this->createForm(new UserPersonalInfoType(), $userPersonalInfo);
        $form->setData($userPersonalInfo);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $userPersonalInfo->save();
                $this->get('session')->getFlashBag()->add('info', 'Address was successfully updated.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_profile'));
            }
        }

        return $this->render('LamkPrivatePilotBundle:Profile:address.html.twig', array(
              'form' => $form->createView(),
              'user' => $userPersonalInfo,
        ));
    }

    /**
     * updateAirplaneAction()
     * 
     * This is intended for one user to edit their own airplane information.
     */
    public function updateAirplaneAction(Request $request)
    {
        $id = $this->getUser()->getId();
        $user = UserQuery::create()
            ->filterById($id)
            ->findOne();

        $airplane = $user->getAirplane();

        if (!$airplane) {

            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        $form = $this->createForm(new AirplaneType, $airplane);
        $form->setData($airplane);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $airplane->save();
                $this->get('session')->getFlashBag()->add('info', 'Airplane was successfully updated.');

                return $this->redirect($this->generateUrl('lamk_private_pilot_profile'));
            }
        }

        return $this->render('LamkPrivatePilotBundle:Profile:airplane.html.twig', array(
              'form'     => $form->createView(),
              'airplane' => $airplane,
        ));
    }
}
