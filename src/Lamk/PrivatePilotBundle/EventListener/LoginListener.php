<?php

namespace Lamk\PrivatePilotBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @author Matt Drollette <matt@drollette.com>
 */
class LoginListener
{
    /**
     * @var SecurityContext
     */
    protected $security;
    /**
     * @var Session
     */
    protected $session;

    /**
     * @param SecurityContext $security
     * @param Session $session
     */
    public function __construct(SecurityContext $security, Session $session)
    {
        $this->security = $security;
        $this->session = $session;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $timezone = $this->security->getToken()->getUser()->getTimezone();
        if (empty($timezone)) {
            $timezone = 'UTC';
        }
        $this->session->set('timezone', $timezone);
    }
}
