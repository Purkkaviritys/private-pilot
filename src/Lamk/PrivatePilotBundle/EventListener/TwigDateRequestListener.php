<?php

namespace Lamk\PrivatePilotBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\Session;

class TwigDateRequestListener
{
    protected $twig;
    protected $session;

    function __construct(\Twig_Environment $twig, Session $session)
    {
        $this->twig = $twig;
        $this->session = $session;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->session->get('timezone') != NULL) {
            $this->twig->getExtension('core')->setTimezone($this->session->get('timezone'));
        } else {
            $this->twig->getExtension('core')->setTimezone('UTC');
        }
    }
}
