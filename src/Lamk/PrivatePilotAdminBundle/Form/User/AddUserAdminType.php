<?php

namespace Lamk\PrivatePilotAdminBundle\Form\User;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * UserAdminType
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 */
class AddUserAdminType extends BaseAbstractType
{

    /**
     *  {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('active', 'checkbox', array(
          'label'    => 'Is the user account active?',
          'required' => true,
        ));
        $builder->add('username', 'text', array(
          'label'    => 'Username',
          'required' => false,
        ));
        $builder->add('emailaddress', 'email', array(
          'label'    => 'Email Address',
          'required' => true,
        ));
        $builder->add('password', 'repeated', array(
          'first_name'  => 'password',
          'second_name' => 'confirm',
          'type'        => 'password',
          // 'label' => 'Password',
          'required'    => true,
        ));
        $builder->add('user_role', 'model', array(
          'class'    => 'Lamk\PrivatePilotBundle\Model\UserRole',
          'property' => 'role',
          //'index_property' => 'id',
          'label'    => 'Role',
        ));
        $builder->add('description', 'textarea', array(
          'label'    => 'Description',
          'required' => false,
        ));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'Lamk\PrivatePilotBundle\Model\User',
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
