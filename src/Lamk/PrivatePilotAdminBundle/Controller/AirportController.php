<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\Airport;
use Lamk\PrivatePilotBundle\Model\AirportQuery;
use Lamk\PrivatePilotBundle\Form\Airport\AirportType;

/**
 * AirfieldController
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class AirportController extends Controller
{

    /**
     * @Route("/admin/airport/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="lamk_private_pilot_admin_airport")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction($page)
    {

        $airports = AirportQuery::create()
            ->orderById()
            ->paginate($page, $rowsPerPage = 10);
        $nextPage = $airports->getNextPage();
        $previousPage = $airports->getPreviousPage();
        $links = $airports->getLinks(10);

        return array(
          'airports'     => $airports,
          'page'         => $page,
          'links'        => $links,
          'rowsPerPage'  => $rowsPerPage,
          'nextpage'     => $nextPage,
          'previouspage' => $previousPage
        );
    }

    /**
     * @Route("/admin/airport/add", name="lamk_private_pilot_admin_airport_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $airport = new Airport();
        $form = $this->createForm(new AirportType(), $airport);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $airport->save();

                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_airport'));
            }
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/admin/airport/{id}/edit", name="lamk_private_pilot_admin_airport_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id, Request $request)
    {

        $airport = AirportQuery::create()
            ->findPk($id);

        if (!$airport) {
            throw $this->createNotFoundException('No airport found for id ' . $id);
        }

        $form = $this->createForm(new AirportType(), $airport);
        $form->setData($airport);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $airport->save();

                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_airport'));
            }
        }

        return array('form' => $form->createView(), 'airport' => $airport);
    }

    /**
     * @Route("/admin/airport/{id}/delete", name="lamk_private_pilot_admin_airport_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction($id, Request $request)
    {

        return array();
    }
}
