<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\User;
use Lamk\PrivatePilotBundle\Model\UserPersonalInfo;
use Lamk\PrivatePilotAdminBundle\Form\User\UserAdminType;
use Lamk\PrivatePilotAdminBundle\Form\User\AddUserAdminType;

/**
 * Admin user controller is used to manage users in the site.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class UserController extends Controller
{

    /**
     * @Route("/admin/user/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="lamk_private_pilot_admin_user")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction($page)
    {
        if (UserQuery::create()->count() >= 1) {
            $users = UserQuery::create()
                ->orderById()
                ->paginate($page, $rowsPerPage = 10);
            $nextPage = $users->getNextPage();
            $previousPage = $users->getPreviousPage();
            $links = $users->getLinks(10);
        } else {
            $users = [''];
        }

        return array(
          'users'        => $users,
          'page'         => $page,
          'links'        => $links,
          'rowsPerPage'  => $rowsPerPage,
          'nextpage'     => $nextPage,
          'previouspage' => $previousPage
        );
    }

    private function sortById()
    {
        $users = UserQuery::create()
            ->joinUserRole()
            ->orderById()
            ->find();

        return $users;
    }

    private function sortByUsername()
    {
        $users = UserQuery::create()
            ->joinUserRole()
            ->orderByUsername()
            ->find();

        return $users;
    }

    private function sortByEmail()
    {
        $users = UserQuery::create()
            ->joinUserRole()
            ->orderByEmailAddress()
            ->find();

        return $users;
    }

    private function sortByRole()
    {
        $users = UserQuery::create()
            ->joinUserRole()
            ->orderByIdRole()
            ->find();

        return $users;
    }

    private function sortByDate()
    {
        $users = UserQuery::create()
            ->joinUserRole()
            ->orderByDateAdded()
            ->find();

        return $users;
    }

    /**
     * @Route("/admin/user/add", name="lamk_private_pilot_admin_user_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $logger = $this->get('logger');
        $user = new User();
        $personalinfo = new UserPersonalInfo();
        $form = $this->createForm(new AddUserAdminType(), $user);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user->setActive(true);
                $user->setUserRole($form->get('user_role')->getData());
                $passwordHash = password_hash($form->get('password')->getData(), PASSWORD_BCRYPT);
                $user->setPassword($passwordHash);
                $user->setUserPersonalInfo($personalinfo);
                $user->save();
                $logger->info('New user was added to the system.');

                /**
                 * @todo <p>Implement something to check that the user's email was real by
                 * confirming it, and only after _that_ make user _active_ in the system.</p>
                 */
                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_user'));
            }
            $logger->error('Send form data was invalid!');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/admin/user/{id}/edit", name="lamk_private_pilot_admin_user_edit", requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id)
    {
        $user = UserQuery::create()
            ->joinUserRole()
            ->findPk($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }
        $request = $this->getRequest();
        $form = $this->createForm(new UserAdminType(), $user);
        $form->setData($user);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user->save();

                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_user'));
            }
        }

        return array('form' => $form->createView(), 'user' => $user);
    }
}
