<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery;

/**
 * Description of OrderController
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class OrderController extends Controller
{

    /**
     * @Route("/admin/order/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="lamk_private_pilot_admin_order")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction($page)
    {
        $orders = ReceivedOrderQuery::create()
            ->lastCreatedFirst()
            ->paginate($page, $rowsPerPage = 10);
        $nextPage = $orders->getNextPage();
        $previousPage = $orders->getPreviousPage();
        $links = $orders->getLinks(10);

        return array(
          'orders'       => $orders,
          'page'         => $page,
          'links'        => $links,
          'rowsPerPage'  => $rowsPerPage,
          'nextpage'     => $nextPage,
          'previouspage' => $previousPage
        );
    }
}
