<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\AirplaneQuery;
use Lamk\PrivatePilotBundle\Model\Airplane;
use Lamk\PrivatePilotBundle\Form\Airplane\AirplaneType;

/**
 * AirplaneController is used to manage airplanes and their data in
 * the Private Pilot service.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class AirplaneController extends Controller
{

    /**
     * @Route("/admin/airplane/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="lamk_private_pilot_admin_airplane")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction($page)
    {
        $planes = AirplaneQuery::create()
            ->orderById()
            ->paginate($page, $rowsPerPage = 10);
        $nextPage = $planes->getNextPage();
        $previousPage = $planes->getPreviousPage();
        $links = $planes->getLinks(10);

        return array(
          'planes'       => $planes,
          'page'         => $page,
          'links'        => $links,
          'rowsPerPage'  => $rowsPerPage,
          'nextpage'     => $nextPage,
          'previouspage' => $previousPage
        );
    }

    /**
     * @Route("/admin/airplane/add", name="lamk_private_pilot_admin_airplane_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $airplane = new Airplane();
        $form = $this->createForm(new AirplaneType(), $airplane);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $airplane->save();

                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_airplane', array(
                          'id' => $airplane->getId()
                )));
            }
        }
        return array('form' => $form->createView());
    }

    /**
     * @Route("/admin/airplane/{id}/edit", name="lamk_private_pilot_admin_airplane_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id, Request $request)
    {

        $airplane = AirplaneQuery::create()
            ->findPk($id);

        if (!$airplane) {
            throw $this->createNotFoundException('No airplane found for id ' . $id);
        }

        $form = $this->createForm(new AirplaneType(), $airplane);
        $form->setData($airplane);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $airplane->save();

                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_airplane'));
            }
        }

        return array('form' => $form->createView(), 'airplane' => $airplane);
    }
}
