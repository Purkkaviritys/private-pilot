<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * StyleController
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class StyleController extends Controller
{

    /**
     * @Route("/admin/style", name="lamk_private_pilot_admin_style")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {

        return array();
    }
}
