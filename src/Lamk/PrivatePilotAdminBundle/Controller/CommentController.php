<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lamk\PrivatePilotBundle\Model\UserCommentQuery;

/**
 * Admin comment controller is used to manage comments on the site.
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class CommentController extends Controller
{

    /**
     * @Route("/admin/comment/delete/{id}", name="lamk_private_pilot_admin_comment_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteCommentAction($id)
    {
        $comment = UserCommentQuery::create()
            ->findPk($id);

        if (!$comment) {
            throw $this->createNotFoundException('No comment was found for id ' . $id);
        }

        $getThreadId = $comment->getIdThread();

        $comment->delete();

        return $this->redirect($this->generateUrl('lamk_private_pilot_flight_show', array(
                  'id' => $getThreadId,
        )));
    }

    /**
     * @Route("/admin/comment/hide/{id}", name="lamk_private_pilot_admin_comment_hide")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function hideCommentAction($id)
    {
        $comment = UserCommentQuery::create()
            ->findPk($id);

        if (!$comment) {
            throw $this->createNotFoundException('No comment was found for id ' . $id);
        }

        $getThreadId = $comment->getIdThread();

        $comment->setVisible(FALSE)
            ->save();

        return $this->redirect($this->generateUrl('lamk_private_pilot_flight_show', array(
                  'id' => $getThreadId,
        )));
    }

    /**
     * @Route("/admin/comment/unhide/{id}", name="lamk_private_pilot_admin_comment_unhide")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function unhideCommentAction($id)
    {
        $comment = UserCommentQuery::create()
            ->findPk($id);

        if (!$comment) {
            throw $this->createNotFoundException('No comment was found for id ' . $id);
        }

        $getThreadId = $comment->getIdThread();

        $comment->setVisible(TRUE)
            ->save();

        return $this->redirect($this->generateUrl('lamk_private_pilot_flight_show', array(
                  'id' => $getThreadId,
        )));
    }
}
