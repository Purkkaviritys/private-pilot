<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\UserQuery;
use Lamk\PrivatePilotBundle\Model\UserCommentQuery;
use Lamk\PrivatePilotBundle\Model\FlightRouteQuery;
use Lamk\PrivatePilotBundle\Model\ReceivedOrderQuery;

/**
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class DefaultController extends Controller
{
    const ROLE_ADMIN = 1;
    const ROLE_PILOT = 2;
    const ROLE_USER = 3;

    /**
     * Count how many users there are in the database.
     */
    private function getUserCount()
    {
        $usersInTotal = UserQuery::create()
            ->count();

        return $usersInTotal;
    }

    /**
     * Count how many admins there are in the database.
     */
    private function getAdminCount()
    {
        $adminsInTotal = UserQuery::create()
            ->filterByIdRole(self::ROLE_ADMIN)
            ->count();

        return $adminsInTotal;
    }

    /**
     * Count how many pilots there are in the database.
     */
    private function getPilotCount()
    {
        $pilotsInTotal = UserQuery::create()
            ->filterByIdRole(self::ROLE_PILOT)
            ->count();

        return $pilotsInTotal;
    }

    /**
     * Count how many customers there are in the database.
     */
    private function getCustomerCount()
    {
        $customersInTotal = UserQuery::create()
            ->filterByIdRole(self::ROLE_USER)
            ->count();

        return $customersInTotal;
    }

    /**
     * Get the latest customers.
     */
    private function getLatestUsers()
    {

        $customers = UserQuery::create()
            ->filterByActive(TRUE)
            ->filterByIdRole(self::ROLE_USER)
            ->orderById(\Criteria::DESC)
            ->limit(5)
            ->find();

        return $customers;
    }

    /**
     * Get recent orders.
     */
    private function getRecentOrders()
    {
        $orders = ReceivedOrderQuery::create()
            ->lastCreatedFirst()
            ->limit(5)
            ->find();

        return $orders;
    }

    /**
     * Get recent comments.
     */
    private function getRecentComments()
    {
        $comments = UserCommentQuery::create()
            ->lastCreatedFirst()
            ->limit(5)
            ->find();

        return $comments;
    }

    /**
     * @Route("/admin", name="lamk_private_pilot_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $usersInTotal = $this->getUserCount();
        $adminsInTotal = $this->getAdminCount();
        $pilotsInTotal = $this->getPilotCount();
        $customersInTotal = $this->getCustomerCount();
        $latestUsers = $this->getLatestUsers();
        $latestOrders = $this->getRecentOrders();
        $latestComments = $this->getRecentComments();

        $flightRoutesInTotal = FlightRouteQuery::create()
            ->count();

        $activeFlightRoutesInTotal = FlightRouteQuery::create()
            ->filterByActive(TRUE)
            ->count();

        $inactiveFlightRoutesInTotal = FlightRouteQuery::create()
            ->filterByActive(FALSE)
            ->count();

        $ordersInTotal = ReceivedOrderQuery::create()
            ->count();

        return array(
          'users_in_total'         => $usersInTotal,
          'customers_in_total'     => $customersInTotal,
          'admins_in_total'        => $adminsInTotal,
          'pilots_in_total'        => $pilotsInTotal,
          'flight_routes_in_total' => $flightRoutesInTotal,
          'active_flight_routes'   => $activeFlightRoutesInTotal,
          'inactive_flight_routes' => $inactiveFlightRoutesInTotal,
          'orders_in_total'        => $ordersInTotal,
          'users'                  => $latestUsers,
          'orders'                 => $latestOrders,
          'comments'               => $latestComments,
        );
    }
}
