<?php

namespace Lamk\PrivatePilotAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lamk\PrivatePilotBundle\Model\UserRole;
use Lamk\PrivatePilotBundle\Model\UserRoleQuery;
use Lamk\PrivatePilotBundle\Form\Type\UserRoleType;

/**
 * Description of RoleController
 *
 * @author Antti Aspinen <antti.aspinen@student.lamk.fi>
 * @version GIT: $Id$ In development.
 */
class RoleController extends Controller
{

    /**
     * @Route("/admin/role", name="lamk_private_pilot_admin_role")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {

        /**
         * Find complete list of first 10 roles saved into the database.
         */
        $roles = UserRoleQuery::create()
            ->limit(10)
            ->find();

        return array('roles' => $roles);
    }

    /**
     * @Route("/admin/role/add", name="lamk_private_pilot_admin_role_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction()
    {
        $roles = new UserRole();
        $form = $this->createForm(new UserRoleType(), $roles);
        $request = $this->getRequest();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $roles->save();

                return $this->redirect($this->generateUrl('lamk_private_pilot_admin_role'));
            }
        }

        return array('form' => $form->createView());
    }
}
